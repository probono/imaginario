TARGET = tst_tag_model

include(tests.pri)

QT += \
    qml

SOURCES += \
    $${SRC_DIR}/tag_model.cpp \
    $${SRC_DIR}/utils.cpp \
    tst_tag_model.cpp

HEADERS += \
    $${SRC_DIR}/abstract_database.h \
    $${SRC_DIR}/database.h \
    $${SRC_DIR}/tag_model.h \
    $${SRC_DIR}/utils.h
