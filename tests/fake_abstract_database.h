/*
 * Copyright (C) 2015 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef FAKE_ABSTRACT_DATABASE_H
#define FAKE_ABSTRACT_DATABASE_H

#include "abstract_database.h"

#include <QMutex>

namespace Imaginario {

class AbstractDatabasePrivate: public QObject
{
    Q_OBJECT
    Q_DECLARE_PUBLIC(AbstractDatabase)

public:
    AbstractDatabasePrivate(AbstractDatabase *q):
        QObject(),
        m_commitResult(true),
        q_ptr(q)
    {}
    ~AbstractDatabasePrivate() {}

    void setCommitResult(bool ok) { m_commitResult = ok; }

Q_SIGNALS:
    void transactionCalled();
    void commitCalled();
    void rollbackCalled();

protected:
    bool m_commitResult;
    QMutex m_mutex;
    AbstractDatabase *q_ptr;
};

} // namespace

#endif // FAKE_ABSTRACT_DATABASE_H
