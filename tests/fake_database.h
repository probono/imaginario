/*
 * Copyright (C) 2015 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef FAKE_DATABASE_H
#define FAKE_DATABASE_H

#include "database.h"
#include "fake_abstract_database.h"

#include <QDateTime>
#include <QMap>
#include <QStringList>

namespace Imaginario {

typedef QHash<Tag,QRectF> TagAreas;

class DatabasePrivate: public AbstractDatabasePrivate
{
    Q_OBJECT
    Q_DECLARE_PUBLIC(Database)

    struct TagData {
    TagData(): parent(-1), isCategory(false), popularity(0.0), usageCount(0) {}
    TagData(TagId parent, const QString &name,
            const QString &icon, float popularity = 0.0):
        parent(parent), name(name), icon(icon), isCategory(false),
        popularity(popularity), usageCount(0)
    {}
    TagData(const QString &name): name(name) {}
    TagId parent;
    QString name;
    QString icon;
    bool isCategory;
    float popularity;
    int usageCount;
};

    struct RollData {
        RollData() {}
        RollData(const QDateTime &time): time(time) {}
        QDateTime time;
    };

public:
    static DatabasePrivate *mocked(Database *o) { return o->d_func(); }
    void setTags(const QStringList &tags) {
        m_tags.clear();
        Q_FOREACH(const QString &name, tags) {
            m_tags.insert(nextTagId(), TagData(name));
        }
    }
    void setTagPopularity(Tag tag, float popularity) {
        m_tags[tag.id()].popularity = popularity;
    }
    void setTagUsageCount(Tag tag, int usageCount) {
        m_tags[tag.id()].usageCount = usageCount;
    }
    void setPhotos(const QList<Database::Photo> &photos) {
        Q_FOREACH(const Database::Photo &pconst, photos) {
            Database::Photo p = pconst;
            if (p.id() == -1) setPhotoId(p, m_photos.count());
            m_photos.insert(p.id(), p);
        }
    }
    void setPhotoId(Database::Photo &p, PhotoId id) { p.d->id = id; }
    void setPhotoRoll(Database::Photo &p, int roll) { p.d->rollId = roll; }
    void setPhotoVersionFlags(Database::Photo &p,
                              bool hasVersions, bool isDefaultVersion) {
        if (isDefaultVersion) {
            p.d->versionFlags = Database::PhotoData::IsDefaultVersion;
        } else if (hasVersions) {
            p.d->versionFlags = Database::PhotoData::IsNonDefaultVersion;
        } else {
            p.d->versionFlags = Database::PhotoData::NoVersions;
        }
    }
    RollId lastRollId() const { return m_lastRollId; }

    void emitPhotoChanged(PhotoId id) { Q_EMIT q_func()->photoChanged(id); }

private:
    DatabasePrivate(Database *q):
        AbstractDatabasePrivate(q),
        m_lastRollId(0)
    {}

    PhotoId addPhoto(const Database::Photo &photo, RollId roll);
    QList<Database::Photo> findPhotos(SearchFilters filters,
                                      Qt::SortOrder sort);
    TagId nextTagId() const { return m_tags.count() + 1; }

Q_SIGNALS:
    void photosCalled(QList<int> photoIds);
    void findPhotosCalled(Imaginario::SearchFilters filters,
                          Qt::SortOrder sort);
    void photoVersionsCalled(int photoId);
    void masterVersionCalled(int photoId);
    void createRollCalled();
    void addPhotoCalled(Imaginario::Database::Photo photo, int roll);
    void makeVersionCalled(int photoId, int master);
    void createTagCalled(Imaginario::Tag parent, QString name, QString icon);
    void addTagsCalled(int photoId, QList<Imaginario::Tag> tags);
    void setTagsCalled(int photoId, QList<Imaginario::Tag> tags);
    void removeTagsCalled(int photoId, QList<Imaginario::Tag> tags);
    void setDescriptionCalled(int photoId, QString description);
    void setRatingCalled(int photoId, int rating);
    void setLocationCalled(int photoId, GeoPoint p);

private:
    friend class Roll;
    friend class Tag;
    RollId m_lastRollId;
    QHash<TagId,TagData> m_tags;
    QMap<PhotoId,Database::Photo> m_photos;
    QHash<PhotoId,TagAreas> m_photoTags;
    QMap<RollId,RollData> m_rolls;
};

} // namespace

Q_DECLARE_METATYPE(Imaginario::SearchFilters)
Q_DECLARE_METATYPE(Imaginario::Database::Photo)
Q_DECLARE_METATYPE(Qt::SortOrder)

#endif // FAKE_DATABASE_H
