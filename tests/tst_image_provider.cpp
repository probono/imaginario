/*
 * Copyright (C) 2015 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "fake_icon_thumbnailer.h"
#include "fake_job_executor.h"
#include "fake_metadata.h"
#include "fake_thumbnailer.h"
#include "image_provider.h"

#include <QDir>
#include <QSignalSpy>
#include <QSize>
#include <QTest>

using namespace Imaginario;

class ImageProviderTest: public QObject
{
    Q_OBJECT

public:
    ImageProviderTest();

private Q_SLOTS:
    void testRequestImage_data();
    void testRequestImage();

private:
    QDir m_dataDir;
};

ImageProviderTest::ImageProviderTest():
    m_dataDir(QString(ITEMS_DIR "/"))
{
}

void ImageProviderTest::testRequestImage_data()
{
    QTest::addColumn<QString>("id");
    QTest::addColumn<QSize>("requestedSize");
    QTest::addColumn<QImage>("icon");
    QTest::addColumn<QImage>("thumbnail");
    QTest::addColumn<QImage>("preview");
    QTest::addColumn<bool>("expectedJob");
    QTest::addColumn<QSize>("expectedSize");
    QTest::addColumn<QImage>("expectedImage");

    QImage nullImage;
    QImage image5(m_dataDir.filePath("image5.jpg"));
    QImage image5_32x32 = image5.scaled(32, 32, Qt::KeepAspectRatioByExpanding,
                                        Qt::SmoothTransformation);
    QImage image5_40x40 = image5.scaled(40, 40, Qt::KeepAspectRatio,
                                        Qt::SmoothTransformation);
    QImage image5_100x100 = image5.scaled(100, 100, Qt::KeepAspectRatio,
                                          Qt::SmoothTransformation);
    QImage image5_200x200 = image5.scaled(200, 200, Qt::KeepAspectRatio,
                                          Qt::SmoothTransformation);

    QTest::newRow("from file") <<
        QUrl::fromLocalFile(m_dataDir.filePath("image5.jpg")).toString() <<
        QSize(40, 40) <<
        nullImage <<
        nullImage <<
        nullImage <<
        true <<
        QSize(300, 200) <<
        image5_40x40;

    QTest::newRow("from file, no scaling") <<
        QUrl::fromLocalFile(m_dataDir.filePath("image5.jpg")).toString() <<
        QSize(400, 400) <<
        nullImage <<
        nullImage <<
        nullImage <<
        false <<
        QSize(300, 200) <<
        image5;

    QTest::newRow("from preview") <<
        QUrl::fromLocalFile(m_dataDir.filePath("image5.jpg")).toString() <<
        QSize(100, 100) <<
        nullImage <<
        nullImage <<
        image5_100x100 <<
        false <<
        QSize(1200, 30) <<
        image5_100x100;

    QTest::newRow("from thumbnail") <<
        QUrl::fromLocalFile(m_dataDir.filePath("image5.jpg")).toString() <<
        QSize(200, 200) <<
        nullImage <<
        image5_200x200 <<
        image5_100x100 <<
        false <<
        QSize() <<
        image5_200x200;

    QTest::newRow("from icon") <<
        QUrl::fromLocalFile(m_dataDir.filePath("image5.jpg")).toString() <<
        QSize(32, 32) <<
        image5_32x32 <<
        image5_200x200 <<
        image5_100x100 <<
        false <<
        QSize(48, 32) <<
        image5_32x32;

    QTest::newRow("make thumbnail") <<
        QUrl::fromLocalFile(m_dataDir.filePath("image5.jpg")).toString() <<
        QSize(200, 200) <<
        nullImage <<
        nullImage <<
        nullImage <<
        true <<
        QSize(300, 200) <<
        image5_200x200;
}

void ImageProviderTest::testRequestImage()
{
    QFETCH(QString, id);
    QFETCH(QSize, requestedSize);
    QFETCH(QImage, icon);
    QFETCH(QImage, thumbnail);
    QFETCH(QImage, preview);
    QFETCH(bool, expectedJob);
    QFETCH(QSize, expectedSize);
    QFETCH(QImage, expectedImage);

    MetadataPrivate *mdMocked = new MetadataPrivate();
    QSignalSpy loadPreviewCalled(mdMocked,
                                 SIGNAL(loadPreviewCalled(QString,QSize)));
    QObject::connect(mdMocked, &MetadataPrivate::loadPreviewCalled,
                     [=](QString file, QSize) {
        mdMocked->setPreview(file, preview, expectedSize);
    });

    ThumbnailerPrivate *tnMocked = new ThumbnailerPrivate();
    QSignalSpy loadCalled(tnMocked, SIGNAL(loadCalled(QUrl,QSize)));
    QObject::connect(tnMocked, &ThumbnailerPrivate::loadCalled,
                     [=](QUrl uri, QSize) {
        tnMocked->setLoadReply(uri, thumbnail);
    });

    IconThumbnailerPrivate *itMocked = new IconThumbnailerPrivate();
    QSignalSpy loadIconCalled(itMocked, SIGNAL(loadCalled(QUrl)));
    QObject::connect(itMocked, &IconThumbnailerPrivate::loadCalled,
                     [=](QUrl uri) {
        itMocked->setLoadReply(uri, icon);
    });

    QScopedPointer<JobExecutor> executor(JobExecutor::instance());
    JobExecutorPrivate *executorMock =
        JobExecutorPrivate::mocked(executor.data());
    QSignalSpy addJobsCalled(executorMock,
                             SIGNAL(addJobsCalled(QVector<Imaginario::Job>)));

    ImageProvider image_provider;
    QSize size;
    QImage image = image_provider.requestImage(id, &size, requestedSize);

    if (expectedImage == icon) {
        QCOMPARE(loadIconCalled.count(), 1);
        QCOMPARE(loadIconCalled.at(0).at(0).toString(), id);
    } else {
        QCOMPARE(loadCalled.count(), 1);
        QCOMPARE(loadCalled.at(0).at(0).toString(), id);
        QCOMPARE(loadCalled.at(0).at(1).toSize(), requestedSize);

        QCOMPARE(loadPreviewCalled.count(), thumbnail.isNull() ? 1 : 0);
        if (!loadPreviewCalled.isEmpty()) {
            QCOMPARE(loadPreviewCalled.at(0).at(0).toString(),
                     QUrl(id).toLocalFile());
            QCOMPARE(loadPreviewCalled.at(0).at(1).toSize(), requestedSize);
        }
    }
    QCOMPARE(addJobsCalled.count(), expectedJob ? 1 : 0);
    if (expectedJob) {
        QVector<Job> jobs = addJobsCalled.at(0).at(0).value<QVector<Job>>();
        QCOMPARE(jobs.count(), 1);
        ThumbnailJob job = static_cast<const ThumbnailJob &>(jobs.at(0));
        QCOMPARE(job.uri().toString(), id);
    }
    QCOMPARE(size, expectedSize);
    QCOMPARE(image, expectedImage);
}

QTEST_GUILESS_MAIN(ImageProviderTest)

#include "tst_image_provider.moc"
