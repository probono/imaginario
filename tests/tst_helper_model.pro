TARGET = tst_helper_model

include(tests.pri)

QT += \
    qml

SOURCES += \
    $${SRC_DIR}/helper_model.cpp \
    $${SRC_DIR}/utils.cpp \
    tst_helper_model.cpp

HEADERS += \
    $${SRC_DIR}/helper_model.h \
    $${SRC_DIR}/utils.cpp

ITEMS_DIR = $${TOP_SRC_DIR}/tests/data
DEFINES += \
    ITEMS_DIR=\\\"$${ITEMS_DIR}\\\"
