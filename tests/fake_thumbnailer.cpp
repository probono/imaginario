/*
 * Copyright (C) 2015 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "fake_thumbnailer.h"

using namespace Imaginario;

static ThumbnailerPrivate *m_privateInstance = 0;

ThumbnailerPrivate::ThumbnailerPrivate():
    QObject()
{
    m_privateInstance = this;
}

ThumbnailerPrivate::~ThumbnailerPrivate()
{
    m_privateInstance = 0;
}

Thumbnailer::Thumbnailer()
{
    if (m_privateInstance) {
        m_privateInstance->q_ptr = this;
        d_ptr = m_privateInstance;
    } else {
        d_ptr = new ThumbnailerPrivate(this);
    }
}

Thumbnailer::~Thumbnailer()
{
    delete d_ptr;
}

QImage Thumbnailer::load(const QUrl &uri, const QSize &requestedSize) const
{
    Q_D(const Thumbnailer);
    Q_EMIT const_cast<ThumbnailerPrivate*>(d)->loadCalled(uri, requestedSize);
    return d->m_loadReply[uri];
}

bool Thumbnailer::create(const QUrl &uri) const
{
    Q_D(const Thumbnailer);
    Q_EMIT const_cast<ThumbnailerPrivate*>(d)->createCalled(uri);
    return d->m_createReplies[uri];
}
