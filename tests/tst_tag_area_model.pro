TARGET = tst_tag_area_model

include(tests.pri)

QT += \
    concurrent

SOURCES += \
    $${SRC_DIR}/tag_area_model.cpp \
    tst_tag_area_model.cpp

HEADERS += \
    $${SRC_DIR}/abstract_database.h \
    $${SRC_DIR}/database.h \
    $${SRC_DIR}/face_detector.h \
    $${SRC_DIR}/tag_area_model.h
