TARGET = tst_folder_model

include(tests.pri)

QT += \
    qml

SOURCES += \
    $${SRC_DIR}/folder_model.cpp \
    tst_folder_model.cpp

HEADERS += \
    $${SRC_DIR}/folder_model.h
