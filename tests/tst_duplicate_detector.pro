TARGET = tst_duplicate_detector

include(tests.pri)

QT += \
    core

SOURCES += \
    $${SRC_DIR}/duplicate_detector.cpp \
    tst_duplicate_detector.cpp

HEADERS += \
    $${SRC_DIR}/abstract_database.h \
    $${SRC_DIR}/database.h \
    $${SRC_DIR}/duplicate_detector.h \
    $${SRC_DIR}/metadata.h
