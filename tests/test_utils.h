/*
 * Copyright (C) 2016 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef IMAGINARIO_TEST_UTILS_H
#define IMAGINARIO_TEST_UTILS_H

#include "metadata.h"
#include "types.h"

#include <QByteArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QRect>
#include <QSet>
#include <QTest>
#include <QVariant>
#include <QVariantMap>

namespace QTest {

template<>
char *toString(const Imaginario::GeoPoint &p)
{
    QByteArray ba = "GeoPoint(";
    ba += QByteArray::number(p.lat, 'f', 3) + ", " +
        QByteArray::number(p.lon, 'f', 3);
    ba += ")";
    return qstrdup(ba.data());
}

template<>
char *toString(const QHash<QString,QSet<QString>> &p)
{
    QByteArray ba = "QMap(";
    for (auto i = p.begin(); i != p.end(); i++) {
        ba += '"';
        ba += i.key().toUtf8();
        ba += "\": [";
        ba += QStringList(i.value().toList()).join(',');
        ba += "]\n";
    }
    ba += ")";
    return qstrdup(ba.data());
}

template<>
char *toString(const QList<QVariant> &p)
{
    QByteArray ba = "QList(";
    QStringList parts;
    Q_FOREACH(const QVariant &v, p) {
        parts.append(QString::fromUtf8(toString(v)));
    }
    ba += parts.join(", ").toUtf8().constData();
    ba += ")";
    return qstrdup(ba.data());
}

template<>
char *toString(const QSet<Imaginario::Metadata::RegionInfo> &set)
{
    QByteArray ba = "QSet<RegionInfo>(";
    QStringList list;
    Q_FOREACH(const Imaginario::Metadata::RegionInfo &ri, set) {
        const QRectF &r = ri.area;
        list.append(QString("QRect(%1,%2 %3x%4) - '%5' - '%6'").
                    arg(r.x()).arg(r.y()).arg(r.width()).arg(r.height()).
                    arg(ri.name).arg(ri.description));
    }
    ba += list.join(", ");
    ba += ")";
    return qstrdup(ba.data());
}

template<>
char *toString(const QHash<QString,QSet<Imaginario::Metadata::RegionInfo>> &p)
{
    QByteArray ba = "QHash(";
    for (auto i = p.begin(); i != p.end(); i++) {
        ba += '"';
        ba += i.key().toUtf8();
        ba += "\": ";
        char *valueString = toString(i.value());
        ba += valueString;
        delete[] valueString;
        ba += ", ";
    }
    ba += ")";
    return qstrdup(ba.data());
}

template<>
char *toString(const QSet<QRect> &set)
{
    QByteArray ba = "QSet<QRect>(";
    QStringList list;
    Q_FOREACH(const QRect &r, set) {
        list.append(QString("QRect(%1,%2 %3x%4)").
                    arg(r.x()).arg(r.y()).arg(r.width()).arg(r.height()));
    }
    ba += list.join(", ");
    ba += ")";
    return qstrdup(ba.data());
}

template<>
char *toString(const QSet<QString> &set)
{
    QByteArray ba = "QSet<QString>(";
    QStringList list = set.toList();
    ba += list.join(", ");
    ba += ")";
    return qstrdup(ba.data());
}

template<>
char *toString(const QSet<int> &set)
{
    QByteArray ba = "QSet<int>(";
    QStringList list;
    Q_FOREACH(int i, set) {
        list.append(QString::number(i));
    }
    ba += list.join(", ");
    ba += ")";
    return qstrdup(ba.data());
}

template<>
char *toString(const QVariantMap &p)
{
    QJsonDocument doc(QJsonObject::fromVariantMap(p));
    QByteArray ba = doc.toJson(QJsonDocument::Compact);
    return qstrdup(ba.data());
}

template<>
char *toString(const QJsonObject &p)
{
    QJsonDocument doc(p);
    QByteArray ba = doc.toJson(QJsonDocument::Compact);
    return qstrdup(ba.data());
}

template<>
char *toString(const QMap<QString,int> &p)
{
    QVariantMap variantMap;

    for (auto i = p.constBegin(); i != p.constEnd(); i++) {
        variantMap.insert(i.key(), i.value());
    }
    return toString(variantMap);
}

} // QTest namespace

#endif // IMAGINARIO_TEST_UTILS_H
