/*
 * Copyright (C) 2018 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "fake_program_finder.h"

using namespace Imaginario;

static ProgramFinderPrivate *m_privateInstance = 0;

ProgramFinderPrivate::ProgramFinderPrivate(ProgramFinder *q):
    QObject(),
    m_preserveDPtr(!q),
    q_ptr(q)
{
    m_privateInstance = this;
}

ProgramFinderPrivate::~ProgramFinderPrivate()
{
    m_privateInstance = 0;
}

ProgramFinder::ProgramFinder()
{
    if (m_privateInstance) {
        d_ptr.reset(m_privateInstance);
        m_privateInstance->q_ptr = this;
    } else {
        d_ptr.reset(new ProgramFinderPrivate(this));
    }
}

ProgramFinder::~ProgramFinder()
{
    if (d_ptr->m_preserveDPtr) {
        // Steal the d_ptr, so that it won't be deleted
        d_ptr.take();
    }
}

QJsonObject ProgramFinder::find(const QString &programName) const
{
    Q_D(const ProgramFinder);
    Q_EMIT const_cast<ProgramFinderPrivate*>(d)->findCalled(programName);
    return d->m_responses.value(programName);
}
