/*
 * Copyright (C) 2015 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef FAKE_THUMBNAILER_H
#define FAKE_THUMBNAILER_H

#include "thumbnailer.h"

#include <QHash>
#include <QObject>
#include <QUrl>

namespace Imaginario {

class ThumbnailerPrivate: public QObject
{
    Q_OBJECT
    Q_DECLARE_PUBLIC(Thumbnailer)

public:
    ThumbnailerPrivate();
    ~ThumbnailerPrivate();
    static ThumbnailerPrivate *mocked(Thumbnailer *o) { return o->d_ptr; }

    void setLoadReply(const QUrl &uri, const QImage &thumbnail) {
        m_loadReply[uri] = thumbnail;
    }
    void setCreateReply(const QUrl &url, bool success) {
        m_createReplies[url] = success;
    }

private:
    ThumbnailerPrivate(Thumbnailer *q):
        q_ptr(q)
    {}

Q_SIGNALS:
    void loadCalled(QUrl uri, QSize requestedSize);
    void createCalled(QUrl uri);

private:
    QHash<QUrl,bool> m_createReplies;
    QHash<QUrl,QImage> m_loadReply;
    mutable Thumbnailer *q_ptr;
};

} // namespace

#endif // FAKE_THUMBNAILER_H
