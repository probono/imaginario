/*
 * Copyright (C) 2016 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "utils.h"

#include <QTemporaryDir>
#include <QTest>

using namespace Imaginario;

class UtilsTest: public QObject
{
    Q_OBJECT

public:
    UtilsTest();

private Q_SLOTS:
    void testFindFiles_data();
    void testFindFiles();
    void testParseListTag_data();
    void testParseListTag();

private:
    QDir m_dataDir;
};

UtilsTest::UtilsTest():
    m_dataDir(QString(ITEMS_DIR "/"))
{
}

void UtilsTest::testFindFiles_data()
{
    QTest::addColumn<bool>("recursive");
    QTest::addColumn<QStringList>("expectedFiles");

    QTest::newRow("non recursive") <<
        false <<
        QStringList { "a.jpg", "b.jpg", "c.jpg", "z.jpg" };

    QTest::newRow("recursive") <<
        true <<
        QStringList { "a.jpg", "b.jpg", "c.jpg",
            "subdir/1.jpg",
            "subdir/2.jpg",
            "subdir/3.jpg",
            "z.jpg"
        };
}

void UtilsTest::testFindFiles()
{
    QFETCH(bool, recursive);
    QFETCH(QStringList, expectedFiles);

    QTemporaryDir tmpDir;
    QDir dest(tmpDir.path());

    QString fileName = m_dataDir.filePath("image0.png");
    QFile::copy(fileName, dest.filePath("a.jpg"));
    QFile::copy(fileName, dest.filePath("b.jpg"));
    QFile::copy(fileName, dest.filePath("c.jpg"));
    QFile::copy(fileName, dest.filePath("z.jpg"));
    dest.mkdir("subdir");
    QFile::copy(fileName, dest.filePath("subdir/1.jpg"));
    QFile::copy(fileName, dest.filePath("subdir/2.jpg"));
    QFile::copy(fileName, dest.filePath("subdir/3.jpg"));

    Utils utils;
    auto result =
        utils.findFiles(QUrl::fromLocalFile(tmpDir.path()), recursive);

    QStringList files;
    for (const QUrl &url: result) {
        files.append(dest.relativeFilePath(url.toLocalFile()));
    }

    QCOMPARE(files, expectedFiles);
}

void UtilsTest::testParseListTag_data()
{
    typedef QList<int> TagList;
    typedef QList<QList<int>> TagList2;
    QTest::addColumn<QVariant>("input");
    QTest::addColumn<TagList2>("expectedTags");

    QTest::newRow("empty") <<
        QVariant() <<
        TagList2();

    QTest::newRow("single tag") <<
        QVariant(4) <<
        TagList2 { TagList { 4 }};

    QTest::newRow("tag list") <<
        QVariant::fromValue(TagList { 4, 2, 7, 12 }) <<
        TagList2 { TagList { 4, 2, 7, 12 }};

    QTest::newRow("list of lists") <<
        QVariant::fromValue(TagList2{
                            TagList { 4, 2, 7, 12 },
                            TagList { 6 },
                            TagList { 1, 5 }
                            }) <<
        TagList2 {
            TagList { 4, 2, 7, 12 },
            TagList { 6 },
            TagList { 1, 5 }
        };
}

void UtilsTest::testParseListTag()
{
    typedef QList<int> TagList;
    typedef QList<QList<int>> TagList2;

    QFETCH(QVariant, input);
    QFETCH(TagList2, expectedTags);

    bool ok = true;
    QList<QList<Tag>> result = parseList<QList<Tag>>(input, &ok);
    QVERIFY(ok);

    TagList2 tags;
    Q_FOREACH(const QList<Tag> &tl, result) {
        TagList intList;
        Q_FOREACH(Tag t, tl) {
            intList.append(t.id());
        }
        tags.append(intList);
    }
    QCOMPARE(tags, expectedTags);
}

QTEST_GUILESS_MAIN(UtilsTest)

#include "tst_utils.moc"
