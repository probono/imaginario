TARGET = tst_face_detector

include(tests.pri)

QT += \
    core

LIBS += -lopencv_core -lopencv_highgui -lopencv_imgproc -lopencv_objdetect

ITEMS_DIR = $${TOP_SRC_DIR}/tests/data
DEFINES += \
    DATA_DIR=\\\"$${DATA_DIR}\\\" \
    ITEMS_DIR=\\\"$${ITEMS_DIR}\\\"

SOURCES += \
    $${SRC_DIR}/face_detector.cpp \
    tst_face_detector.cpp

HEADERS += \
    $${SRC_DIR}/face_detector.h \
    test_utils.h
