TARGET = tst_image_provider

include(tests.pri)

QT += \
    gui \
    quick

SOURCES += \
    $${SRC_DIR}/image_provider.cpp \
    tst_image_provider.cpp

HEADERS += \
    $${SRC_DIR}/image_provider.h \
    $${SRC_DIR}/job_executor.h \
    $${SRC_DIR}/metadata.h \
    $${SRC_DIR}/thumbnailer.h

ITEMS_DIR = $${TOP_SRC_DIR}/tests/data
DEFINES += \
    ITEMS_DIR=\\\"$${ITEMS_DIR}\\\"
