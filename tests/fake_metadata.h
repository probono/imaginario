/*
 * Copyright (C) 2015 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef FAKE_METADATA_H
#define FAKE_METADATA_H

#include "metadata.h"

#include <QHash>

namespace Imaginario {

class MetadataPrivate: public QObject
{
    Q_OBJECT
    Q_DECLARE_PUBLIC(Metadata)

    struct PreviewData {
        PreviewData(const QImage &preview, const QSize &size):
            preview(preview), size(size) {}
        PreviewData() {}
        QImage preview;
        QSize size;
    };

public:
    MetadataPrivate();
    ~MetadataPrivate();
    static MetadataPrivate *mocked(Metadata *o) { return o->d_ptr; }
    void setImportData(const QString &file,
                       const Metadata::ImportData &data) {
        m_importData[file] = data;
    }

    void setPreview(const QString &file, const QImage &preview,
                    const QSize &size) {
        m_preview[file] = PreviewData(preview, size);
    }

private:
    MetadataPrivate(Metadata *q):
        m_embed(false),
        q_ptr(q)
    {}

Q_SIGNALS:
    void instanceCreated(Metadata *instance);
    void writeChangesCalled(QString file, Metadata::Changes changes);
    void loadPreviewCalled(QString file, QSize requestedSize);

private:
    bool m_embed;
    QHash<QString,Metadata::ImportData> m_importData;
    QHash<QString,PreviewData> m_preview;
    mutable Metadata *q_ptr;
};

} // namespace

Q_DECLARE_METATYPE(Imaginario::Metadata::ImportData)

#endif // FAKE_METADATA_H
