TARGET = tst_program_finder

include(tests.pri)

SOURCES += \
    $${SRC_DIR}/program_finder.cpp \
    tst_program_finder.cpp

HEADERS += \
    $${SRC_DIR}/program_finder.h

DEFINES += \
    DATA_DIR=\\\"$${TEST_DATA_DIR}\\\"

check.commands = "LANGUAGE=es_ES LANG=en_US.UTF-8 ./$${TARGET}"
