/*
 * Copyright (C) 2016 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "icon_thumbnailer.h"

#include <QCryptographicHash>
#include <QDir>
#include <QSize>
#include <QTemporaryDir>
#include <QTest>

using namespace Imaginario;

class IconThumbnailerTest: public QObject
{
    Q_OBJECT

public:
    IconThumbnailerTest();

    void populateCache(int size);

private Q_SLOTS:
    void testLoad_data();
    void testLoad();

private:
    QDir m_dataDir;
};

IconThumbnailerTest::IconThumbnailerTest():
    m_dataDir(QString(ITEMS_DIR "/"))
{
}

void IconThumbnailerTest::populateCache(int size)
{
    IconThumbnailer t(size);

    QStringList fileNames {
        "image0.png", "image1.jpg", "image2.jpg", "image3.png", "image4.jpg",
        "image5.jpg", "image6.jpg"
    };

    Q_FOREACH(const QString &fileName, fileNames) {
        t.load(QUrl::fromLocalFile(m_dataDir.filePath(fileName)));
    }
}

void IconThumbnailerTest::testLoad_data()
{
    QTest::addColumn<QUrl>("uri");
    QTest::addColumn<int>("requestedSize");
    QTest::addColumn<QSize>("expectedSize");

    QTest::newRow("invalid URI") <<
        QUrl("http?:don't think so") <<
        32 <<
        QSize(-1, -1);

    QTest::newRow("nonexisting file") <<
        QUrl("file:///etc/do you really have a file with this name?") <<
        32 <<
        QSize(-1, -1);

    QTest::newRow("landscape") <<
        QUrl::fromLocalFile(m_dataDir.filePath("image5.jpg")) <<
        32 <<
        QSize(32, 32);

    QTest::newRow("portrait") <<
        QUrl::fromLocalFile(m_dataDir.filePath("image6.jpg")) <<
        32 <<
        QSize(32, 32);

    QTest::newRow("size 48") <<
        QUrl::fromLocalFile(m_dataDir.filePath("image6.jpg")) <<
        48 <<
        QSize(48, 48);
}

void IconThumbnailerTest::testLoad()
{
    QFETCH(QUrl, uri);
    QFETCH(int, requestedSize);
    QFETCH(QSize, expectedSize);

    QTemporaryDir tmpDir;
    qputenv("XDG_CACHE_HOME", tmpDir.path().toUtf8());
    populateCache(requestedSize);

    IconThumbnailer thumbnailer(requestedSize);
    QImage t = thumbnailer.load(uri);

    if (expectedSize.isValid()) {
        QCOMPARE(t.size(), expectedSize);

        QByteArray ba = QCryptographicHash::hash(uri.toEncoded(),
                                                 QCryptographicHash::Md5);
        QString fileName = QString::fromLatin1(ba.toHex()) + ".png";
        QString subDir =
            QString("/tst_icon_thumbnailer/tag_icons_%1/").arg(requestedSize);
        QFileInfo fileInfo(tmpDir.path() + subDir + fileName);
        /*Check if the created file is proper */
        QImage cachedImage(fileInfo.filePath());
        QVERIFY(!cachedImage.isNull());
    } else {
        QVERIFY(t.isNull());
    }
}

QTEST_GUILESS_MAIN(IconThumbnailerTest)

#include "tst_icon_thumbnailer.moc"
