/*
 * Copyright (C) 2015 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "fake_database.h"

#include <QDebug>

using namespace Imaginario;

static Database *m_instance = 0;

PhotoId DatabasePrivate::addPhoto(const Database::Photo &photo, RollId roll) {
    Q_Q(Database);

    Database::Photo p(photo);
    p.d->id = m_photos.count();
    p.d->rollId = roll;
    m_photos.insert(p.id(), p);
    Q_EMIT addPhotoCalled(photo, roll);
    Q_EMIT q->photoAdded(p.id());
    return p.id();
}

QList<Database::Photo> DatabasePrivate::findPhotos(SearchFilters filters,
                                                   Qt::SortOrder sort) {
    Q_EMIT findPhotosCalled(filters, sort);
    return m_photos.values();
}

Database::Database(QObject *parent):
    AbstractDatabase(new DatabasePrivate(this), parent)
{
    qRegisterMetaType<SearchFilters>();
    qRegisterMetaType<Qt::SortOrder>();
}

Database::~Database()
{
    m_instance = 0;
}

Database *Database::instance()
{
    if (!m_instance) {
        m_instance = new Database;
    }
    return m_instance;
}

RollId Database::createRoll(const QDateTime &time)
{
    Q_D(Database);
    d->m_lastRollId++;
    d->m_rolls.insert(d->m_lastRollId, DatabasePrivate::RollData(time));
    Q_EMIT d->createRollCalled();
    Q_EMIT rollAdded(Roll(d->m_lastRollId));
    return d->m_lastRollId;
}

QList<Roll> Database::rolls(int limit)
{
    Q_D(const Database);
    QList<Roll> ret;
    Q_FOREACH(RollId id, d->m_rolls.keys().mid(0, limit)) {
        ret.append(Roll(id));
    }
    return ret;
}

PhotoId Database::addPhoto(const Photo &photo, RollId roll)
{
    Q_D(Database);
    return d->addPhoto(photo, roll);
}

Database::Photo Database::photo(PhotoId photoId)
{
    Q_D(Database);
    return d->m_photos[photoId];
}

bool Database::deletePhoto(PhotoId photoId)
{
    Q_D(Database);
    d->m_photos.remove(photoId);
    Q_EMIT photoDeleted(photoId);
    return true;
}

bool Database::makeVersion(PhotoId photoId, PhotoId master)
{
    Q_D(Database);
    Q_EMIT d->makeVersionCalled(photoId, master);
    return true;
}

bool Database::addTags(PhotoId photo, const QList<Tag> &tags)
{
    Q_D(Database);
    TagAreas &photoTags = d->m_photoTags[photo];
    Q_FOREACH(Tag t, tags) {
        photoTags.insert(t, QRectF());
    }
    d->addTagsCalled(photo, tags);
    return true;
}

bool Database::addTagWithArea(PhotoId photo, Tag tag, const QRectF &area)
{
    Q_D(Database);
    TagAreas &photoTags = d->m_photoTags[photo];
    photoTags.insert(tag, area);
    Q_EMIT photoTagAreasChanged(photo);
    return true;
}

bool Database::setTags(PhotoId photo, const QList<Tag> &tags)
{
    Q_D(Database);
    TagAreas &photoTags = d->m_photoTags[photo];
    photoTags.clear();
    Q_FOREACH(Tag t, tags) {
        photoTags.insert(t, QRectF());
    }
    d->setTagsCalled(photo, tags);
    return true;
}

bool Database::removeTags(PhotoId photo, const QList<Tag> &tags)
{
    Q_D(Database);
    TagAreas &photoTags = d->m_photoTags[photo];
    Q_FOREACH(Tag t, tags) {
        photoTags.remove(t);
    }
    d->removeTagsCalled(photo, tags);
    return true;
}

QList<Tag> Database::tags(PhotoId photoId)
{
    Q_D(Database);
    return d->m_photoTags.value(photoId).keys();
}

QVector<TagArea> Database::tagAreas(PhotoId photoId)
{
    Q_D(Database);
    const TagAreas &photoTags = d->m_photoTags[photoId];
    QVector<TagArea> ret;
    for (TagAreas::const_iterator i = photoTags.begin();
         i != photoTags.end();
         i++) {
        if (i.value().isNull()) continue;
        ret.append(TagArea(i.value(), i.key()));
    }

    return ret;
}

int Database::computeUsageCount(Tag tag, const QList<PhotoId> &photos)
{
    Q_D(const Database);
    int count = 0;
    Q_FOREACH(PhotoId photoId, photos) {
        if (d->m_photoTags.value(photoId).contains(tag)) count++;
    }
    return count;
}

bool Database::setDescription(PhotoId photo, const QString &description)
{
    Q_D(Database);

    if (d->m_photos.contains(photo)) {
        d->m_photos[photo].setDescription(description);
    }
    Q_EMIT d->setDescriptionCalled(photo, description);
    Q_EMIT photoChanged(photo);
    return true;
}

bool Database::setRating(PhotoId photo, int rating)
{
    Q_D(Database);

    if (d->m_photos.contains(photo)) {
        d->m_photos[photo].setRating(rating);
    }
    Q_EMIT d->setRatingCalled(photo, rating);
    Q_EMIT photoChanged(photo);
    return true;
}

bool Database::setLocation(PhotoId photo, const GeoPoint &p)
{
    Q_D(Database);

    if (d->m_photos.contains(photo)) {
        d->m_photos[photo].setLocation(p);
    }
    Q_EMIT d->setLocationCalled(photo, p);
    Q_EMIT photoChanged(photo);
    return true;
}

QList<Database::Photo> Database::photos(const QList<PhotoId> &photoIds)
{
    Q_D(Database);
    Q_EMIT d->photosCalled(photoIds);
    return d->m_photos.values();
}

QList<Database::Photo> Database::findPhotos(SearchFilters filters,
                                            Qt::SortOrder sort)
{
    Q_D(Database);
    return d->findPhotos(filters, sort);
}

QList<Database::Photo> Database::photoVersions(PhotoId photoId)
{
    Q_D(Database);
    Q_EMIT d->photoVersionsCalled(photoId);
    return d->m_photos.values();
}

PhotoId Database::masterVersion(PhotoId photoId)
{
    Q_D(Database);
    Q_EMIT d->masterVersionCalled(photoId);
    return photoId;
}

Tag Database::createTag(Tag parent, const QString &name, const QString &icon)
{
    Q_D(Database);
    TagId id = d->nextTagId();
    d->m_tags.insert(id, DatabasePrivate::TagData(parent.id(),
                                                  name, icon));
    Q_EMIT d->createTagCalled(parent, name, icon);
    Tag t = tag(name);
    Q_EMIT tagCreated(t);

    // update parent
    if (parent.isValid()) {
        auto &data = d->m_tags[parent.id()];
        if (!data.isCategory) {
            data.isCategory = true;
            Q_EMIT tagChanged(parent);
        }
    }
    return t;
}

bool Database::updateTag(Tag tag, const TagUpdate &data)
{
    Q_D(Database);
    if (!d->m_tags.contains(tag.id())) return false;

    DatabasePrivate::TagData &tagData = d->m_tags[tag.id()];
    if (!data.name.isEmpty()) { tagData.name = data.name; }
    if (!data.icon.isEmpty()) { tagData.icon = data.icon; }
    if (data.parent.isValid()) {
        tagData.parent = data.parent.value<Tag>().id();
    }
    Q_EMIT tagChanged(tag);
    return true;
}

bool Database::deleteTag(Tag tag)
{
    Q_D(Database);
    if (!d->m_tags.contains(tag.id())) return false;

    d->m_tags.remove(tag.id());
    Q_EMIT tagDeleted(tag);
    return true;
}

Tag Database::tag(const QString &name)
{
    Q_D(const Database);
    QHashIterator<TagId,DatabasePrivate::TagData> i(d->m_tags);
    while (i.hasNext()) {
        i.next();
        if (i.value().name == name) {
            return Tag(i.key());
        }
    }
    return Tag();
}

QList<Tag> Database::allTags()
{
    Q_D(Database);
    QList<Tag> tags;
    for (auto i = d->m_tags.constBegin(); i != d->m_tags.constEnd(); i++) {
        tags.append(Tag(i.key()));
    }
    return tags;
}

Tag Database::uncategorizedTag() const
{
    return const_cast<Database*>(this)->tag("uncategorized");
}

Tag Database::peopleTag() const
{
    return const_cast<Database*>(this)->tag("people");
}

Tag Database::placesTag() const
{
    return const_cast<Database*>(this)->tag("places");
}

Tag Database::eventsTag() const
{
    return const_cast<Database*>(this)->tag("events");
}

Tag Database::importedTagsTag() const
{
    return const_cast<Database*>(this)->tag("importedTags");
}

QList<Tag> Database::tags(Tag parent)
{
    Q_D(Database);
    QList<Tag> children;
    for (auto i = d->m_tags.constBegin(); i != d->m_tags.constEnd(); i++) {
        if (i->parent == parent.id()) {
            children.append(Tag(i.key()));
        }
    }
    return children;
}

QList<Tag> Database::tags(const QStringList &tagNames)
{
    QList<Tag> tags;
    Q_FOREACH(const QString &tagName, tagNames) {
        Tag t = tag(tagName);
        if (Q_LIKELY(t.isValid())) {
            tags.append(t);
        }
    }
    return tags;
}

QString Tag::name() const {
    return m_instance->d_func()->m_tags[m_id].name;
}

QString Tag::icon() const {
    return m_instance->d_func()->m_tags[m_id].icon;
}

bool Tag::isCategory() const {
    return m_instance->d_func()->m_tags[m_id].isCategory;
}

Tag Tag::parent() const {
    return Tag(m_instance->d_func()->m_tags[m_id].parent);
}

float Tag::popularity() const {
    return m_instance->d_func()->m_tags[m_id].popularity;
}

int Tag::usageCount() const {
    return m_instance->d_func()->m_tags[m_id].usageCount;
}

QDateTime Roll::time() const {
    return m_instance->d_func()->m_rolls[m_id].time;
}
