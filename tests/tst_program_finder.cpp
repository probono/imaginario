/*
 * Copyright (C) 2018 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "program_finder.h"

#include "test_utils.h"

#include <QDir>
#include <QJsonObject>
#include <QTemporaryDir>
#include <QTest>

using namespace Imaginario;

class ProgramFinderTest: public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void testEmpty();
    void testFind_data();
    void testFind();
};

void ProgramFinderTest::testEmpty()
{
    QTemporaryDir tmpDir;
    qputenv("XDG_DATA_DIRS", tmpDir.path().toUtf8());
    qputenv("PATH", tmpDir.path().toUtf8());

    ProgramFinder finder;

    QCOMPARE(finder.find("edit"), QJsonObject());
}

void ProgramFinderTest::testFind_data()
{
    QTest::addColumn<QString>("name");
    QTest::addColumn<QJsonObject>("expected");

    QTest::newRow("not found") <<
        "my-app" <<
        QJsonObject();

    QTest::newRow("firefox") <<
        "firefox" <<
        QJsonObject{
            { "actionName", "Navegador web Firefox" },
            { "command", "firefox %u" },
            { "icon", "firefox" },
        };

    QTest::newRow("desktop with path") <<
        "do-stuff" <<
        QJsonObject{
            { "actionName", "Test with path" },
            { "command", "/usr/bin/do-stuff %U" },
            { "icon", "dostuff" },
        };

    QTest::newRow("desktop with version") <<
        "do-more" <<
        QJsonObject{
            { "actionName", "Test with path and version" },
            { "command", "/home/me/bin/do-more_0.1.AppImage %f" },
            { "icon", "domore" },
        };

    QDir testDir(DATA_DIR);
    QTest::newRow("executable") <<
        "just_a_helper" <<
        QJsonObject{
            { "command", testDir.absoluteFilePath("bin/just_a_helper") },
        };
}

void ProgramFinderTest::testFind()
{
    QFETCH(QString, name);
    QFETCH(QJsonObject, expected);

    QTemporaryDir tmpDir;
    qputenv("XDG_DATA_DIRS", DATA_DIR);
    qputenv("PATH", DATA_DIR "/bin");
    qputenv("HOME", tmpDir.path().toUtf8());

    ProgramFinder finder;
    QCOMPARE(finder.find(name), expected);
}

QTEST_GUILESS_MAIN(ProgramFinderTest)

#include "tst_program_finder.moc"
