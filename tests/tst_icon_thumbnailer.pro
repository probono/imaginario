TARGET = tst_icon_thumbnailer

include(tests.pri)

QT += \
    gui

SOURCES += \
    $${SRC_DIR}/icon_thumbnailer.cpp \
    tst_icon_thumbnailer.cpp

HEADERS += \
    $${SRC_DIR}/icon_thumbnailer.h

ITEMS_DIR = $${TOP_SRC_DIR}/tests/data
DEFINES += \
    ITEMS_DIR=\\\"$${ITEMS_DIR}\\\"
