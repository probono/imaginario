TEMPLATE = subdirs
SUBDIRS = \
    src

!CONFIG(nomake_tests) {
    SUBDIRS += tests
    tests.depends = src
}

click.target = click
click.depends = install
click.commands = "click build click"
QMAKE_EXTRA_TARGETS += click

include(common-config.pri)

icon.files = data/imaginario.svg

CONFIG(desktop) {
    icon.path = $${INSTALL_ICON_DIR}
    INSTALLS += icon

    QMAKE_SUBSTITUTES += data/desktop/imaginario.desktop.in
    desktop.files = data/desktop/imaginario.desktop
    desktop.path = $${INSTALL_DESKTOP_DIR}
    INSTALLS += desktop
} else {
    CLICK_DIR = $${INSTALL_PREFIX}

    icon.path = $${CLICK_DIR}
    INSTALLS += icon

    QMAKE_SUBSTITUTES += data/ubuntu-touch/imaginario.desktop.in
    desktop.files = data/ubuntu-touch/imaginario.desktop
    desktop.path = $${CLICK_DIR}
    INSTALLS += desktop

    apparmor.files = data/ubuntu-touch/imaginario.apparmor
    apparmor.path = $${CLICK_DIR}
    INSTALLS += apparmor

    contenthub.files = data/ubuntu-touch/imaginario.content-hub
    contenthub.path = $${CLICK_DIR}
    INSTALLS += contenthub

    QMAKE_SUBSTITUTES += data/ubuntu-touch/manifest.json.in
    manifest.files = data/ubuntu-touch/manifest.json
    manifest.path = $${CLICK_DIR}
    INSTALLS += manifest
}

unix {
    # specify the source files that should be included into
    # the translation file, from those files a translation
    # template is created in po/template.pot, to create a
    # translation copy the template to e.g. de.po and edit the sources
    UBUNTU_TRANSLATION_SOURCES+= \
        $$files(src/*.cpp,true) \
        $$files(src/*.qml,true) \
        $$files(src/*.js,true) \

    # specifies all translations files and makes sure they are
    # compiled and installed into the right place in the click package
    UBUNTU_PO_FILES+=$$files(po/*.po)

    UBUNTU_TRANSLATION_DOMAIN=$${APPLICATION_NAME}

    include(translations.pri)
}
