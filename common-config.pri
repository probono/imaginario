PROJECT_VERSION = 0.6

PARTS = $$split(PROJECT_VERSION, .)
count(PARTS, 2) {
    APPLICATION_NAME = "it.mardy.imaginario"
    APPLICATION_DISPLAY_NAME = "Imaginario"
} else {
    APPLICATION_NAME = "it.mardy.imaginario-dev"
    APPLICATION_DISPLAY_NAME = "Imaginario (dev)"
}

INSTALL_PREFIX=/usr

!CONFIG(desktop) {
    CONFIG(qtc) {
        INSTALL_PREFIX = /
    } else {
        INSTALL_PREFIX = $${TOP_BUILD_DIR}/click
    }
}

!isEmpty(PREFIX) {
    INSTALL_PREFIX=$${PREFIX}
}

INSTALL_BIN_DIR = $${INSTALL_PREFIX}/bin
INSTALL_LIB_DIR = $${INSTALL_PREFIX}/lib/$$system("dpkg-architecture -qDEB_HOST_MULTIARCH")
contains(INSTALL_PREFIX, "^/usr") {
    INSTALL_DATA_DIR = $${INSTALL_PREFIX}/share/imaginario
    INSTALL_ICON_DIR = $${INSTALL_PREFIX}/share/icons/hicolor/scalable/apps
    INSTALL_DESKTOP_DIR = $${INSTALL_PREFIX}/share/applications
} else:CONFIG(desktop) {
    INSTALL_DATA_DIR = $${INSTALL_PREFIX}/share
    INSTALL_ICON_DIR = $${INSTALL_DATA_DIR}
    INSTALL_DESKTOP_DIR = $${INSTALL_DATA_DIR}
} else {
    INSTALL_DATA_DIR = $${INSTALL_PREFIX}
}

unix:CLICK_ARCH = $$system("dpkg-architecture -qDEB_HOST_ARCH")
DATA_DIR = data

include(coverage.pri)
