/*
 * Copyright (C) 2014 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "abstract_database_p.h"

#include <QByteArray>
#include <QCoreApplication>
#include <QDebug>
#include <QDir>
#include <QSqlDriver>
#include <QStandardPaths>

#define FIELD_DB_VERSION "DB version"

using namespace Imaginario;

AbstractDatabasePrivate::AbstractDatabasePrivate(AbstractDatabase *q,
                                                 const QString &fileName,
                                                 const QString &connectionName):
    QObject(),
    q_ptr(q),
    m_db(QSqlDatabase::addDatabase("QSQLITE", connectionName)),
    m_inTransaction(false)
{
    QString dirName =
        QStandardPaths::writableLocation(QStandardPaths::ConfigLocation) +
        "/" + QCoreApplication::applicationName();
    QDir dir(dirName);
    dir.mkpath(".");
    m_dbPath = dir.absoluteFilePath(fileName);
    m_db.setDatabaseName(m_dbPath);
    if (!m_db.open()) {
        qWarning() << "DB open failed:" << m_db.lastError();
    }
}

AbstractDatabasePrivate::~AbstractDatabasePrivate()
{
    QString name = m_db.connectionName();
    m_db = QSqlDatabase();
    QSqlDatabase::removeDatabase(name);
}

void AbstractDatabasePrivate::emit(const char *signalName, QGenericArgument val0)
{
    Q_Q(AbstractDatabase);
    if (m_inTransaction) {
        m_queuedSignals.append(QueuedSignal(signalName, val0));
    } else {
        QMetaObject::invokeMethod(q, signalName, val0);
    }
}

bool AbstractDatabasePrivate::setDbVersion(int version)
{
    QSqlQuery q(m_db);
    q.prepare("UPDATE meta SET data = :version WHERE name = '"
              FIELD_DB_VERSION "'");
    q.bindValue(":version", version);
    q.exec();
    return !q.lastError().isValid();
}

int AbstractDatabasePrivate::dbVersion() const
{
    QSqlQuery q = exec("SELECT data FROM meta WHERE name = '"
                       FIELD_DB_VERSION "'");
    if (q.next()) {
        return q.value(0).toInt();
    } else {
        return 0;
    }
}

bool AbstractDatabasePrivate::init(int latest_db_version)
{
    bool ok = true;
    int version = dbVersion();
    if (Q_UNLIKELY(version == 0)) {
        m_db.transaction();
        ok = createEmptyDb(latest_db_version) && createDb();
        if (Q_LIKELY(ok)) {
            ok = m_db.commit();
        } else {
            qWarning() << "DB creation failed:" << m_db.lastError();
            m_db.rollback();
        }
    } else if (version != latest_db_version) {
        m_db.transaction();
        ok = updateFrom(version);
        if (Q_LIKELY(ok)) {
            ok = m_db.commit();
        } else {
            qWarning() << "DB update failed:" << m_db.lastError();
            m_db.rollback();
        }
    }

    return ok;
}

bool AbstractDatabasePrivate::createEmptyDb(int latest_db_version)
{
    exec("CREATE TABLE meta ("
         " id INTEGER PRIMARY KEY NOT NULL,"
         " name TEXT UNIQUE NOT NULL,"
         " data TEXT"
         ")");
    if (Q_UNLIKELY(m_db.lastError().isValid())) return false;

    QSqlQuery q(m_db);
    q.prepare("INSERT INTO meta (name, data) VALUES ('"
              FIELD_DB_VERSION "', :version)");
    q.bindValue(":version", latest_db_version);
    q.exec();
    if (Q_UNLIKELY(m_db.lastError().isValid())) return false;

    return true;
}

bool AbstractDatabasePrivate::updateFrom(int version)
{
    Q_UNUSED(version);
    return true;
}

AbstractDatabase::AbstractDatabase(AbstractDatabasePrivate *priv,
                                   QObject *parent):
    QObject(parent),
    d_ptr(priv)
{
}

AbstractDatabase::~AbstractDatabase()
{
    delete d_ptr;
}

bool AbstractDatabase::transaction()
{
    Q_D(AbstractDatabase);
    bool ok = d->m_db.transaction();
    if (ok) {
        d->m_inTransaction = true;
        d->m_mutex.lock();
    }
    return ok;
}

bool AbstractDatabase::commit()
{
    Q_D(AbstractDatabase);
    bool ok = d->m_db.commit();
    d->m_inTransaction = false;
    if (ok) {
        Q_FOREACH(const QueuedSignal &s, d->m_queuedSignals) {
            /* const cast, not to detach the QVariant */
            QGenericArgument val0(QMetaType::typeName(s.var0.userType()),
                                  const_cast<void*>(s.var0.constData()));
            QMetaObject::invokeMethod(this, s.signalName, val0);
        }
    }
    d->m_queuedSignals.clear();
    d->m_mutex.unlock();
    d->afterTransaction(ok);
    return ok;
}

bool AbstractDatabase::rollback()
{
    Q_D(AbstractDatabase);
    d->m_inTransaction = false;
    d->m_queuedSignals.clear();
    d->m_mutex.unlock();
    bool ok = d->m_db.rollback();
    d->afterTransaction(false);
    return ok;
}

QMutex &AbstractDatabase::mutex()
{
    Q_D(AbstractDatabase);
    return d->m_mutex;
}
