/*
 * Copyright (C) 2017 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "i18n.h"

#include <QDebug>
#include <QDir>
namespace C {
#include <libintl.h>
}

using namespace Imaginario;

GettextTranslator::GettextTranslator(QObject *parent):
    QTranslator(parent)
{
    QString appDir(getenv("APP_DIR"));
    qDebug() << "APP_DIR is" << appDir;
    if (!QDir::isAbsolutePath(appDir)) {
        appDir = "/usr";
    }
    QString localePath(QDir(appDir).filePath("share/locale"));
    C::bindtextdomain(APPLICATION_NAME, localePath.toUtf8());

    //C::bindtextdomain(APPLICATION_NAME, NULL);
    C::textdomain(APPLICATION_NAME);
    qDebug() << "domain is" << APPLICATION_NAME;
}

QString GettextTranslator::translate(const char *context,
                                     const char *sourceText,
                                     const char *disambiguation,
                                     int n) const
{
    Q_UNUSED(context);
    Q_UNUSED(disambiguation);
    return QString::fromUtf8(C::dngettext(APPLICATION_NAME, sourceText, sourceText, n));
}
