/*
 * Copyright (C) 2015 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef IMAGINARIO_UTILS_H
#define IMAGINARIO_UTILS_H

#include <QList>
#include <QMimeType>
#include <QUrl>
#include <QVariant>
#include <QVariantMap>
#include "types.h"

class QDir;
class QFileInfo;
class QJSValue;

namespace Imaginario {

class Utils: public QObject
{
    Q_OBJECT

public:
    Utils(QObject *parent = 0);
    ~Utils();

    Q_INVOKABLE QVariantMap geoFields(const GeoPoint &p) const;
    Q_INVOKABLE GeoPoint geo(const QJSValue &value) const;

    Q_INVOKABLE bool tagIsValid(Tag tag) const;
    Q_INVOKABLE QString tagName(Tag tag) const;

    Q_INVOKABLE QDateTime fileTime(const QUrl &url) const;
    Q_INVOKABLE QString fileName(const QUrl &url) const;
    Q_INVOKABLE QList<QUrl> findFiles(const QUrl &dirUrl,
                                      bool recursive) const;

    Q_INVOKABLE QMimeType invalidMimeType() const { return QMimeType(); }
};

template <typename T>
QList<T> parseList(const QVariant &variant, bool *ok = 0);

QString makeFileVersion(const QDir &destDir, const QFileInfo &fileInfo);

} // namespace

#endif // IMAGINARIO_UTILS_H
