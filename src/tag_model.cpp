/*
 * Copyright (C) 2015 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "database.h"
#include "tag_model.h"
#include "utils.h"

#include <QDebug>
#include <QHash>
#include <algorithm>

using namespace Imaginario;

namespace Imaginario {

class TagModelPrivate: public QObject
{
    Q_OBJECT
    Q_DECLARE_PUBLIC(TagModel)

public:
    TagModelPrivate(TagModel *q);
    ~TagModelPrivate();

    void queueUpdate();
    void queueComputeUsageCount();
    void sort(QList<Tag> &tags) const;
    void filterUnused(QList<Tag> &tags) const;
    QModelIndex indexOf(Tag tag);
    QModelIndex indexOfCachedTag(Tag tag, Tag *parentTagP = 0) const;
    inline QList<Tag> &childList(Tag parentTag);
    inline QList<Tag> readChildren(Tag parentTag) const;

public Q_SLOTS:
    void update();
    void onTagCreated(Tag tag);
    void onTagChanged(Tag tag);
    void onTagDeleted(Tag tag);
    void onPhotoTagsChanged(PhotoId id);
    void computeUsageCount();

private:
    QHash<int, QByteArray> m_roles;
    Database *m_db;
    bool m_updateQueued;
    bool m_computeUsageCountQueued;
    bool m_asTree;
    bool m_allTags;
    bool m_hideUnused;
    Tag m_parentTag;
    PhotoId m_photoId;
    QVariant m_photosForUsageCount;
    QList<PhotoId> m_photoList;
    QList<Tag> m_tags;
    mutable QHash<TagId,QList<Tag>> m_children;
    QHash<TagId,int> m_usageCount;
    mutable TagModel *q_ptr;
};

} // namespace

TagModelPrivate::TagModelPrivate(TagModel *q):
    m_db(Database::instance()),
    m_updateQueued(false),
    m_computeUsageCountQueued(false),
    m_asTree(true),
    m_allTags(false),
    m_hideUnused(false),
    m_photoId(-1),
    q_ptr(q)
{
    m_roles[TagModel::TagRole] = "tag";
    m_roles[TagModel::TagIdRole] = "tagId";
    m_roles[TagModel::NameRole] = "name";
    m_roles[TagModel::IconRole] = "icon";
    m_roles[TagModel::IconSourceRole] = "iconSource";
    m_roles[TagModel::PopularityRole] = "popularity";
    m_roles[TagModel::UsageCountRole] = "usageCount";
    m_roles[TagModel::ParentTagIdRole] = "parentTagId";
    m_roles[TagModel::IconAndNameRole] = "iconAndName";

    QObject::connect(m_db, SIGNAL(tagCreated(Tag)),
                     this, SLOT(onTagCreated(Tag)));
    QObject::connect(m_db, SIGNAL(tagChanged(Tag)),
                     this, SLOT(onTagChanged(Tag)));
    QObject::connect(m_db, SIGNAL(tagDeleted(Tag)),
                     this, SLOT(onTagDeleted(Tag)));
}

TagModelPrivate::~TagModelPrivate()
{
}

void TagModelPrivate::queueUpdate()
{
    if (!m_updateQueued) {
        QMetaObject::invokeMethod(this, "update", Qt::QueuedConnection);
        m_updateQueued = true;
        m_computeUsageCountQueued = true;
    }
}

void TagModelPrivate::queueComputeUsageCount()
{
    if (!m_computeUsageCountQueued) {
        QMetaObject::invokeMethod(this, "computeUsageCount",
                                  Qt::QueuedConnection);
        m_computeUsageCountQueued = true;
    }
}

void TagModelPrivate::filterUnused(QList<Tag> &tags) const
{
    QList<Tag> filtered;

    if (!m_hideUnused) return;

    Q_FOREACH(Tag tag, tags) {
        if (tag.usageCount() > 0) {
            filtered.append(tag);
        }
    }
    tags = filtered;
}

QModelIndex TagModelPrivate::indexOf(Tag tag)
{
    Q_Q(TagModel);

    if (!tag.isValid()) return QModelIndex();

    QModelIndex index = indexOfCachedTag(tag);
    if (index.isValid()) return index;

    Tag parentTag = tag.parent();
    QList<Tag> children = readChildren(parentTag);
    childList(parentTag) = children;
    return q->createIndex(children.indexOf(tag), 0, tag.id());
}

QModelIndex TagModelPrivate::indexOfCachedTag(Tag tag, Tag *parentTagP) const
{
    Q_Q(const TagModel);

    Tag parentTag;

    int row = m_tags.indexOf(tag);
    if (row >= 0) {
        if (parentTagP) *parentTagP = m_parentTag;
        return q->createIndex(row, 0, tag.id());
    }

    for (auto i = m_children.constBegin(); i != m_children.constEnd(); i++) {
        row = i.value().indexOf(tag);
        if (row >= 0) {
            parentTag = Tag(i.key());
            if (parentTagP) *parentTagP = parentTag;
            return q->createIndex(row, 0, tag.id());
        }
    }

    return QModelIndex();
}

QList<Tag> &TagModelPrivate::childList(Tag parentTag) {
    return parentTag == m_parentTag ? m_tags : m_children[parentTag.id()];
}

QList<Tag> TagModelPrivate::readChildren(Tag parentTag) const
{
    QList<Tag> children = m_db->tags(parentTag);
    filterUnused(children);
    sort(children);
    return children;
}

void TagModelPrivate::sort(QList<Tag> &tags) const
{
    std::sort(tags.begin(), tags.end(), [](Tag a, Tag b) {
        return a.name().localeAwareCompare(b.name()) < 0;
    });
}

void TagModelPrivate::update()
{
    Q_Q(TagModel);

    m_updateQueued = false;

    QObject::disconnect(m_db, SIGNAL(photoTagsChanged(PhotoId)),
                        this, SLOT(onPhotoTagsChanged(PhotoId)));
    q->beginResetModel();

    m_children.clear();

    m_asTree = false;
    if (m_allTags) {
        m_tags = m_db->allTags();
    } else if (m_photoId >= 0) {
        m_tags = m_db->tags(m_photoId);
        QObject::connect(m_db, SIGNAL(photoTagsChanged(PhotoId)),
                         this, SLOT(onPhotoTagsChanged(PhotoId)));
    } else {
        m_tags = m_db->tags(m_parentTag);
        /* The assumption that we don't need a tree-like model when a parent
         * tag is set is based on the knowledge of how our current UIs work; in
         * the future we might want to offer a separate property to control
         * this. */
        if (!m_parentTag.isValid()) {
            m_asTree = true;
        }
    }

    filterUnused(m_tags);
    sort(m_tags);
    computeUsageCount();

    q->endResetModel();
}

void TagModelPrivate::onTagCreated(Tag tag)
{
    /* If filtering by photoId, the tag will appear on the model only once
     * added to the photo */
    if (m_photoId >= 0) return;

    /* TODO: insert the tag in the right place */
    Q_UNUSED(tag);
    queueUpdate();
}

void TagModelPrivate::onTagChanged(Tag tag)
{
    Q_Q(TagModel);

    if (!m_asTree) {
        // No point in trying to be smart here
        queueUpdate();
        return;
    }

    Tag oldParentTag;
    QModelIndex index = indexOfCachedTag(tag, &oldParentTag);
    if (!index.isValid()) {
        /* Either the tag is not in the model, or it's in a node
         * which hasn't been opened yet. In either case, we don't
         * care about it. */
        return;
    }

    if (!m_allTags && oldParentTag.id() != tag.parent().id()) {
        qDebug() << "Parent was changed from" << oldParentTag.id() << "to" << tag.parent().id();
        QModelIndex newParentIndex = indexOf(tag.parent());
        QList<Tag> children = readChildren(tag.parent());
        int newRow = children.indexOf(tag);
        q->beginMoveRows(index.parent(), index.row(), index.row(),
                         newParentIndex, newRow);
        childList(tag.parent()) = children;
        QList<Tag> &oldSiblings = childList(oldParentTag);
        oldSiblings.removeOne(tag);
        q->endMoveRows();
        index = q->index(newRow, 0, newParentIndex);
    }

    Q_EMIT q->dataChanged(index, index);
}

void TagModelPrivate::onTagDeleted(Tag tag)
{
    Q_UNUSED(tag);
    /* We might want to do something smarter here */
    queueUpdate();
}

void TagModelPrivate::onPhotoTagsChanged(PhotoId id)
{
    if (id != m_photoId) return;
    queueUpdate();
}

void TagModelPrivate::computeUsageCount()
{
    m_computeUsageCountQueued = false;

    m_usageCount.clear();
    if (m_photoList.isEmpty()) return;

    Q_FOREACH(Tag tag, m_tags) {
        m_usageCount[tag.id()] =
            m_db->computeUsageCount(tag, m_photoList);
    }
}

TagModel::TagModel(QObject *parent):
    QAbstractItemModel(parent),
    d_ptr(new TagModelPrivate(this))
{
    QObject::connect(this, SIGNAL(modelReset()),
                     this, SIGNAL(countChanged()));
}

TagModel::~TagModel()
{
    delete d_ptr;
}

void TagModel::setParentTag(Tag parent)
{
    Q_D(TagModel);
    if (d->m_parentTag == parent) return;
    d->m_parentTag = parent;
    d->queueUpdate();
    Q_EMIT parentTagChanged();
}

Tag TagModel::parentTag() const
{
    Q_D(const TagModel);
    return d->m_parentTag;
}

void TagModel::setPhotoId(PhotoId photoId)
{
    Q_D(TagModel);
    if (d->m_photoId == photoId) return;
    d->m_photoId = photoId;
    d->queueUpdate();
    Q_EMIT photoIdChanged();
}

PhotoId TagModel::photoId() const
{
    Q_D(const TagModel);
    return d->m_photoId;
}

void TagModel::setAllTags(bool allTags)
{
    Q_D(TagModel);
    if (d->m_allTags == allTags) return;
    d->m_allTags = allTags;
    d->queueUpdate();
    Q_EMIT allTagsChanged();
}

bool TagModel::allTags() const
{
    Q_D(const TagModel);
    return d->m_allTags;
}

Tag TagModel::uncategorizedTag() const
{
    Q_D(const TagModel);
    return d->m_db->uncategorizedTag();
}

Tag TagModel::peopleTag() const
{
    Q_D(const TagModel);
    return d->m_db->peopleTag();
}

Tag TagModel::placesTag() const
{
    Q_D(const TagModel);
    return d->m_db->placesTag();
}

Tag TagModel::eventsTag() const
{
    Q_D(const TagModel);
    return d->m_db->eventsTag();
}

Tag TagModel::importedTagsTag() const
{
    Q_D(const TagModel);
    return d->m_db->importedTagsTag();
}

void TagModel::setPhotosForUsageCount(const QVariant &photos)
{
    Q_D(TagModel);
    d->m_photosForUsageCount = photos;
    d->m_photoList = parseList<PhotoId>(photos);
    d->queueComputeUsageCount();
    Q_EMIT photosForUsageCountChanged();
}

QVariant TagModel::photosForUsageCount() const
{
    Q_D(const TagModel);
    return d->m_photosForUsageCount;
}

void TagModel::setHideUnused(bool hide)
{
    Q_D(TagModel);

    if (hide == d->m_hideUnused) return;
    d->m_hideUnused = hide;
    d->queueUpdate();
    Q_EMIT hideUnusedChanged();
}

bool TagModel::hideUnused() const
{
    Q_D(const TagModel);
    return d->m_hideUnused;
}

Tag TagModel::tag(const QString &tagName) const
{
    Q_D(const TagModel);
    return d->m_db->tag(tagName);
}

QStringList TagModel::tagNames(const QVariant &tags) const
{
    QStringList names;
    QList<Tag> tagsList = parseList<Tag>(tags);
    Q_FOREACH(const Tag &tag, tagsList) {
        names.append(tag.name());
    }
    return names;
}

QStringList TagModel::findMatches(const QString &text) const
{
    Q_D(const TagModel);

    // remove leading spaces
    int i;
    for (i = 0; i < text.length() && text[i].isSpace(); i++);
    QStringRef cleanText = text.midRef(i);

    QStringList matches;
    Q_FOREACH(const Tag &tag, d->m_tags) {
        if (tag.name().startsWith(cleanText, Qt::CaseInsensitive)) {
            matches.append(tag.name());
        }
    }
    return matches;
}

QString TagModel::properCase(const QString &text) const
{
    Q_D(const TagModel);

    QString cleanText = text.trimmed();

    Q_FOREACH(const Tag &tag, d->m_tags) {
        if (tag.name().compare(cleanText, Qt::CaseInsensitive) == 0) {
            return tag.name();
        }
    }
    return cleanText;
}

void TagModel::classBegin()
{
    Q_D(TagModel);
    d->m_updateQueued = true;
}

void TagModel::componentComplete()
{
    Q_D(TagModel);
    d->update();
}

QVariant TagModel::get(int row, const QString &roleName) const
{
    int role = roleNames().key(roleName.toLatin1(), -1);
    return data(index(row, 0), role);
}

int TagModel::rowCount(const QModelIndex &parent) const
{
    Q_D(const TagModel);
    if (!d->m_asTree || !parent.isValid()) {
        return d->m_tags.count();
    }

    Tag parentTag(parent.internalId());

    auto it = d->m_children.find(parentTag.id());
    if (it == d->m_children.end()) {
        if (Q_UNLIKELY(!parentTag.isCategory())) {
            qWarning() << "Tag has no children!" << parentTag;
            return 0;
        }
        QList<Tag> children = d->readChildren(parentTag);
        it = d->m_children.insert(parentTag.id(), children);
    }
    const QList<Tag> &children = it.value();
    return children.count();
}

QVariant TagModel::data(const QModelIndex &index, int role) const
{
    Q_D(const TagModel);

    const Tag t(index.internalId());
    switch (role) {
    case TagRole:
        return QVariant::fromValue(t);
    case TagIdRole:
        return t.id();
    case Qt::DisplayRole:
    case NameRole:
        return t.name();
    case IconRole:
        return t.icon();
    case IconSourceRole:
        return t.iconSource();
    case PopularityRole:
        return t.popularity();
    case UsageCountRole:
        return d->m_usageCount.value(t.id(), 0);
    case ParentTagIdRole:
        return t.parent().id();
    case IconAndNameRole:
        return QString("<img src=\"%1\" width=\"16\" height=\"16\"> %2").
            arg(t.iconSource()).arg(t.name());
    default:
        qWarning() << "Unknown role ID:" << role;
        return QVariant();
    }
}

QHash<int, QByteArray> TagModel::roleNames() const
{
    Q_D(const TagModel);
    return d->m_roles;
}

bool TagModel::hasChildren(const QModelIndex &parent) const
{
    Q_D(const TagModel);
    if (d->m_asTree) {
        if (Q_UNLIKELY(!parent.isValid())) {
            return false;
        }
        Tag parentTag(parent.internalId());
        return parentTag.isCategory();
    } else {
        return false;
    }
}

int TagModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return 1;
}

QModelIndex TagModel::index(int row, int col, const QModelIndex &parent) const
{
    Q_D(const TagModel);

    Q_ASSERT(col == 0);
    if (Q_UNLIKELY(row < 0)) return QModelIndex();

    TagId tagId = -1;
    if (parent.isValid()) {
        Tag parentTag(parent.internalId());
        auto i = d->m_children.find(parentTag.id());
        if (Q_UNLIKELY(i == d->m_children.constEnd())) {
            qWarning() << "Tag has no children!" << parentTag;
            return QModelIndex();
        }
        const QList<Tag> &children = i.value();
        tagId = children[row].id();
    } else {
        tagId = d->m_tags[row].id();
    }
    return createIndex(row, col, tagId);
}

QModelIndex TagModel::parent(const QModelIndex &index) const
{
    Q_D(const TagModel);

    if (Q_UNLIKELY(!d->m_asTree || !index.isValid())) return QModelIndex();

    Tag tag(index.internalId());
    Tag parentTag;
    d->indexOfCachedTag(tag, &parentTag);
    if (!parentTag.isValid()) return QModelIndex();
    return d->indexOfCachedTag(parentTag);
}

#include "tag_model.moc"
