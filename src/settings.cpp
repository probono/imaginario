/*
 * Copyright (C) 2015 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "settings.h"

#include <QByteArray>
#include <QDebug>
#include <QJsonDocument>
#include <QSettings>
#include <QStandardPaths>

using namespace Imaginario;

namespace Imaginario {

const QString keyEmbed = QStringLiteral("Embed");
const QString keyWriteXmp = QStringLiteral("WriteXMP");
const QString keyCopyFiles = QStringLiteral("CopyFiles");
const QString keyPhotoDir = QStringLiteral("PhotoDir");
const QString keyImportParentTag = QStringLiteral("ImportParentTag");
const QString keyHiddenTags = QStringLiteral("HiddenTags");
const QString keyAutoTagData = QStringLiteral("AutoTagData");
const QString keyShowTags = QStringLiteral("ShowTags");
const QString keyShowAreaTags = QStringLiteral("ShowAreaTags");
const QString keyShowRating = QStringLiteral("ShowRating");
const QString keyHelpersData = QStringLiteral("HelpersData");

class SettingsPrivate
{
    Q_DECLARE_PUBLIC(Settings)

public:
    SettingsPrivate(Settings *q);
    ~SettingsPrivate();


private:
    QSettings m_settings;
    bool m_inUbuntuTouch;
    mutable Settings *q_ptr;
};

} // namespace

SettingsPrivate::SettingsPrivate(Settings *q):
    m_inUbuntuTouch(!qgetenv("APP_ID").isEmpty()),
    q_ptr(q)
{
}

SettingsPrivate::~SettingsPrivate()
{
}

Settings::Settings(QObject *parent):
    QObject(parent),
    d_ptr(new SettingsPrivate(this))
{
}

Settings::~Settings()
{
    delete d_ptr;
}

void Settings::setEmbed(bool embed)
{
    Q_D(Settings);
    d->m_settings.setValue(keyEmbed, embed);
    Q_EMIT embedChanged();
}

bool Settings::embed() const
{
    Q_D(const Settings);
    return d->m_settings.value(keyEmbed, d->m_inUbuntuTouch ? true : false).toBool();
}

void Settings::setWriteXmp(bool writeXmp)
{
    Q_D(Settings);
    d->m_settings.setValue(keyWriteXmp, writeXmp);
    Q_EMIT writeXmpChanged();
}

bool Settings::writeXmp() const
{
    Q_D(const Settings);
    return d->m_settings.value(keyWriteXmp, d->m_inUbuntuTouch ? true : false).toBool();
}

void Settings::setCopyFiles(bool copyFiles)
{
    Q_D(Settings);
    d->m_settings.setValue(keyCopyFiles, copyFiles);
    Q_EMIT copyFilesChanged();
}

bool Settings::copyFiles() const
{
    Q_D(const Settings);
    return d->m_settings.value(keyCopyFiles, true).toBool();
}

void Settings::setPhotoDir(const QString &photoDir)
{
    Q_D(Settings);
    d->m_settings.setValue(keyPhotoDir, photoDir);
    Q_EMIT photoDirChanged();
}

QString Settings::photoDir() const
{
    Q_D(const Settings);
    QString defaultPhotoDir = d->m_inUbuntuTouch ?
        (QStandardPaths::writableLocation(QStandardPaths::DataLocation) +
         "/Photos") :
        QStandardPaths::writableLocation(QStandardPaths::PicturesLocation);
    return d->m_settings.value(keyPhotoDir, defaultPhotoDir).toString();
}

void Settings::setHiddentags(const QStringList &tags)
{
    Q_D(Settings);
    d->m_settings.setValue(keyHiddenTags, tags);
    Q_EMIT hiddenTagsChanged();
}

QStringList Settings::hiddenTags() const
{
    Q_D(const Settings);
    return d->m_settings.value(keyHiddenTags).toStringList();
}

void Settings::setAutoTagData(const QString &data)
{
    Q_D(Settings);
    d->m_settings.setValue(keyAutoTagData, data);
    Q_EMIT autoTagDataChanged();
}

QString Settings::autoTagData() const
{
    Q_D(const Settings);
    return d->m_settings.value(keyAutoTagData).toString();
}

void Settings::setShowTags(bool show)
{
    Q_D(Settings);
    d->m_settings.setValue(keyShowTags, show);
    Q_EMIT showTagsChanged();
}

bool Settings::showTags() const
{
    Q_D(const Settings);
    return d->m_settings.value(keyShowTags, true).toBool();
}

void Settings::setShowAreaTags(bool show)
{
    Q_D(Settings);
    d->m_settings.setValue(keyShowAreaTags, show);
    Q_EMIT showAreaTagsChanged();
}

bool Settings::showAreaTags() const
{
    Q_D(const Settings);
    return d->m_settings.value(keyShowAreaTags, true).toBool();
}

void Settings::setShowRating(bool show)
{
    Q_D(Settings);
    d->m_settings.setValue(keyShowRating, show);
    Q_EMIT showRatingChanged();
}

bool Settings::showRating() const
{
    Q_D(const Settings);
    return d->m_settings.value(keyShowRating, true).toBool();
}

void Settings::setHelpers(const QJsonArray &helpers)
{
    Q_D(Settings);
    QByteArray serialized =
        QJsonDocument(helpers).toJson(QJsonDocument::Compact);
    d->m_settings.setValue(keyHelpersData, QString::fromUtf8(serialized));
    Q_EMIT helpersChanged();
}

QJsonArray Settings::helpers() const
{
    Q_D(const Settings);
    QString serializedData = d->m_settings.value(keyHelpersData).toString();
    QJsonDocument doc = QJsonDocument::fromJson(serializedData.toUtf8());
    return doc.array();
}
