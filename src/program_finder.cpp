/*
 * Copyright (C) 2018 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "program_finder.h"

#include <QDebug>
#include <QDir>
#include <QJsonObject>
#include <QLocale>
#include <QSettings>
#include <QStandardPaths>

using namespace Imaginario;

namespace Imaginario {

class ProgramFinderPrivate
{
public:
    ProgramFinderPrivate();
    ~ProgramFinderPrivate();

    void scanDesktopFiles();
    QVariant localizedValue(const QSettings &settings, const QString &key) const;
    QJsonObject find(const QString &programName) const;

private:
    mutable QHash<QString, QJsonObject> m_cache;
    QHash<QString, QString> m_desktopFileByCommand;
};

} // namespace

ProgramFinderPrivate::ProgramFinderPrivate()
{
    scanDesktopFiles();
}

ProgramFinderPrivate::~ProgramFinderPrivate()
{
}

void ProgramFinderPrivate::scanDesktopFiles()
{
    QStringList appDirs(QStandardPaths::standardLocations(
        QStandardPaths::ApplicationsLocation));

    for (const QString &appDir: appDirs) {
        QDir dir(appDir);
        QStringList entries = dir.entryList(QStringList{ "*.desktop" });
        for (const QString &fileName: entries) {
            QString filePath = dir.filePath(fileName);
            QSettings desktopFile(filePath, QSettings::IniFormat);
            desktopFile.beginGroup("Desktop Entry");
            QString command = desktopFile.value("Exec").toString();
            QString binaryName = command.split(' ').first().split('/').last();
            m_desktopFileByCommand.insert(binaryName, filePath);
            /* The binary name might include a version number; let's try also
             * splitting it out */
            int i = binaryName.indexOf('_');
            if (i > 0) {
                m_desktopFileByCommand.insert(binaryName.left(i), filePath);
            }
        }
    }
}

QVariant ProgramFinderPrivate::localizedValue(const QSettings &settings,
                                              const QString &key) const
{
    QVariant ret;

    QString localeName = QLocale::system().name();
    QString localizedKeyTemplate = QString("%1[%2]").arg(key);

    ret = settings.value(localizedKeyTemplate.arg(localeName));
    if (!ret.isNull()) return ret;

    ret = settings.value(localizedKeyTemplate.arg(localeName.split('_').
                                                  first()));
    if (!ret.isNull()) return ret;

    return settings.value(key);
}

QJsonObject ProgramFinderPrivate::find(const QString &programName) const
{
    QJsonObject ret = m_cache.value(programName);
    if (!ret.isEmpty()) return ret;

    QString desktopFilePath = m_desktopFileByCommand.value(programName);
    if (!desktopFilePath.isEmpty()) {
        QSettings desktopFile(desktopFilePath, QSettings::IniFormat);
        desktopFile.beginGroup("Desktop Entry");

        ret.insert("command", desktopFile.value("Exec").toString());
        ret.insert("actionName",
                   localizedValue(desktopFile, "Name").toString());
        ret.insert("icon", desktopFile.value("Icon").toString());
    } else {
        QString path = QStandardPaths::findExecutable(programName);
        if (!path.isEmpty()) {
            ret.insert("command", path);
        }
    }

    if (!ret.isEmpty()) {
        m_cache.insert(programName, ret);
    }

    return ret;
}

ProgramFinder::ProgramFinder():
    d_ptr(new ProgramFinderPrivate())
{
}

ProgramFinder::~ProgramFinder()
{
}

QJsonObject ProgramFinder::find(const QString &programName) const
{
    Q_D(const ProgramFinder);
    return d->find(programName);
}
