/*
 * Copyright (C) 2014 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef IMAGINARIO_ABSTRACT_DATABASE_P_H
#define IMAGINARIO_ABSTRACT_DATABASE_P_H

#include "abstract_database.h"

#include <QList>
#include <QMutex>
#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>
#include <QVariant>

namespace Imaginario {

struct QueuedSignal {
    QueuedSignal(const char *signalName, const QGenericArgument &val0):
        signalName(signalName),
        var0(QMetaType::type(val0.name()), val0.data())
    {
    }
    const char *signalName;
    QVariant var0;
};

class AbstractDatabasePrivate: public QObject
{
    Q_OBJECT
    Q_DECLARE_PUBLIC(AbstractDatabase)

public:
    AbstractDatabasePrivate(AbstractDatabase *q,
                            const QString &name,
                            const QString &connectionName =
                            QLatin1String(QSqlDatabase::defaultConnection));
    ~AbstractDatabasePrivate();

    void emit(const char *signalName, QGenericArgument val0);

    bool setDbVersion(int version);
    int dbVersion() const;

    bool init(int latest_db_version);
    bool createEmptyDb(int latest_db_version);
    virtual bool createDb() = 0;
    virtual bool updateFrom(int version);

    virtual void afterTransaction(bool succeeded) { Q_UNUSED(succeeded); }

    QSqlQuery exec(const QString &q) const { return m_db.exec(q); }
    bool execOk(const QString &q) const {
        return !m_db.exec(q).lastError().isValid();
    }

    AbstractDatabase *q_ptr;
    QString m_dbPath;
    QSqlDatabase m_db;
    QList<QueuedSignal> m_queuedSignals;
    QMutex m_mutex;
    bool m_inTransaction;
};

} // namespace

#endif // IMAGINARIO_ABSTRACT_DATABASE_P_H
