/*
 * Copyright (C) 2015 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef IMAGINARIO_TAG_MODEL_H
#define IMAGINARIO_TAG_MODEL_H

#include "types.h"

#include <QAbstractItemModel>
#include <QQmlParserStatus>
#include <QStringList>
#include <QVariant>

namespace Imaginario {

class TagModelPrivate;
class TagModel: public QAbstractItemModel, public QQmlParserStatus
{
    Q_OBJECT
    Q_ENUMS(Roles)
    Q_INTERFACES(QQmlParserStatus)
    Q_PROPERTY(int count READ rowCount NOTIFY countChanged)
    Q_PROPERTY(Tag parentTag READ parentTag WRITE setParentTag \
               NOTIFY parentTagChanged)
    Q_PROPERTY(PhotoId photoId READ photoId \
               WRITE setPhotoId NOTIFY photoIdChanged)
    Q_PROPERTY(bool allTags READ allTags WRITE setAllTags \
               NOTIFY allTagsChanged)
    Q_PROPERTY(Tag uncategorizedTag READ uncategorizedTag CONSTANT)
    Q_PROPERTY(Tag peopleTag READ peopleTag CONSTANT)
    Q_PROPERTY(Tag placesTag READ placesTag CONSTANT)
    Q_PROPERTY(Tag eventsTag READ eventsTag CONSTANT)
    Q_PROPERTY(Tag importedTagsTag READ importedTagsTag CONSTANT)
    Q_PROPERTY(QVariant photosForUsageCount READ photosForUsageCount \
               WRITE setPhotosForUsageCount NOTIFY photosForUsageCountChanged)
    Q_PROPERTY(bool hideUnused READ hideUnused \
               WRITE setHideUnused NOTIFY hideUnusedChanged)

public:
    enum Roles {
        TagRole = Qt::UserRole + 1,
        TagIdRole,
        NameRole,
        IconRole,
        IconSourceRole,
        PopularityRole,
        UsageCountRole,
        ParentTagIdRole,
        IconAndNameRole,
    };

    TagModel(QObject *parent = 0);
    ~TagModel();

    void setParentTag(Tag parent);
    Tag parentTag() const;

    void setPhotoId(PhotoId photoId);
    PhotoId photoId() const;

    void setAllTags(bool allTags);
    bool allTags() const;

    Tag uncategorizedTag() const;
    Tag peopleTag() const;
    Tag placesTag() const;
    Tag eventsTag() const;
    Tag importedTagsTag() const;

    void setPhotosForUsageCount(const QVariant &photos);
    QVariant photosForUsageCount() const;

    void setHideUnused(bool hide);
    bool hideUnused() const;

    Q_INVOKABLE Tag tag(const QString &tagName) const;
    Q_INVOKABLE QStringList tagNames(const QVariant &tags) const;

    Q_INVOKABLE QStringList findMatches(const QString &text) const;
    Q_INVOKABLE QString properCase(const QString &text) const;

    Q_INVOKABLE QVariant get(int row, const QString &role) const;

    int rowCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
    QVariant data(const QModelIndex &index,
                  int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;
    QHash<int, QByteArray> roleNames() const Q_DECL_OVERRIDE;
    bool hasChildren(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
    int columnCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
    QModelIndex index(int row, int col,
                      const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
    QModelIndex parent(const QModelIndex &index) const Q_DECL_OVERRIDE;

    void classBegin() Q_DECL_OVERRIDE;
    void componentComplete() Q_DECL_OVERRIDE;

Q_SIGNALS:
    void countChanged();
    void parentTagChanged();
    void photoIdChanged();
    void allTagsChanged();
    void photosForUsageCountChanged();
    void hideUnusedChanged();

private:
    TagModelPrivate *d_ptr;
    Q_DECLARE_PRIVATE(TagModel)
};

} // namespace

#endif // IMAGINARIO_TAG_MODEL_H
