/*
 * Copyright (C) 2016 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "database.h"
#include "photo_model.h"
#include "time_model.h"

#include <QDebug>
#include <QVector>

using namespace Imaginario;

namespace Imaginario {

class TimeModelPrivate: public QObject
{
    Q_OBJECT
    Q_DECLARE_PUBLIC(TimeModel)

public:
    TimeModelPrivate(TimeModel *q);
    ~TimeModelPrivate();

    int indexFromDate(const QDate &date) const;
    void countPhotos();

private Q_SLOTS:
    void queueUpdate();
    void update();

private:
    QHash<int, QByteArray> m_roles;
    PhotoModel *m_model;
    bool m_updateQueued;
    QDate m_date0;
    QDate m_date1;
    QList<Database::Photo> m_photos;
    QVector<int> m_counts;
    int m_maxCount;
    TimeModel *q_ptr;
};

} // namespace

TimeModelPrivate::TimeModelPrivate(TimeModel *q):
    QObject(q),
    m_model(0),
    m_updateQueued(false),
    m_maxCount(1),
    q_ptr(q)
{
    m_roles[TimeModel::TimeRole] = "time";
    m_roles[TimeModel::CountRole] = "count";
    m_roles[TimeModel::NormalizedCountRole] = "normalizedCount";
}

TimeModelPrivate::~TimeModelPrivate()
{
}

int TimeModelPrivate::indexFromDate(const QDate &date) const
{
    int firstMonth = m_date0.month() + m_date0.year() * 12;
    return date.year() * 12 + date.month() - firstMonth;
}

void TimeModelPrivate::countPhotos()
{
    m_date0 = m_date1 = QDate();
    m_maxCount = 1;
    QMap<QDate,int> months;
    bool first = true;
    for (const Database::Photo &photo: m_photos) {
        QDate date = photo.time().date();
        if (!date.isValid()) continue;

        if (first) {
            m_date0 = m_date1 = date;
            first = false;
        } else {
            if (date < m_date0) {
                m_date0 = date;
            } else if (date > m_date1) {
                m_date1 = date;
            }
        }

        // set the day to the first of the month
        date = date.addDays(1 - date.day());

        months[date]++;
    }

    int monthSpan = 0;
    if (m_date0.isValid()) {
        m_date0 = m_date0.addDays(1 - m_date0.day());
        m_date1 = m_date1.addDays(1 - m_date1.day());
        monthSpan = (m_date1.year() - m_date0.year()) * 12 +
            (m_date1.month() - m_date0.month()) + 1;
    }

    /* Go through all months, convert the map to an array and find
     * the maximum */
    m_counts = QVector<int>(monthSpan);
    for (auto i = months.constBegin(); i != months.constEnd(); i++) {
        int count = i.value();
        if (count > m_maxCount) {
            m_maxCount = count;
        }
        m_counts[indexFromDate(i.key())] = count;
    }
}

void TimeModelPrivate::queueUpdate()
{
    if (!m_updateQueued) {
        QMetaObject::invokeMethod(this, "update", Qt::QueuedConnection);
        m_updateQueued = true;
    }
}

void TimeModelPrivate::update()
{
    Q_Q(TimeModel);

    m_updateQueued = false;

    q->beginResetModel();
    m_photos = m_model->photos();
    countPhotos();
    q->endResetModel();

    Q_EMIT q->rangeChanged();
}

TimeModel::TimeModel(QObject *parent):
    QAbstractListModel(parent),
    d_ptr(new TimeModelPrivate(this))
{
    QObject::connect(this, SIGNAL(modelReset()),
                     this, SIGNAL(countChanged()));
}

TimeModel::~TimeModel()
{
    delete d_ptr;
}

void TimeModel::setPhotoModel(const QVariant &model)
{
    Q_D(TimeModel);

    PhotoModel *m = model.value<PhotoModel*>();
    if (m == d->m_model) return;

    if (d->m_model) {
        d->m_model->disconnect(d);
    }
    d->m_model = m;
    if (d->m_model) {
        QObject::connect(d->m_model, SIGNAL(modelReset()),
                         d, SLOT(queueUpdate()));
        QObject::connect(d->m_model,
                         SIGNAL(rowsRemoved(const QModelIndex&,int,int)),
                         d, SLOT(queueUpdate()));
        QObject::connect(d->m_model,
                         SIGNAL(rowsInserted(const QModelIndex&,int,int)),
                         d, SLOT(queueUpdate()));
        QObject::connect(d->m_model,
                         SIGNAL(dataChanged(const QModelIndex&,const QModelIndex&,const QVector<int>&)),
                         d, SLOT(queueUpdate()));
    }

    d->queueUpdate();
    Q_EMIT photoModelChanged();
}

QVariant TimeModel::photoModel() const
{
    Q_D(const TimeModel);
    return QVariant::fromValue(d->m_model);
}

QDate TimeModel::date0() const
{
    Q_D(const TimeModel);
    return d->m_date0;
}

QDate TimeModel::date1() const
{
    Q_D(const TimeModel);
    return d->m_date1;
}

QVariant TimeModel::get(int row, const QString &roleName) const
{
    int role = roleNames().key(roleName.toLatin1(), -1);
    return data(index(row, 0), role);
}

int TimeModel::rowCount(const QModelIndex &parent) const
{
    Q_D(const TimeModel);
    Q_UNUSED(parent);
    return d->m_counts.count();
}

QVariant TimeModel::data(const QModelIndex &index, int role) const
{
    Q_D(const TimeModel);

    int row = index.row();
    if (row < 0 || row >= d->m_counts.count()) return QVariant();

    int count = d->m_counts[row];
    switch (role) {
    case TimeRole:
        return d->m_date0.addMonths(row);
    case CountRole:
        return count;
    case NormalizedCountRole:
        return double(count) / d->m_maxCount;
    default:
        qWarning() << "Unknown role ID:" << role;
        return QVariant();
    }
}

QHash<int, QByteArray> TimeModel::roleNames() const
{
    Q_D(const TimeModel);
    return d->m_roles;
}

#include "time_model.moc"
