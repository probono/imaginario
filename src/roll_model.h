/*
 * Copyright (C) 2016 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef IMAGINARIO_ROLL_MODEL_H
#define IMAGINARIO_ROLL_MODEL_H

#include "types.h"

#include <QAbstractListModel>
#include <QDateTime>
#include <QQmlParserStatus>
#include <QVariant>

namespace Imaginario {

class RollModelPrivate;
class RollModel: public QAbstractListModel, public QQmlParserStatus
{
    Q_OBJECT
    Q_INTERFACES(QQmlParserStatus)
    Q_ENUMS(Roles)
    Q_PROPERTY(int count READ rowCount NOTIFY countChanged)
    Q_PROPERTY(int roll0 READ roll0 WRITE setRoll0 NOTIFY roll0Changed)
    Q_PROPERTY(int roll1 READ roll1 WRITE setRoll1 NOTIFY roll1Changed)
    Q_PROPERTY(int lastRollId READ lastRollId NOTIFY lastRollIdChanged)

public:
    enum Roles {
        RollIdRole = Qt::UserRole + 1,
        TimeRole,
        RollRole,
        PhotoCountRole,
    };

    RollModel(QObject *parent = 0);
    ~RollModel();

    void setRoll0(int roll);
    int roll0() const;

    void setRoll1(int roll);
    int roll1() const;

    int lastRollId() const;

    Q_INVOKABLE QVariant get(int row, const QString &role) const;

    int rowCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
    QVariant data(const QModelIndex &index,
                  int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;
    QHash<int, QByteArray> roleNames() const Q_DECL_OVERRIDE;

    void classBegin() Q_DECL_OVERRIDE;
    void componentComplete() Q_DECL_OVERRIDE;

Q_SIGNALS:
    void countChanged();
    void roll0Changed();
    void roll1Changed();
    void lastRollIdChanged();

private:
    RollModelPrivate *d_ptr;
    Q_DECLARE_PRIVATE(RollModel)
};

} // namespace

#endif // IMAGINARIO_ROLL_MODEL_H
