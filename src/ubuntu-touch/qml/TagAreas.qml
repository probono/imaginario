import Imaginario 1.0
import QtQuick 2.0
import Ubuntu.Components 1.3

Item {
    id: root

    property alias model: repeater.model
    property int photoId: -1
    property var writer: null
    property bool empty: isEmpty()

    property bool __needsSaving: false
    property var ourAreas: []

    anchors.centerIn: parent

    onPhotoIdChanged: {
        for (var i = 0; i < ourAreas.length; i++) {
            ourAreas[i].destroy(100)
        }
        ourAreas = []
    }

    Repeater {
        id: repeater
        TagArea {
            x: rect.x * root.width
            y: rect.y * root.height
            width: rect.width * root.width
            height: rect.height * root.height
            tagModel: peopleModel
            name: tag
            onChanged: root.__needsSaving = true
        }
    }

    Component {
        id: tagAreaComponent
        TagArea {
            width: units.gu(8)
            height: units.gu(8)
            tagModel: peopleModel
            onChanged: root.__needsSaving = true
        }
    }

    TagModel {
        id: peopleModel
        parentTag: peopleTag
    }

    function handlePressAndHold(point) {
        if (!contains(point)) return

        var area = tagAreaComponent.createObject(root, {
            "x": point.x - units.gu(4),
            "y": point.y - units.gu(4),
        })
        root.ourAreas.push(area)
        root.__needsSaving = true
    }

    function isEmpty() {
        for (var i = 0; i < children.length; i++) {
            var tagArea = children[i]
            if (tagArea == repeater) continue
            if (tagArea.isDeleted) continue
            return false
        }
        return true
    }

    function save() {
        if (!root.__needsSaving) return

        var regions = []
        for (var i = 0; i < children.length; i++) {
            var tagArea = children[i]
            if (tagArea == repeater) continue
            if (!tagArea.name || tagArea.isDeleted) continue

            regions.push({
                "tag": writer.createTag(tagArea.name),
                "rect": Qt.rect(tagArea.x / root.width,
                                tagArea.y / root.height,
                                tagArea.width / root.width,
                                tagArea.height / root.height)
            })
        }

        writer.setRegions(photoId, regions)
        root.__needsSaving = false
    }
}
