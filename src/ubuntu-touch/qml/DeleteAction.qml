import QtQuick 2.0
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3

Action {
    property var model
    property var selectedIndexes

    text: i18n.tr("Delete")
    iconName: "delete"
    enabled: selectedIndexes.length > 0
    onTriggered: PopupUtils.open(Qt.resolvedUrl("DeleteConfirmation.qml"), pageStack.currentPage, {
        "model": model,
        "selectedIndexes": selectedIndexes
    })
}
