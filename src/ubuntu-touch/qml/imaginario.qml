import QtQuick 2.0
import QtQuick.Window 2.2
import Ubuntu.Components 1.3
import Ubuntu.Content 1.3

Window {
    id: window

    width: units.gu(100)
    height: units.gu(75)

    property var __importerPage: null
    property bool __startedByUser: true

    MainView {
        id: mainView
        applicationName: Qt.application.name
        automaticOrientation: true
        anchors.fill: parent

        Component.onCompleted: { Theme.name = "Theme"; window.visible = true }

        PageStack {
            id: pageStack
            Component.onCompleted: pageStack.push(root)

            MainPage {
                id: root
                model: photoModel
                onImportRequested: pageStack.push(pickerPage)
                onItemClicked: pageStack.push(Qt.resolvedUrl("DisplayPage.qml"), {
                    "initialIndex": index,
                    "model": photoModel,
                })
            }

            Page {
                id: pickerPage

                visible: false
                header: PageHeader {
                    title: i18n.tr("Select source")
                }

                HubPicker {
                    onDone: pageStack.pop()
                }
            }
        }

        FilteredPhotoModel {
            id: photoModel
        }

        Connections {
            target: ContentHub

            onImportRequested: {
                console.log("Import requested: " + transfer.state)

                if (pageStack.currentPage !== __importerPage) {
                    __importerPage = pageStack.push(Qt.resolvedUrl("ImporterPage.qml"), {
                        "model": photoModel
                    })
                }
                __importerPage.handleTransfer(transfer)
            }

            onExportRequested: {
                console.log("Export requested")
                if (startupTimer.running) {
                    __startedByUser = false
                    startupTimer.stop()
                }
                pageStack.push(Qt.resolvedUrl("ExporterPage.qml"), {
                    "model": photoModel,
                    "transfer": transfer,
                    "quitWhenDone": !__startedByUser,
                })
            }
        }

        Timer {
            id: startupTimer
            interval: 1
            running: true
            repeat: false
            onTriggered: __startedByUser = true
        }
    }
}
