import Imaginario 1.0
import QtQuick 2.0
import Ubuntu.Components 1.3
import Ubuntu.Components.ListItems 1.3 as ListItem
import Ubuntu.Components.Popups 1.3

Page {
    id: root

    property var model

    header: PageHeader {
        title: i18n.tr("Filter images (%1)").arg(previewModel.count)
        flickable: flick
    }

    TagModel {
        id: tagModel
        allTags: true
        hideUnused: true
    }

    PhotoModel {
        id: previewModel
        requiredTags: tagView.selectedTags
        time0: dateChooser0.date
        time1: dateChooser1.date
        location0: areaChooser.location0
        location1: areaChooser.location1
        forbiddenTags: wShowHidden.checked ? [] : Settings.hiddenTags
    }

    Flickable {
        id: flick
        anchors.fill: parent
        contentHeight: contentItem.childrenRect.height
        implicitHeight: contentHeight

        Column {
            anchors.left: parent.left
            anchors.right: parent.right

            ListItem.Header { text: i18n.tr("Filter by tags") }

            TagSections {
                tagModel: tagModel
                onParentTagChanged: {
                    tagModel.parentTag = parentTag
                    tagModel.allTags = (selectedIndex == 0)
                }
            }

            TagView {
                id: tagView

                anchors.left: parent.left
                anchors.right: parent.right
                model: tagModel
                selectedTags: root.model.requiredTags ? root.model.requiredTags : []
                textColor: Theme.palette.normal.baseText
            }

            ListItem.Header { text: i18n.tr("Filter by date") }

            ListItem.Standard {
                text: i18n.tr("Taken after")
                control: DateChooser {
                    id: dateChooser0
                    date: root.model.time0
                    hasResetButton: true
                }
            }

            ListItem.Standard {
                text: i18n.tr("Taken before")
                control: DateChooser {
                    id: dateChooser1
                    minimum: dateChooser0.date
                    date: root.model.time1
                    hasResetButton: true
                    endOfDay: true
                }
            }

            ListItem.Header { text: i18n.tr("Filter by location") }

            ListItem.Standard {
                text: areaChooser.hasArea ? i18n.tr("Area selected") : i18n.tr("Select area")
                control: AreaChooser {
                    id: areaChooser
                    model: previewModel
                    location0: root.model.location0
                    location1: root.model.location1
                }
            }

            ListItem.Standard {
                text: i18n.tr("Show hidden tags")
                control: CheckBox {
                    id: wShowHidden
                    enabled: Settings.hiddenTags.length > 0
                    checked: root.model.forbiddenTags.length == 0 && enabled
                }
            }

            ListItem.SingleControl {
                highlightWhenPressed: false
                height: units.gu(6)
                control: Button {
                    text: i18n.tr("Apply filter")
                    anchors {
                        fill: parent
                        margins: units.gu(1)
                    }
                    onClicked: {
                        root.model.requiredTags = tagView.selectedTags
                        root.model.time0 = dateChooser0.date
                        root.model.time1 = dateChooser1.date
                        root.model.location0 = areaChooser.location0
                        root.model.location1 = areaChooser.location1
                        root.model.forbiddenTags = previewModel.forbiddenTags
                        root.model.filterName = i18n.tr("Search results")
                        root.model.hasFilter = true
                        pageStack.pop()
                    }
                }
            }
        }
    }
}
