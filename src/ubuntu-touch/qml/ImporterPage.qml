import QtQuick 2.0
import Ubuntu.Components 1.3

Page {
    id: root

    property var model: null

    title: i18n.tr("Import images")
    visible: false

    HubImporter {
        id: importer
        onDone: {
            root.model.filterName = i18n.tr("Last import roll")
            root.model.showRoll(rollId)
            pageStack.pop()
        }
    }

    function handleTransfer(transfer) {
        importer.handleTransfer(transfer)
    }
}
