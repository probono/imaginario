import Imaginario 1.0
import QtQuick 2.0
import Ubuntu.Components 1.3
import Ubuntu.Components.ListItems 1.3 as ListItem
import Ubuntu.Components.Popups 1.3

Sections {
    id: root

    property var tagModel: null
    property var parentTag: tagModel.uncategorizedTag

    property int __thirdSection: 0
    property var __thirdSections: [
        tagModel.placesTag,
        tagModel.eventsTag,
        tagModel.importedTagsTag
    ]
    property bool __ready: false

    anchors { left: parent.left; right: parent.right; margins: units.gu(1) }
    model: [
        i18n.tr("All"),
        Utils.tagName(tagModel.peopleTag),
        Utils.tagName(__thirdSections[__thirdSection]),
        i18n.tr("…")
    ]
    onSelectedIndexChanged: if (__ready) {
        var index = selectedIndex
        switch (index) {
            case 0: parentTag = tagModel.uncategorizedTag; break
            case 1: parentTag = tagModel.peopleTag; break
            case 2: parentTag = __thirdSections[__thirdSection]; break
            case 3: {
                var options = []
                for (var i = 0; i < __thirdSections.length; i++) {
                    var tag = __thirdSections[i]
                    options.push(Utils.tagName(tag))
                }
                PopupUtils.open(sectionSelectorComponent, root, {
                    "model": options,
                    "selectedIndex": __thirdSection,
                })
                return
            }
        }
    }

    Component.onCompleted: {
        if (parentTag == tagModel.peopleTag) {
            selectedIndex = 1
        } else {
            var i = __thirdSections.indexOf(parentTag)
            if (i >= 0) {
                __thirdSection = i
                selectedIndex = 2
            }
        }
        __ready = true
    }

    Component {
        id: sectionSelectorComponent

        Dialog {
            id: dialog

            property alias model: parentSelector.model
            property alias selectedIndex: parentSelector.selectedIndex
            title: i18n.tr("More categories")

            ListItem.ItemSelector {
                id: parentSelector
                text: i18n.tr("Choose category:")
            }

            Button {
                text: i18n.tr("OK")
                onClicked: {
                    root.__thirdSection = parentSelector.selectedIndex
                    root.selectedIndex = 2
                    PopupUtils.close(dialog)
                }
            }
        }
    }
}
