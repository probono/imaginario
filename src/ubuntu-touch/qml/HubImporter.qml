import Imaginario 1.0
import QtQuick 2.0
import Ubuntu.Components 1.3
import Ubuntu.Components.ListItems 1.3 as ListItem
import Ubuntu.Components.Popups 1.3
import Ubuntu.Content 1.3
import "Ubuntu.js" as Ubuntu

ListView {
    id: root

    signal importRequested
    signal done(int rollId)

    property int __rollId: -1
    property int __fixedItemCount: 0
    property var __autoTagData: ({})

    anchors.fill: parent
    spacing: units.gu(1)
    header: ListItem.SingleControl {
        height: units.gu(6)
        control: Button {
            anchors { fill: parent; margins: units.gu(1) }
            text: i18n.tr("Import %1 items").arg(importer.count)
            onClicked: {
                console.log("Starting import")
                __fixedItemCount = importer.count
                root.state = "importing"
                importer.exec()
            }
        }
    }

    model: importer
    delegate: ImportItemDelegate {
        width: root.width
        height: ListView.isCurrentItem ? implicitHeight : collapsedHeight

        itemUrl: model.url
        itemStatus: model.status
        duplicates: model.duplicateList
        tags: ([].concat(model.ownTags)).concat(model.addedTags)
        selectedTags: model.currentTags

        onIgnoreItem: importer.ignoreItem(index)
        onImportAsNotDuplicate: importer.importAsNotDuplicate(index)
        onImportAsVersion: importer.importAsVersion(index, photoId)
        onImportReplacing: importer.importReplacing(index, photoId)
        onSelectedTagsChanged: importer.setItemTags(index, selectedTags)

        MouseArea {
            anchors.fill: parent
            onPressed: { root.currentIndex = index; mouse.accepted = false }
        }
    }
    displaced: Transition {
        NumberAnimation { properties: "x,y"; duration: 1000 }
    }
    remove: Transition {
        NumberAnimation { properties: "scale"; to: 0; duration: 1000; easing.type: Easing.InBack }
    }

    states: [
        State {
            name: "importing"
            PropertyChanges {
                target: headerItem.control
                enabled: false
                text: i18n.tr("Importing item %1 of %2").arg(importer.importedCount).arg(__fixedItemCount)
            }
        }
    ]

    Component.onCompleted: {
        try {
            __autoTagData = JSON.parse(Settings.autoTagData)
        } catch (e) {}
    }

    ContentTransferHint {
        id: contentTransferHint
        anchors.fill: root
    }

    ContentPeer {
        id: contentPeer
        onAppIdChanged: {
            // remember this app
            var autoTagData = {}
            try {
                autoTagData = JSON.parse(Settings.autoTagData)
            } catch (e) {}
            var shortAppId = Ubuntu.shortAppId(appId)

            // Create or update the info about this application
            var tags = []
            var importTags = false
            var name = contentPeer.name ? contentPeer.name : appId
            if (shortAppId in autoTagData) {
                tags = autoTagData[shortAppId].tags
                importTags = autoTagData[shortAppId].importTags
                name = autoTagData[shortAppId].name
            }

            autoTagData[shortAppId] = {
                "appId": appId,
                "name": name,
                "tags": tags,
                "importTags": importTags,
            }
            Settings.autoTagData = JSON.stringify(autoTagData)
        }
    }

    function handleTransfer(transfer) {
        console.log("Import requested: " + transfer.state)
        contentTransferHint.activeTransfer = transfer
        if (transfer.state === ContentTransfer.Charged) {
            transferItems(transfer)
        } else {
            transfer.stateChanged.connect(function () {
                if (transfer.state === ContentTransfer.Charged) {
                    transferItems(transfer)
                }
            })
        }
    }

    function transferItems(transfer) {
        print ("Collecting items");
        contentPeer.appId = transfer.source
        var urls = []
        for (var i = 0; i < transfer.items.length; i++) {
            var url = transfer.items[i].url
            print ("Original URL: " + url);
            urls.push(url)
        }
        var autoTags = []
        var importTags = false
        var shortAppId = Ubuntu.shortAppId(transfer.source)
        if (shortAppId in __autoTagData) {
            autoTags = __autoTagData[shortAppId].tags
            importTags = __autoTagData[shortAppId].importTags
        }
        importer.addItems(urls, autoTags, importTags)
    }

    Importer {
        id: importer
        embed: Settings.embed
        copyFiles: Settings.copyFiles
        destination: Settings.photoDir
        parentTag: tagModel.importedTagsTag
        onFinished: {
            root.state = ""
            if (__rollId < 0) {
                __rollId = rollId
            }
            checkDone()
        }
        onCountChanged: checkDone()
        autoClear: Importer.ClearDone | Importer.ClearIgnored

        function checkDone() {
            if (count == 0 && __rollId >= 0) {
                doneTimer.interval = (failedCount > 0) ? 3000 : 1000
                doneTimer.start()
            }
        }
    }

    TagModel { id: tagModel }

    Timer {
        id: doneTimer
        repeat: false; running: false
        onTriggered: root.done(__rollId)
    }
}
