import Imaginario 1.0
import QtLocation 5.0
import QtPositioning 5.3
import QtQuick 2.0
import Ubuntu.Components 1.3

Page {
    id: root

    property var area
    property alias photoModel: itemView.model

    signal done()

    header: PageHeader {
        title: i18n.tr("Define area")
        trailingActionBar.actions: [
            Action {
                iconName: "close"
                text: i18n.tr("Close map")
                onTriggered: { clearArea(); root.done() }
            },
            Action {
                iconName: "ok"
                text: i18n.tr("Confirm")
                onTriggered: { saveArea(); root.done() }
            }
        ]
    }

    Plugin {
        id: locationPlugin
        preferred: [ "nokia", "osm" ]
        PluginParameter { name: "app_id"; value: "4gpOPuEpKNJAaxknU0B1" }
        PluginParameter { name: "token"; value: "fUe83z2Tl9OgWrkhS_VDUw" }
    }

    Map {
        id: mapView
        anchors.fill: parent
        gesture.enabled: true
        center: QtPositioning.coordinate((area.location0.latitude + area.location1.latitude) / 2, (area.location0.longitude + area.location1.longitude) / 2)

        MapItemView {
            id: itemView
            delegate: MapQuickItem {
                property var _loc: Utils.geoFields(model.location)

                anchorPoint: Qt.point(sourceItem.width / 2, sourceItem.height)
                coordinate: QtPositioning.coordinate(_loc.latitude, _loc.longitude)
                sourceItem: Image {
                    opacity: 0.5
                    fillMode: Image.PreserveAspectFit
                    width: units.gu(5)
                    source: "qrc:/icons/geotag-handle"
                    sourceSize.width: width
                }
            }
        }
        Component.onCompleted: {
            // workaround for nokia plugin
            plugin = locationPlugin
        }
    }

    function clearArea() {
        area.location0 = {}
        area.location1 = {}
    }

    function saveArea() {
        var c0 = mapView.toCoordinate(Qt.point(0,0))
        var c1 = mapView.toCoordinate(Qt.point(mapView.width, mapView.height))
        area.location0 = {
            "latitude": c0.latitude,
            "longitude": c0.longitude,
        }
        area.location1 = {
            "latitude": c1.latitude,
            "longitude": c1.longitude,
        }
        console.log("Saving area: " + JSON.stringify(area))
    }
}
