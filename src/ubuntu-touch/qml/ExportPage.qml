import QtQuick 2.0
import Ubuntu.Components 1.3
import Ubuntu.Content 1.3

Page {
    id: root

    property var model
    property var selectedIndexes: []

    property var __transfer: null

    visible: false

    header: PageHeader {
        title: i18n.tr("Share items with")
    }

    ContentPeerPicker {
        handler: ContentHandler.Share
        contentType: ContentType.Pictures
        showTitle: false

        onPeerSelected: {
            __transfer = peer.request()
            if (__transfer.state === ContentTransfer.InProgress) {
                root.__exportItems()
            }
            pageStack.pop()
        }

        onCancelPressed: pageStack.pop()
    }

    Connections {
        target: root.__transfer
        onStateChanged: {
            console.log("Transfer StateChanged: " + root.__transfer.state)
            if (root.__transfer.state === ContentTransfer.InProgress) {
                root.__exportItems()
            }
        }
    }

    Component {
        id: itemComponent
        ContentItem {}
    }

    function __exportItems() {
        var items = []
        for (var i = 0; i < selectedIndexes.length; i++) {
            var url = model.get(selectedIndexes[i], "url")
            console.log("Exporting image: " + url)
            items.push(itemComponent.createObject(root, { "url": url }))
        }
        __transfer.items = items
        __transfer.state = ContentTransfer.Charged
    }
}
