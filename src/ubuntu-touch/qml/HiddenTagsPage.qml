import Imaginario 1.0
import QtQuick 2.0
import Ubuntu.Components 1.3
import Ubuntu.Components.ListItems 1.3 as ListItem

Page {
    id: root

    property var selectedTags: []

    signal done()

    header: PageHeader {
        title: i18n.tr("Hide photos by tag")
        flickable: flick
        leadingActionBar.actions: [
            Action {
                iconName: "back"
                onTriggered: {
                    root.selectedTags = tagModel.tagNames(tagView.selectedTags)
                    root.done()
                    pageStack.pop()
                }
            }
        ]
    }

    TagModel {
        id: tagModel
        allTags: true
        hideUnused: true
    }

    Flickable {
        id: flick
        anchors.fill: parent
        contentHeight: contentItem.childrenRect.height

        Column {
            anchors { left: parent.left; right: parent.right; margins: units.gu(1) }

            ListItem.Header {
                text: i18n.tr("Hide photos having these tags:")
            }

            TagView {
                id: tagView

                anchors.left: parent.left
                anchors.right: parent.right
                model: tagModel
                selectedTags: tagsFromNames(root.selectedTags)
                textColor: Theme.palette.normal.baseText
            }
        }
    }

    function tagsFromNames(names) {
        var tags = []
        for (var i = 0; i < names.length; i++) {
            tags.push(tagModel.tag(names[i]))
        }
        return tags
    }
}
