import QtQuick 2.0
import Ubuntu.Components 1.3

GridView {
    id: root

    property bool selectMode: false
    property var selectedIndexes: []

    signal itemClicked(int index)

    property int _itemMaxSize: units.gu(16)
    property int _itemsPerRow: width / units.gu(13)

    anchors.fill: parent
    cellWidth: width / _itemsPerRow
    cellHeight: cellWidth

    delegate: Item {
        width: GridView.view.cellWidth
        height: GridView.view.cellHeight

        Image {
            id: image
            source: "image://item/" + model.url.toString()
            sourceSize { width: _itemMaxSize; height: _itemMaxSize }
            anchors.fill: parent
            anchors.margins: 1
            fillMode: Image.PreserveAspectCrop
            smooth: true
            asynchronous: true
        }

        Rectangle {
            anchors.fill: image
            color: "blue"
            opacity: 0.3
            visible: root.isSelected(index)
        }

        MouseArea {
            anchors.fill: image
            onClicked: {
                if (root.selectMode) {
                    root.toggleSelection(index)
                } else {
                    root.itemClicked(index)
                }
            }
        }
    }

    onSelectModeChanged: if (!selectMode) clearSelection()
    onCountChanged: clearSelection()

    function isSelected(index) {
        return selectedIndexes.indexOf(index) >= 0
    }

    function toggleSelection(index) {
        var i = selectedIndexes.indexOf(index)
        var newList = selectedIndexes
        if (i < 0) {
            newList.push(index)
        } else {
            newList.splice(i, 1)
        }
        selectedIndexes = newList
    }

    function clearSelection() {
        selectedIndexes = []
    }

    function getSelectedIds() {
        var selectedIds = []
        for (var i = 0; i < selectedIndexes.length; i++) {
            selectedIds.push(model.get(selectedIndexes[i], "photoId"))
        }
        return selectedIds
    }
}

