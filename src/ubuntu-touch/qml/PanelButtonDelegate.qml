import QtQuick 2.0
import Ubuntu.Components 1.3

Icon {
    id: root
    anchors.top: parent.top
    anchors.bottom: parent.bottom
    width: height
    name: modelData.iconName
    source: modelData.iconSource
    visible: modelData.visible
    opacity: modelData.enabled ? 1.0 : 0.5
    color: "white"
    MouseArea {
        anchors.fill: parent
        onClicked: modelData.trigger(root)
    }
}
