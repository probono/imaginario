import QtQuick 2.0
import Ubuntu.Components 1.3

Item {
    id: root

    property alias title: statusLabel.text
    property alias message: messageLabel.text

    anchors.fill: parent
    opacity: title ? 1.0 : 0.0

    Behavior on opacity {
        NumberAnimation { duration: 1500 }
    }

    Rectangle {
        anchors.fill: parent
        color: "black"
        opacity: 0.8
        visible: messageLabel.text != ""
    }

    Rectangle {
        anchors.fill: statusLabel
        color: "black"
        opacity: 0.8
    }

    Label {
        id: statusLabel
        anchors { left: parent.left; right: parent.right; bottom: messageLabel.top; margins: units.gu(1) }
        fontSize: "large"
        horizontalAlignment: Text.AlignHCenter
    }

    Label {
        id: messageLabel
        anchors {
            left: parent.left; right: parent.right
            leftMargin: units.gu(2); rightMargin: units.gu(2)
            bottom: parent.bottom
        }
        height: parent.height / 2
        wrapMode: Text.Wrap
        horizontalAlignment: Text.AlignHCenter
    }
}
