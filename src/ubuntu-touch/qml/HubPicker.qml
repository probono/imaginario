import Ubuntu.Content 1.3

ContentPeerPicker {
    id: root

    signal done

    handler: ContentHandler.Source
    contentType: ContentType.Pictures
    showTitle: false

    onPeerSelected: {
        peer.selectionType = ContentTransfer.Multiple;
        peer.request();
        root.done()
    }

    onCancelPressed: root.done()
}
