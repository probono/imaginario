import Imaginario 1.0
import QtQuick 2.0
import Ubuntu.Components 1.3

Item {
    id: root

    property var model: null

    property bool __detectorHasRun: false
    property int __initialCount: 0

    anchors { horizontalCenter: parent.horizontalCenter; bottom: parent.bottom; margins: units.gu(8) }
    width: units.gu(30)
    height: contents.height + contents.anchors.margins * 2
    opacity: 0

    onEnabledChanged: if (enabled) {
        showMessage(i18n.tr("Press and hold to tag someone.<br/><br/>Open the menu again for the option of automatic detection of faces."), 6000)
    }

    states: [
        State {
            name: "shown"
            when: message.text || model.detecting
            PropertyChanges { target: root; opacity: 1.0 }
        }
    ]

    transitions: [
        Transition {
            id: stateTransition
            NumberAnimation { properties: "opacity"; duration: 300 }
        }
    ]

    Connections {
        target: model
        onPhotoIdChanged: {
            model.cancelDetection()
            __detectorHasRun = false
            stateTransition.enabled = false
            messageTimer.stop()
            message.text = ""
            stateTransition.enabled = true
            __initialCount = model.count
        }
        onDetectingChanged: {
            if (model.detecting) __detectorHasRun = true
            else if (__detectorHasRun && model.count == __initialCount) {
                showMessage(i18n.tr("No faces were found"), 3000)
            }
        }
    }

    Rectangle {
        anchors.fill: parent
        radius: units.gu(2)
        color: Theme.palette.normal.overlay
        opacity: 0.8
    }

    Column {
        id: contents
        anchors { left: parent.left; right: parent.right; top: parent.top; margins: units.gu(1) }
        spacing: units.gu(1)

        Label {
            id: message
            anchors { left: parent.left; right: parent.right }
            horizontalAlignment: Text.AlignHCenter
            wrapMode: Text.Wrap
            visible: text
        }

        ActivityIndicator {
            anchors.horizontalCenter: parent.horizontalCenter
            running: model.detecting
            visible: running
        }
    }

    Timer {
        id: messageTimer
        interval: 3000
        onTriggered: message.text = ""
    }

    function showMessage(text, interval) {
        message.text = text
        messageTimer.interval = interval
        messageTimer.restart()
    }
}
