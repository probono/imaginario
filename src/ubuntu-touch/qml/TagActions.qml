import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3

ActionSelectionPopover {
    id: root

    property var tagModel: null
    property int index: -1

    signal deletionRequested()

    actions: ActionList {
        Action {
            text: i18n.tr("Edit tag")
            onTriggered: pageStack.push(Qt.resolvedUrl("TagEditPage.qml"), {
                "tagModel": tagModel,
                "index": index,
            })
        }

        Action {
            text: i18n.tr("Delete tag")
            onTriggered: root.deletionRequested()
        }
    }
}
