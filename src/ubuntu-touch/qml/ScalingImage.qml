import QtQuick 2.0

Image {
    id: root

    signal loaded

    anchors.fill: parent
    asynchronous: true
    fillMode: Image.PreserveAspectFit
    opacity: 0.0
    horizontalAlignment: Image.AlignLeft
    verticalAlignment: Image.AlignTop

    states: [
        State {
            name: "loaded"
            when: root.status == Image.Ready
            PropertyChanges { target: root; opacity: 1.0 }
        }
    ]

    transitions: [
        Transition {
            to: "loaded"
            SequentialAnimation {
                NumberAnimation { properties: "opacity"; duration: 300 }
                ScriptAction { script: root.loaded() }
            }
        }
    ]
}
