/*
 * Copyright (C) 2016 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "database.h"
#include "roll_model.h"

#include <QDebug>

using namespace Imaginario;

namespace Imaginario {

class RollModelPrivate: public QObject
{
    Q_OBJECT
    Q_DECLARE_PUBLIC(RollModel)

public:
    RollModelPrivate(RollModel *q);
    ~RollModelPrivate();

    void queueUpdate();

private Q_SLOTS:
    void update();
    void onRollAdded();

private:
    QHash<int, QByteArray> m_roles;
    Database *m_db;
    bool m_updateQueued;
    int m_roll0;
    int m_roll1;
    int m_lastRollId;
    QList<Roll> m_rolls;
    RollModel *q_ptr;
};

} // namespace

RollModelPrivate::RollModelPrivate(RollModel *q):
    QObject(q),
    m_db(Database::instance()),
    m_updateQueued(false),
    m_roll0(-1),
    m_roll1(-1),
    m_lastRollId(-1),
    q_ptr(q)
{
    m_roles[RollModel::RollIdRole] = "rollId";
    m_roles[RollModel::TimeRole] = "time";
    m_roles[RollModel::RollRole] = "roll";
    m_roles[RollModel::PhotoCountRole] = "photoCount";

    QObject::connect(m_db, SIGNAL(rollAdded(Roll)),
                     this, SLOT(onRollAdded()));
}

RollModelPrivate::~RollModelPrivate()
{
}

void RollModelPrivate::queueUpdate()
{
    if (!m_updateQueued) {
        QMetaObject::invokeMethod(this, "update", Qt::QueuedConnection);
        m_updateQueued = true;
    }
}

void RollModelPrivate::update()
{
    Q_Q(RollModel);

    m_updateQueued = false;

    q->beginResetModel();
    m_rolls.clear();
    QList<Roll> allRolls = m_db->rolls();

    if (allRolls.isEmpty()) {
        m_lastRollId = -1;
    } else {
        m_lastRollId = allRolls.first().id();
    }

    if (m_roll0 == -1 && m_roll1 == -1) {
        m_rolls = allRolls;
    } else {
        for (const Roll &roll: allRolls) {
            if (roll.id() < m_roll0) continue;
            if (m_roll1 >= 0 && roll.id() > m_roll1) continue;
            m_rolls.append(roll);
        }
    }
    q->endResetModel();
}

void RollModelPrivate::onRollAdded()
{
    queueUpdate();
}

RollModel::RollModel(QObject *parent):
    QAbstractListModel(parent),
    d_ptr(new RollModelPrivate(this))
{
    QObject::connect(this, SIGNAL(modelReset()),
                     this, SIGNAL(countChanged()));
    QObject::connect(this, SIGNAL(rowsRemoved(const QModelIndex&,int,int)),
                     this, SIGNAL(countChanged()));
    QObject::connect(this, SIGNAL(rowsInserted(const QModelIndex&,int,int)),
                     this, SIGNAL(countChanged()));
    QObject::connect(this, SIGNAL(modelReset()),
                     this, SIGNAL(lastRollIdChanged()));
}

RollModel::~RollModel()
{
    delete d_ptr;
}

void RollModel::setRoll0(int roll)
{
    Q_D(RollModel);
    d->m_roll0 = roll;
    d->queueUpdate();
    Q_EMIT roll0Changed();
}

int RollModel::roll0() const
{
    Q_D(const RollModel);
    return d->m_roll0;
}

void RollModel::setRoll1(int roll)
{
    Q_D(RollModel);
    d->m_roll1 = roll;
    d->queueUpdate();
    Q_EMIT roll1Changed();
}

int RollModel::roll1() const
{
    Q_D(const RollModel);
    return d->m_roll1;
}

int RollModel::lastRollId() const
{
    Q_D(const RollModel);
    return d->m_lastRollId;
}

void RollModel::classBegin()
{
    Q_D(RollModel);
    d->m_updateQueued = true;
}

void RollModel::componentComplete()
{
    Q_D(RollModel);
    d->update();
}

QVariant RollModel::get(int row, const QString &roleName) const
{
    int role = roleNames().key(roleName.toLatin1(), -1);
    return data(index(row, 0), role);
}

int RollModel::rowCount(const QModelIndex &parent) const
{
    Q_D(const RollModel);
    Q_UNUSED(parent);
    return d->m_rolls.count();
}

QVariant RollModel::data(const QModelIndex &index, int role) const
{
    Q_D(const RollModel);

    int row = index.row();
    if (row < 0 || row >= d->m_rolls.count()) return QVariant();

    const Roll &r = d->m_rolls[row];
    switch (role) {
    case RollIdRole:
        return r.id();
    case TimeRole:
        return r.time();
    case RollRole:
        return QVariant::fromValue(r);
    case PhotoCountRole:
        {
            SearchFilters filters;
            filters.roll0 = filters.roll1 = r.id();
            return d->m_db->findPhotos(filters).count();
        }
    default:
        qWarning() << "Unknown role ID:" << role;
        return QVariant();
    }
}

QHash<int, QByteArray> RollModel::roleNames() const
{
    Q_D(const RollModel);
    return d->m_roles;
}

#include "roll_model.moc"
