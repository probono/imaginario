import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.0
import QtQuick.Window 2.2

Dialog {
    id: root

    property alias panelImplicitWidth: tabView.implicitWidth
    property alias panelImplicitHeight: tabView.implicitHeight
    property alias currentIndex: tabView.currentIndex
    default property alias data: tabView.data

    standardButtons: StandardButton.NoButton
    width: tabView.implicitWidth + 24 // workaround for QTBUG-49058

    ColumnLayout {
        TabView {
            id: tabView

            property bool canBack: __history.length > 0
            property bool canNext: useProperty("done", true, false)
            property string nextButtonLabel: usePropertyIfTrue("nextButtonLabel", qsTr("Next"))
            property bool isLast: useProperty("isLast", currentIndex == count - 1)
            property Item currentItem: null

            implicitWidth: 600 * Screen.devicePixelRatio
            implicitHeight: 400 * Screen.devicePixelRatio
            frameVisible: false
            tabsVisible: false

            property var __history: []

            onCurrentIndexChanged: updateCurrentItem()
            onCountChanged: updateCurrentItem()

            function useProperty(name, defaultValue, defaultUnloaded) {
                return currentItem ? (currentItem.hasOwnProperty(name) ? currentItem[name] : defaultValue) : defaultUnloaded
            }

            function usePropertyIfTrue(name, defaultValue, defaultUnloaded) {
                var value = useProperty(name, defaultValue, defaultUnloaded)
                return value ? value : defaultValue
            }

            function updateCurrentItem() {
                if (count > 0) {
                    currentItem = getTab(currentIndex)
                } else {
                    currentItem = null
                }
            }

            function jumpTo(index) {
                var t = __history
                __history.push(currentIndex)
                __history = t
                currentIndex = index
            }

            function back() {
                var t = __history
                currentIndex = t.pop()
                __history = t
            }

            function next() {
                var tab = getTab(currentIndex)
                if (tab.hasOwnProperty("onConfirmed")) {
                    tab.onConfirmed()
                } else {
                    if (isLast) {
                        root.close()
                    } else {
                        jumpTo(currentIndex + 1)
                    }
                }
            }
        }

        RowLayout {
            anchors { right: parent.right }

            Button {
                enabled: tabView.currentIndex == 0
                anchors.verticalCenter: parent.verticalCenter
                text: qsTr("Cancel")
                iconName: "cancel"
                onClicked: root.close()
            }

            Button {
                enabled: tabView.canBack
                anchors.verticalCenter: parent.verticalCenter
                text: qsTr("Previous")
                iconName: "back"
                onClicked: tabView.back()
            }

            Button {
                visible: !closeButton.visible
                enabled: tabView.canNext
                anchors.verticalCenter: parent.verticalCenter
                text: tabView.nextButtonLabel
                iconName: "next"
                onClicked: tabView.next()
            }

            Button {
                id: closeButton
                visible: tabView.isLast
                anchors.verticalCenter: parent.verticalCenter
                text: qsTr("Close")
                iconName: "dialog-ok"
                onClicked: tabView.next()
            }
        }
    }

    function jumpTo(index) { tabView.jumpTo(index) }
}
