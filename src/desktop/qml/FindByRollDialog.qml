import Imaginario 1.0
import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.0
import QtQuick.Window 2.2
import "."

Dialog {
    id: root

    property var photoModel: null
    property var rollModel: null

    title: qsTr("Find by rating")

    property bool _ready: false

    contentItem: ColumnLayout {
        spacing: 20

        Label {
            verticalAlignment: Text.AlignVCenter
            font.bold: true
            text: qsTr("View all pictures imported")
        }

        RowLayout {
            Layout.minimumWidth: 580 * Screen.devicePixelRatio
            ComboBox {
                id: combo
                Layout.minimumWidth: 60 * Screen.devicePixelRatio
                model: [
                    qsTr("At"),
                    qsTr("Between")
                ]
            }

            ComboBox {
                id: roll0
                Layout.fillWidth: true
                model: rollListModel
                textRole: "text"
            }

            Label {
                text: qsTr("and")
                visible: roll1.visible
            }

            ComboBox {
                id: roll1
                Layout.fillWidth: true
                visible: combo.currentIndex == 1
                model: rollListModel
                textRole: "text"
            }
        }

        Label {
            verticalAlignment: Text.AlignVCenter
            text: qsTr("Estimated photo count: %1").arg(root._ready ? root.countPhotos() : 0)
        }

        RowLayout {
            anchors { right: parent.right }

            Button {
                anchors.verticalCenter: parent.verticalCenter
                text: qsTr("Cancel")
                iconName: "cancel"
                onClicked: root.close()
            }

            Button {
                anchors.verticalCenter: parent.verticalCenter
                text: qsTr("Find")
                iconName: "dialog-ok"
                onClicked: {
                    root.save()
                    root.close()
                }
            }
        }
    }

    Component.onCompleted: {
        for (var i = 0; i < rollModel.count; i++) {
            var r = rollModel.get(i, "roll")
            var photoCount = rollModel.get(i, "photoCount")
            rollListModel.append({
                "text": qsTr("%1 (%2)").arg(Qt.formatDateTime(r.time)).arg(photoCount),
                "rollId": r.rollId,
                "count": photoCount,
            })
        }
        _ready = true
    }

    ListModel {
        id: rollListModel
        dynamicRoles: true
    }

    function countPhotos() {
        var rolls = selectedRolls()
        var rollId0 = rolls.roll0
        var rollId1 = rolls.roll1

        var total = 0
        for (var i = 0; i < rollListModel.count; i++) {
            var row = rollListModel.get(i)
            if (row.rollId < rollId0) continue
            if (rollId1 >= 0 && row.rollId > rollId1) continue
            total += row.count
        }
        return total
    }

    function selectedRoll(comboBox) {
        var row = rollListModel.get(comboBox.currentIndex)
        return row ? row.rollId : -1
    }

    function selectedRolls() {
        var rollId0 = selectedRoll(roll0)
        var rollId1 = combo.currentIndex == 1 ?
            selectedRoll(roll1) : rollId0
        if (rollId0 > rollId1) {
            var tmp = rollId1
            rollId1 = rollId0
            rollId0 = tmp
        }

        return { "roll0": rollId0, "roll1": rollId1 }
    }

    function save() {
        var rolls = selectedRolls()
        photoModel.roll0 = rolls.roll0
        photoModel.roll1 = rolls.roll1
    }
}
