import Imaginario 1.0
import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.0
import QtQuick.Window 2.2

ColumnLayout {
    id: root

    property bool done: importer.count > 0
    property var importer: null
    property var tagModel: null

    Label {
        anchors { left: parent.left; right: parent.right }
        text: qsTr("Found files:")
    }

    SplitView {
        anchors { left: parent.left; right: parent.right }
        Layout.minimumHeight: 400 * Screen.devicePixelRatio
        Layout.fillHeight: true

        ImportView {
            id: importView
            anchors { top: parent.top; bottom: parent.bottom }
            Layout.minimumWidth: 128 * Screen.devicePixelRatio
            model: importer
        }

        Image {
            anchors { top: parent.top; bottom: parent.bottom }
            Layout.fillWidth: true
            Layout.minimumWidth: 256 * Screen.devicePixelRatio
            fillMode: Image.PreserveAspectFit
            smooth: true
            source: importer.get(importView.currentIndex, "url")
            asynchronous: true
        }
    }

    TagInput {
        id: tagInput
        anchors { left: parent.left; right: parent.right }
        model: tagModel
    }

    GridLayout {
        anchors { left: parent.left; right: parent.right }
        columns: 2

        CheckBox {
            id: importTagsCtrl
            text: qsTr("Import tags")
            checked: importer.importTags
            onCheckedChanged: importer.importTags = checked
        }
    }

    function run() {
        importer.commonTags = tagInput.getTags()
        importer.exec()
    }
}
