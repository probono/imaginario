import Imaginario 1.0
import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Window 2.2

WizardDialog {
    id: root

    title: qsTr("Welcome to Imaginario")

    panelImplicitWidth: 600 * Screen.devicePixelRatio
    panelImplicitHeight: 400 * Screen.devicePixelRatio

    WizardPage {
        WelcomeWizardIntro {}
    }

    WizardPage {
        Preferences {}
    }

    WizardPage {
        WizardImporterSelect {
            fspotImporter: __fspotImporter
            shotwellImporter: __shotwellImporter
        }

        function onConfirmed() {
            var importer = item.selectedImporter
            if (importer == __fspotImporter) {
                root.jumpTo(root.currentIndex + 1)
            } else if (importer == __shotwellImporter) {
                root.jumpTo(root.currentIndex + 2)
            } else {
                root.jumpTo(root.currentIndex + 3)
            }
        }
    }

    WizardPage {
        WizardImporterFspot {
            importer: __fspotImporter
        }
        function onConfirmed() {
            root.jumpTo(root.currentIndex + 2)
        }
    }

    WizardPage {
        WizardImporterShotwell {
            importer: __shotwellImporter
        }
    }

    WizardPage {
        WelcomeWizardFinished {}
    }

    property var __fspotImporter: FspotImporter {}
    property var __shotwellImporter: ShotwellImporter {}
}
