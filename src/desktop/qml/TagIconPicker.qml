import Imaginario 1.0
import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.0
import QtQuick.Window 2.2

Dialog {
    id: root

    property var tag: null
    property string icon: ""

    title: qsTr("Choose an icon for tag %1").arg(tag.name)

    contentItem: ColumnLayout {
        Label {
            text: qsTr("From a tagged photo:")
        }

        GridView {
            id: view
            Layout.minimumHeight: 300 * Screen.devicePixelRatio
            Layout.fillHeight: true
            anchors { left: parent.left; right: parent.right }
            cellWidth: 96 * Screen.devicePixelRatio
            cellHeight: cellWidth
            clip: true
            model: PhotoModel {
                requiredTags: tag
            }
            delegate: Item {
                width: GridView.view.cellWidth
                height: GridView.view.cellHeight

                Rectangle {
                    anchors { fill: parent; margins: 1 }
                    color: parent.GridView.isCurrentItem ? palette.highlight : palette.base
                    Image {
                        anchors { fill: parent; margins: 3 }
                        source: "image://item/" + model.url
                        asynchronous: true
                        fillMode: Image.PreserveAspectCrop
                        smooth: true
                        sourceSize { width: width; height: height }
                    }

                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            view.currentIndex = index
                            root.icon = model.url
                        }
                    }
                }
            }
        }

        RowLayout {
            Layout.columnSpan: 2

            Button {
                anchors.verticalCenter: parent.verticalCenter
                text: qsTr("Cancel")
                iconName: "cancel"
                onClicked: root.reject()
            }

            Button {
                anchors.verticalCenter: parent.verticalCenter
                text: qsTr("Ok")
                iconName: "dialog-ok"
                onClicked: root.accept()
            }
        }

        SystemPalette { id: palette }
    }
}
