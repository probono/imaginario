import Imaginario 1.0
import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Window 2.2

GridView {
    id: root

    implicitWidth: 256 * Screen.devicePixelRatio
    clip: true

    property int itemSize: 128 * Screen.devicePixelRatio
    property int _itemMaxSize: 256 * Screen.devicePixelRatio
    property int _itemsPerRow: width / itemSize

    cellWidth: width / _itemsPerRow
    cellHeight: cellWidth

    delegate: Item {
        width: GridView.view.cellWidth
        height: GridView.view.cellHeight
        Rectangle {
            anchors { fill: parent; margins: 2 }
            color: "lightgrey"

            ImportDelegate {
                id: image
                anchors.fill: parent
                source: model.url
                isPair: model.isPair
                itemMaxSize: _itemMaxSize
                onClicked: root.currentIndex = index
            }

            Loader {
                id: brokenImage
                anchors.fill: parent
                active: image.brokenImage
                Component.onCompleted: setSource(Qt.resolvedUrl("ImportDelegateBroken.qml"), {
                    "itemMaxSize": _itemMaxSize,
                    "fileName": Utils.fileName(model.url),
                });
                onLoaded: image.visible = false
            }
        }
    }
}
