import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.0

Item {
    id: root

    Column {
        anchors {
            left: parent.left; right: parent.right; margins: 16
            verticalCenter: parent.verticalCenter
        }
        spacing: 24

        Label {
            anchors { left: parent.left; right: parent.right }
            text: qsTr("Welcome to Imaginario")
            font.bold: true
            horizontalAlignment: Text.AlignHCenter
        }

        Label {
            anchors { left: parent.left; right: parent.right }
            text: qsTr("Looks like this is the first time you use Imaginario.<br><br>Please take a few seconds to configure the application according to your needs and (if available) importing your existing photo database into Imaginario.")
            textFormat: Text.StyledText
            wrapMode: Text.Wrap
        }
    }
}
