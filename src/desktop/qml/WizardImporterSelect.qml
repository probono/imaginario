import QtQuick 2.5
import QtQuick.Controls 1.4

Column {
    property var fspotImporter: null
    property var shotwellImporter: null
    property var selectedImporter: importerGroup.selectedImporter()

    spacing: 8

    ExclusiveGroup {
        id: importerGroup

        function selectedImporter() {
            if (current == radioFspot) return fspotImporter
            if (current == radioShotwell) return shotwellImporter
            return null
        }
    }

    Column {
        anchors { left: parent.left; right: parent.right }
        visible: fspotImporter.count > 0

        RadioButton {
            id: radioFspot
            exclusiveGroup: importerGroup
            text: qsTr("Import photos from F-Spot")
        }

        Label {
            anchors { left: parent.left; right: parent.right; leftMargin: 25 }
            text: qsTr("It appears you have been using F-Spot before. There are <b>%1 photos</b> in your F-Spot database.", "", fspotImporter.count).arg(fspotImporter.count)
            textFormat: Text.StyledText
            wrapMode: Text.Wrap
        }
    }

    Column {
        anchors { left: parent.left; right: parent.right }
        visible: shotwellImporter.count > 0

        RadioButton {
            id: radioShotwell
            exclusiveGroup: importerGroup
            text: qsTr("Import photos from Shotwell")
        }

        Label {
            anchors { left: parent.left; right: parent.right; leftMargin: 25 }
            text: qsTr("It appears you have been using Shotwell before. There are <b>%1 photos</b> in your Shotwell database.", "", shotwellImporter.count).arg(shotwellImporter.count)
            textFormat: Text.StyledText
            wrapMode: Text.Wrap
        }
    }

    RadioButton {
        id: radioNoImport
        text: qsTr("Don't import photos from another program.")
        exclusiveGroup: importerGroup
    }

    Component.onCompleted: selectFirst()

    function selectFirst() {
        var firstButton = radioFspot.visible ? radioFspot :
            radioShotwell.visible ? radioShotwell :
            radioNoImport
        firstButton.checked = true
    }
}
