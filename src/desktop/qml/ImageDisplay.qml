import Imaginario 1.0
import QtQuick 2.5

Rectangle {
    id: root

    property int initialIndex: -1
    property alias currentIndex: display.currentIndex
    property alias model: display.model

    signal done

    property int _maxDimension: Math.max(width, height)

    color: "black"
    opacity: 0
    anchors.fill: parent
    states: [
        State {
            name: "opened"
            PropertyChanges { target: root; opacity: 1 }
        }
    ]

    transitions: [
        Transition {
            to: "opened"
            NumberAnimation { properties: "opacity"; duration: 1000 }
        },
        Transition {
            from: "opened"
            SequentialAnimation {
                NumberAnimation { properties: "opacity"; duration: 200 }
                ScriptAction { script: root.done() }
            }
        }
    ]

    Component.onCompleted: {
        display.positionViewAtIndex(initialIndex, ListView.Beginning)
    }

    function show() {
        state = "opened"
    }

    function hide() {
        state = ""
    }

    ListView {
        id: display
        anchors.fill: parent
        focus: true
        currentIndex: initialIndex
        orientation: ListView.Horizontal
        snapMode: ListView.SnapOneItem
        highlightFollowsCurrentItem: true
        highlightRangeMode: ListView.StrictlyEnforceRange
        highlightMoveDuration: 1000
        spacing: 3
        delegate: Image {
            property string title: model.title || model.fileName
            width: ListView.view.width
            height: ListView.view.height
            fillMode: Image.PreserveAspectFit
            smooth: true
            source: model.url
            asynchronous: true
            autoTransform: true
            sourceSize { width: _maxDimension; height: _maxDimension }
        }
        Keys.onSpacePressed: incrementCurrentIndex()
    }
}
