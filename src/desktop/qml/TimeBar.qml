import Imaginario 1.0
import QtQuick 2.0

Item {
    id: root

    property var photoModel: null
    property date visibleDate
    property alias date0: timeWidget.date0
    property alias date1: timeWidget.date1

    signal dateClicked(var date)

    clip: true
    onVisibleDateChanged: timeWidget.ensureCursorVisible()

    property var __datelessModel: null

    TimeWidget {
        id: timeWidget

        anchors { fill: parent; margins: 4 }
        clip: true
        contentOffset: requestedContentOffset
        monthWidth: 18
        model: timeModel

        onDateClicked: root.dateClicked(date)

        Behavior on contentOffset {
            SmoothedAnimation { duration: 500 }
        }

        Rectangle {
            id: cursor
            property real position: timeWidget.updateCounter, timeWidget.offsetForDate(root.visibleDate, TimeWidget.ApproxToNewer)
            x: position - timeWidget.contentOffset
            width: timeWidget.monthWidth
            height: parent.height
            visible: root.photoModel.count > 0
            opacity: 0.3
            color: "blue"
            Behavior on position {
                NumberAnimation { duration: 500; easing.type: Easing.InOutQuad }
            }
        }

        TimeModel {
            id: timeModel
            photoModel: __datelessModel ? __datelessModel : root.photoModel
        }

        function ensureVisible(x0, itemWidth) {
            if (x0 < contentOffset) {
                requestedContentOffset = x0
            } else if (x0 + itemWidth > contentOffset + width) {
                requestedContentOffset = x0 + itemWidth - width
            }
        }

        function ensureCursorVisible() {
            ensureVisible(offsetForDate(root.visibleDate, TimeWidget.ApproxToNewer), cursor.width)
        }
    }

    TimeArrow {
        anchors.top: parent.top
        timeBar: timeWidget
        width: 12
        restPosition: 0
        onMonthSelected: root.photoModel.time1 = date
        followedTime: root.photoModel.time1
        older: false
    }

    TimeArrow {
        anchors.bottom: parent.bottom
        timeBar: timeWidget
        restPosition: parent.width
        width: 12
        onMonthSelected: root.photoModel.time0 = date
        followedTime: root.photoModel.time0
        older: true
    }

    Connections {
        target: photoModel
        onHasTimeFilterChanged: checkTimes()
    }

    Component {
        id: datelessModelComponent
        PhotoModel {
            roll0: photoModel.roll0
            roll1: photoModel.roll1
            rating0: photoModel.rating0
            rating1: photoModel.rating1
            location0: photoModel.location0
            location1: photoModel.location1
            nonGeoTagged: photoModel.nonGeoTagged
            requiredTags: photoModel.requiredTags
            skipNonDefaultVersions: photoModel.skipNonDefaultVersions
            forbiddenTags: photoModel.forbiddenTags
        }
    }

    function checkTimes() {
        if (photoModel.hasTimeFilter) {
            // use a similar model, just with no time filters
            __datelessModel = datelessModelComponent.createObject(null)
        } else {
            var model = __datelessModel
            __datelessModel = null
            model.destroy()
        }
    }
}
