import Imaginario 1.0
import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.1

ColumnLayout {
    id: root

    property var importer: null
    property var duplicateModel: null
    property bool done: false

    spacing: 8

    Label {
        anchors { left: parent.left; right: parent.right }
        text: qsTr("Imported items: <b>%1</b>").arg(importer.importedCount)
        textFormat: Text.StyledText
        wrapMode: Text.Wrap
    }

    Label {
        anchors { left: parent.left; right: parent.right }
        text: qsTr("Duplicate items: <b>%1</b>").arg(duplicateModel.count)
        textFormat: Text.StyledText
        wrapMode: Text.Wrap
    }

    Label {
        anchors { left: parent.left; right: parent.right }
        text: qsTr("Failed items: <b>%1</b>").arg(importer.failedCount)
        textFormat: Text.StyledText
        wrapMode: Text.Wrap
    }

    Label {
        anchors { left: parent.left; right: parent.right }
        text: qsTr("Ignored items: <b>%1</b>").arg(importer.ignoredCount)
        textFormat: Text.StyledText
        wrapMode: Text.Wrap
    }

    ProgressBar {
        id: progressBar
        minimumValue: 0
        maximumValue: importer.count
        value: importer.importedCount + importer.failedCount + importer.ignoredCount + duplicateModel.count
        visible: false
    }

    Label {
        id: statusLabel
        property string name: ""
        anchors { left: parent.left; right: parent.right }
        text: qsTr("Current status: <b>%1</b>").arg(name)
        textFormat: Text.StyledText
        wrapMode: Text.Wrap
    }

    Item {
        anchors { left: parent.left; right: parent.right }
        Layout.fillHeight: true
        Label {
            id: duplicateWarning
            anchors { fill: parent; margins: 40 }
            text: qsTr("Some items appear to be already present in your photo database.\n\nIn the next page you'll be able to see them and decide how to proceed.")
            wrapMode: Text.Wrap
            visible: false
        }
    }

    states: [
        State {
            name: "running"
            when: importer.running
            PropertyChanges { target: statusLabel; name: qsTr("Importing...") }
            PropertyChanges { target: progressBar; visible: true }
        },
        State {
            name: "duplicateCheck"
            extend: "done"
            when: duplicateModel.count > 0
            PropertyChanges { target: duplicateWarning; visible: true }
        },
        State {
            name: "done"
            when: progressBar.value > 0
            PropertyChanges { target: statusLabel; name: qsTr("Import completed") }
            PropertyChanges { target: root; done: true }
        }
    ]
}
