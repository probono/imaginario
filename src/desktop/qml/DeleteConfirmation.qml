import QtQuick 2.0
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.0
import "."

Dialog {
    id: root

    property var imageView: null
    property bool deleteFromDrive: false

    title: qsTr("Delete confirmation")

    contentItem: ColumnLayout {
        Label {
            Layout.fillHeight: true
            text: (deleteFromDrive ? qsTr("Delete the %1 selected images from the disk?") : qsTr("Remove the %1 selected images from the catalogue?")).arg(imageView.selectedIndexes.length)
        }

        RowLayout {
            Button {
                anchors.verticalCenter: parent.verticalCenter
                text: qsTr("Cancel")
                iconName: "cancel"
                onClicked: root.close()
            }

            Button {
                anchors.verticalCenter: parent.verticalCenter
                text: deleteFromDrive ? qsTr("Delete photos") : qsTr("Remove photos")
                iconName: "dialog-ok"
                onClicked: {
                    if (deleteFromDrive) {
                        GlobalWriter.deletePhotos(imageView.getSelectedIds())
                    }
                    imageView.model.deletePhotos(imageView.selectedIndexes)
                    root.close()
                }
            }
        }
    }
}
