import Imaginario 1.0
import QtQuick 2.5
import QtQuick.Controls 1.4

Item {
    id: root

    property alias source: image.source
    property int itemMaxSize: 0
    property bool brokenImage: image.status == Image.Error
    property bool isPair: false

    signal clicked

    Rectangle {
        id: frame
        x: image.x + Math.ceil((image.width - image.paintedWidth) / 2) - 1
        y: image.y + Math.ceil((image.height - image.paintedHeight) / 2) - 1
        width: image.paintedWidth + 2
        height: image.paintedHeight + 2
        color: "black"
    }

    Image {
        id: image
        anchors {
            left: parent.left; right: parent.right
            top: parent.top; bottom: label.top
            margins: 1
        }
        fillMode: Image.PreserveAspectFit
        smooth: true
        asynchronous: true
        autoTransform: true
        sourceSize { width: itemMaxSize; height: itemMaxSize }
    }

    Text {
        anchors {
            right: frame.right; top: frame.top
            margins: 4
        }
        visible: isPair
        text: qsTr("RAW+edit")
        color: "white"
        font.pointSize: 6

        Rectangle {
            anchors { fill: parent; margins: -1 }
            color: "black"
            radius: 4
            z: -1
            opacity: 0.5
        }
    }

    Label {
        id: label
        anchors {
            left: parent.left; right: parent.right
            bottom: parent.bottom
            margins: 1
        }
        text: Qt.formatDate(Utils.fileTime(root.source), Qt.ISODate)
        horizontalAlignment: Text.AlignHCenter
    }

    MouseArea {
        anchors.fill: parent
        onClicked: root.clicked()
    }
}
