import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.0
import QtQuick.Window 2.2

Rectangle {
    id: root

    property var photoModel: null

    color: palette.highlight
    implicitHeight: 40 * Screen.devicePixelRatio
    visible: tagBar.filterActive || locationBar.filterActive || rollBar.filterActive || ratingBar.filterActive || areaBar.filterActive

    RowLayout {
        anchors { fill: parent; margins: 2 }

        Label {
            text: qsTr("Find:")
        }

        RatingFilterRow {
            id: ratingBar
            photoModel: root.photoModel
        }

        RollFilterRow {
            id: rollBar
            photoModel: root.photoModel
        }

        LocationFilterRow {
            id: locationBar
            photoModel: root.photoModel
        }

        AreaFilterRow {
            id: areaBar
            photoModel: root.photoModel
        }

        TagFilterRow {
            id: tagBar
            anchors { top: parent.top; bottom: parent.bottom }
            Layout.fillWidth: true
            photoModel: root.photoModel
        }

        Button {
            iconName: "cancel"
            onClicked: {
                console.log("Button clicked")
                photoModel.clearFilter()
            }
        }
    }

    SystemPalette { id: palette }
}
