import QtQuick 2.0
import "popupUtils.js" as PopupUtils
import "."

DropArea {
    id: root

    property var view: null

    signal importRequested(var urls)

    keys: [ "text/uri-list" ]

    onPositionChanged: updateDropArea(drag)
    onDropped: handleDrop(drop)

    function handleDrop(drag) {
        if (drag.source != null) {
            var index = indexAt(drag.x, drag.y)
            if (index < 0 || view.selectedIndexes.indexOf(index) >= 0) return
            var photoIds = []
            var fileNames = []
            var l = view.selectedIndexes.length
            var masterId = view.model.get(index, "photoId")
            var masterName = view.model.get(index, "fileName")
            for (var i = 0; i < l; i++) {
                var idx = view.selectedIndexes[i]
                photoIds.push(view.model.get(idx, "photoId"))
                fileNames.push(view.model.get(idx, "fileName"))
            }
            var dialog = PopupUtils.open(Qt.resolvedUrl("VersionConfirmation.qml"), root, {
                "fileNames": fileNames,
                "masterName": masterName,
            })
            dialog.confirmed.connect(function() {
                for (var i = 0; i < l; i++) {
                    var photoId = photoIds[i]
                    console.log("makeVersion: " + photoId + ", ", + masterId)
                    GlobalWriter.makeVersion(photoId, masterId)
                }
            })
        } else {
            root.importRequested(drag.urls)
        }
    }

    function updateDropArea(drag) {
        // If it's from another program, no highlight
        if (!drag.source) return

        var index = indexAt(drag.x, drag.y)
        if (index < 0) return
        view.currentIndex = index
    }

    function indexAt(x, y) {
        var pos = mapToItem(view, x + view.contentX, y + view.contentY)
        return view.indexAt(pos.x, pos.y)
    }
}
