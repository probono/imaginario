import QtQuick 2.5
import QtQuick.Controls 1.4

Item {
    id: root
    property alias fileName: label.text
    property int itemMaxSize: 16

    anchors.fill: parent

    Image {
        id: brokenImage
        anchors {
            left: parent.left; right: parent.right
            top: parent.top; bottom: label.top
            margins: 16
        }
        fillMode: Image.PreserveAspectFit
        smooth: true
        source: "qrc:/icons/broken"
        sourceSize { width: root.itemMaxSize; height: root.itemMaxSize }
    }

    Label {
        id: label
        anchors {
            left: parent.left; right: parent.right
            bottom: parent.bottom; margins: 1
        }
        horizontalAlignment: Text.AlignHCenter
        elide: Text.ElideMiddle
    }
}
