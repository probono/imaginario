import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.0

Item {
    id: root

    Column {
        anchors {
            left: parent.left; right: parent.right; margins: 16
            verticalCenter: parent.verticalCenter
        }
        spacing: 12

        Label {
            anchors { left: parent.left; right: parent.right }
            text: qsTr("Setup complete")
            font.bold: true
            horizontalAlignment: Text.AlignHCenter
        }

        Label {
            anchors { left: parent.left; right: parent.right }
            text: qsTr("Imaginario has been configured. You can close this window and start using the application.")
            textFormat: Text.StyledText
            wrapMode: Text.Wrap
        }
    }
}
