import Imaginario 1.0
import QtQuick 2.5

Item {
    id: root

    property Component delegate: null
    property alias photoId: areaModel.photoId

    property var __objects: []

    TagAreaModel {
        id: areaModel
        onModelReset: rebuildObjects()
    }

    function rebuildObjects() {
        for (var i = 0; i < __objects.length; i++) {
            __objects[i].destroy(10)
        }
        var l = areaModel.count
        for (var i = 0; i < l; i++) {
            var rect = areaModel.get(i, "rect")
            var tag = areaModel.get(i, "tag")
            var item = delegate.createObject(root, {
                "x": Qt.binding(function() { return rect.x * root.width }),
                "y": Qt.binding(function() { return rect.y * root.height }),
                "width": Qt.binding(function() { return rect.width * root.width }),
                "height": Qt.binding(function() { return rect.height * root.height }),
                "tagName": tag,
            })
            __objects.push(item)
        }
    }
}
