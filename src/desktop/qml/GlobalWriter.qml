pragma Singleton

import Imaginario 1.0

Writer {
    id: writer
    embed: Settings.embed
    writeXmp: Settings.writeXmp
}
