import Imaginario 1.0
import QtQml 2.2
import QtQuick 2.0
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.0
import "popupUtils.js" as PopupUtils
import "."

ColumnLayout {
    id: root

    property alias model: imageView.model
    property alias selectedTag: tagInputRow.parentTag
    property alias selectedIndexes: imageView.selectedIndexes

    TimeBar {
        id: timeWidget
        anchors { left: parent.left; right: parent.right }
        height: 30
        photoModel: imageView.model
        // updateCounter forces binding updates
        visibleDate: imageView.model.updateCounter, imageView.model.get(imageView.firstVisibleIndex, "time")
        date0: photoModel.time0
        date1: photoModel.time1

        onDateClicked: {
            /* find the last day of the month */
            date.setMonth(date.getMonth() + 1)
            date.setDate(0)
            var i = imageView.model.firstIndex(PhotoModel.OlderThan, date)
            imageView.positionViewAtIndex(i, GridView.Beginning)
        }
    }

    FilterBar {
        id: filterBar
        anchors { left: parent.left; right: parent.right }
        photoModel: imageView.model
        z: 1
    }

    ScrolledImageView {
        id: imageView
        Layout.fillHeight: true

        anchors { left: parent.left; right: parent.right }
        selectMode: true
        onItemActivated: root.showFullscreen()
        onRightClicked: contextMenu.popup()

        Keys.onPressed: if (event.key == Qt.Key_T) tagInputRow.show()

        Connections {
            target: GlobalWriter
            onThumbnailUpdated: {
                var i = model.indexOfItem(photoId)
                if (i >= 0) {
                    model.forceItemRefresh(i);
                }
            }
        }

        Menu {
            id: contextMenu
            property var selectedIndexes: imageView.selectedIndexes

            MenuItem {
                action: actions.copy
            }
            MenuItem {
                text: qsTr("Detach version")
                visible: selectedIndexes.length == 1 && model.get(selectedIndexes[0], "hasVersions")
                onTriggered: GlobalWriter.removeVersion(model.get(selectedIndexes[0], "photoId"))
            }
            MenuSeparator {}
            Instantiator {
                model: HelperModel {
                    id: helperModel
                    helpersData: Settings.helpers
                    mimeType: selectedIndexes.length > 0 ? imageView.model.get(selectedIndexes[0], "mimeType") : Utils.invalidMimeType()
                }
                MenuItem {
                    text: model.actionName
                    enabled: model.canBatch ? (selectedIndexes.length > 0) : (selectedIndexes.length == 1)
                    onTriggered: {
                        var urls = []
                        var masterIds = []
                        var photoIds = []
                        for (var i = 0; i < selectedIndexes.length; i++) {
                            var photoIndex = selectedIndexes[i]
                            masterIds.push(imageView.model.get(photoIndex, "masterVersion"))
                            photoIds.push(imageView.model.get(photoIndex, "photoId"))
                            urls.push(imageView.model.get(photoIndex, "url"))
                        }

                        helperModel.runHelper(index, urls, function (newNames, makeVersion) {
                            if (makeVersion) {
                                console.log("making new version")
                                for (var i = 0; i < newNames.length; i++) {
                                    var newName = newNames[i]
                                    var newId = GlobalWriter.createItem(Qt.resolvedUrl(newName))
                                    if (newId > 0) {
                                        GlobalWriter.makeVersion(masterIds[i], newId)
                                    }
                                }
                            } else if (!model.readOnly) {
                                console.log("Refreshing thumbnails")
                                GlobalWriter.rebuildThumbnails(photoIds)
                            }
                        })
                    }
                }
                onObjectAdded: contextMenu.insertItem(index, object)
                onObjectRemoved: contextMenu.removeItem(object)
            }
            MenuSeparator {}
            MenuItem { action: actions.deleteFromCatalog }
            MenuItem { action: actions.deleteFromDrive }
            MenuSeparator {}
            Menu {
                id: ratm
                title: qsTr("Set rating")
                enabled: selectedIndexes.length > 0
                MenuItem { text: "☆☆☆☆☆"; onTriggered: ratm.setRating(0) }
                MenuItem { text: "★☆☆☆☆"; onTriggered: ratm.setRating(1) }
                MenuItem { text: "★★☆☆☆"; onTriggered: ratm.setRating(2) }
                MenuItem { text: "★★★☆☆"; onTriggered: ratm.setRating(3) }
                MenuItem { text: "★★★★☆"; onTriggered: ratm.setRating(4) }
                MenuItem { text: "★★★★★"; onTriggered: ratm.setRating(5) }

                function setRating(rating) {
                    console.log("Setting rating to " + rating)
                    var photoIds = []
                    var length = selectedIndexes.length
                    for (var i = 0; i < length; i++) {
                        photoIds.push(model.get(selectedIndexes[i], "photoId"))
                    }
                    GlobalWriter.setRating(photoIds, rating)
                }
            }
        }
    }

    RowLayout {
        id: tagInputRow

        property var selectedIndexes: imageView.selectedIndexes
        property var model: imageView.model
        property var parentTag: tagModel.uncategorizedTag

        anchors { left: parent.left; right: parent.right }
        visible: false

        onVisibleChanged: if (!visible && tagInput.activeFocus) {
            imageView.forceActiveFocus()
        }
        onSelectedIndexesChanged: if (visible) setup();

        property var __selectedPhotos: []
        property var __oldCommonTags: []

        Label {
            text: qsTr("Tags:")
        }

        TagInput {
            id: tagInput
            Layout.fillWidth: true
            model: tagModel
            Keys.onEscapePressed: tagInputRow.visible = false
            Keys.onReturnPressed: tagInputRow.save()
        }

        TagModel {
            id: tagModel
            onCountChanged: if (allTags) tagInputRow.continueSetup()
        }

        function show() {
            setup()
            tagInputRow.visible = true
            tagInput.forceActiveFocus()
        }

        function setup() {
            tagInput.text = ""
            tagInput.enabled = false
            tagModel.allTags = false

            var photoIds = []
            var length = selectedIndexes.length
            for (var i = 0; i < length; i++) {
                photoIds.push(model.get(selectedIndexes[i], "photoId"))
            }
            __selectedPhotos = photoIds
            tagModel.photosForUsageCount = photoIds
            tagModel.allTags = true
        }

        function continueSetup() {
            /* The model is ready; find common tags */
            var commonTags = []
            var numPhotos = __selectedPhotos.length
            var length = tagModel.count
            for (var i = 0; i < length; i++) {
                var usageCount = tagModel.get(i, "usageCount") 
                if (usageCount == numPhotos) {
                    var name = tagModel.get(i, "name")
                    commonTags.push(name)
                }
            }

            __oldCommonTags = commonTags
            tagInput.text = commonTags.join(', ')
            tagInput.enabled = true
        }

        function save() {
            var commonTags = tagInput.getTags()
            var addedTags = []
            var removedTags = []
            for (var i = 0; i < commonTags.length; i++) {
                var tag = commonTags[i]
                if (__oldCommonTags.indexOf(tag) < 0 && addedTags.indexOf(tag) < 0) {
                    if (!tagModel.tag(tag).valid) {
                        var icon = model.get(selectedIndexes[0], "url")
                        GlobalWriter.createTag(tag, parentTag, icon)
                    }
                    addedTags.push(tag)
                }
            }
            for (var i = 0; i < __oldCommonTags.length; i++) {
                var tag = __oldCommonTags[i]
                if (commonTags.indexOf(tag) < 0) {
                    removedTags.push(tag)
                }
            }
            GlobalWriter.changePhotoTags(__selectedPhotos, addedTags, removedTags)
            tagInput.text = commonTags.join(', ')
        }
    }

    function handleDropOnTag(tag) {
        var photoIds = []
        var length = selectedIndexes.length
        for (var i = 0; i < length; i++) {
            photoIds.push(model.get(selectedIndexes[i], "photoId"))
        }
        GlobalWriter.changePhotoTags(photoIds, [tag], [])
    }

    function selectAll() { imageView.selectAll() }
    function selectNone() { imageView.selectNone() }

    function getSelectedIds() { return imageView.getSelectedIds() }
    function selectedUrls() { return imageView.selectedUrls() }

    function showFullscreen() {
        PopupUtils.open(Qt.resolvedUrl("DisplayWindow.qml"), this, {
            "initialIndex": imageView.currentIndex,
            "model": imageView.model,
        })
    }
    function zoomIn() { imageView.zoomIn() }
    function zoomOut() { imageView.zoomOut() }

}
