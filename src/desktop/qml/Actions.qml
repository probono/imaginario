import Imaginario 1.0
import QtQml 2.2
import QtQuick 2.2
import QtQuick.Controls 1.4
import "importer.js" as ImporterDlg
import "popupUtils.js" as PopupUtils
import "."

QtObject {
    property var photoModel: null
    property var imageView: null
    property var tagView: null

    property var _tagModel: tagView ? tagView.model : null

    property var copy: Action {
        text: qsTr("Copy")
        enabled: imageView.selectedIndexes.length > 0
        shortcut: StandardKey.Copy
        onTriggered: Clipboard.copyUrls(imageView.selectedUrls())
    }

    property var createTag: Action {
        text: qsTr("Create new tag…")
        onTriggered: PopupUtils.open(Qt.resolvedUrl("TagEditDialog.qml"), this, {
            "parentTag": (tagView.currentIndex.valid ? _tagModel.data(tagView.currentIndex, TagModel.TagRole) : _tagModel.uncategorizedTag),
        })
    }

    property var deleteFromCatalog: Action {
        text: qsTr("Delete from catalog")
        enabled: imageView.selectedIndexes.length > 0
        shortcut: StandardKey.Delete
        onTriggered: PopupUtils.open(Qt.resolvedUrl("DeleteConfirmation.qml"), imageView, {
            "imageView": imageView
        })
    }

    property var deleteFromDrive: Action {
        text: qsTr("Delete from drive")
        enabled: imageView.selectedIndexes.length > 0
        shortcut: "Shift+Delete"
        onTriggered: PopupUtils.open(Qt.resolvedUrl("DeleteConfirmation.qml"), imageView, {
            "imageView": imageView,
            "deleteFromDrive": true,
        })
    }

    property var deleteTag: Action {
        enabled: tagView.currentIndex.valid
        text: qsTr("Edit tag…")
        onTriggered: PopupUtils.open(Qt.resolvedUrl("TagEditDialog.qml"), this, {
            "tag": _tagModel.data(tagView.currentIndex, TagModel.TagRole),
        })
    }

    property var editTag: Action {
        enabled: tagView.currentIndex.valid
        text: qsTr("Delete tag")
        onTriggered: PopupUtils.open(Qt.resolvedUrl("TagDeleteConfirmation.qml"), this, {
            "tag": _tagModel.data(tagView.currentIndex, TagModel.TagRole),
        })
    }

    property var importPhotos: Action {
        text: qsTr("Import...")
        shortcut: StandardKey.New
        onTriggered: ImporterDlg.open(photoModel)
    }

    property var refreshThumbnails: Action {
        text: qsTr("Refresh thumbnail")
        enabled: imageView.selectedIndexes.length > 0
        onTriggered: GlobalWriter.rebuildThumbnails(imageView.getSelectedIds())
    }

    property var zoomIn: Action {
        text: qsTr("Zoom in")
        shortcut: StandardKey.ZoomIn
        onTriggered: imageView.zoomIn()
    }

    property var zoomOut: Action {
        text: qsTr("Zoom out")
        shortcut: StandardKey.ZoomOut
        onTriggered: imageView.zoomOut()
    }
}

