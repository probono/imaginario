import QtQuick 2.5
import QtQuick.Controls 1.4

Label {
    id: root

    property var photoModel: null

    property bool filterActive: photoModel.areaTagged

    visible: filterActive
    text: qsTr("With area tags")
}
