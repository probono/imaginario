import Imaginario 1.0
import QtQuick 2.5
import QtQuick.Controls 1.4

Item {
    id: root
    property alias backgroundColor: background.color
    property alias url: image.source
    property alias isPair: image.isPair
    property var itemStatus
    property var itemDuplicateAction
    property var duplicateList: []
    property int duplicatePhotoId: dupView.getDuplicatePhotoId()

    signal clicked(var mouse)

    property int __dupsWidth: width * 3 / 8
    property int __dupsFullWidth: width / 2

    Rectangle {
        id: background
        anchors { fill: parent; margins: 1 }
    }

    ImportDelegate {
        id: image
        anchors { left: parent.left; bottom: parent.bottom }
        height: parent.height * 3 / 4
        width: height
        itemMaxSize: width
    }

    MouseArea {
        anchors.fill: parent
        onClicked: root.clicked(mouse)
    }

    Item {
        anchors { top: parent.top; bottom: parent.bottom; right: parent.right }
        width: __dupsWidth

        MouseArea {
            id: buttonUp
            anchors { top: parent.top; left: parent.left; right: parent.right }
            height: width / 2
            enabled: dupView.currentIndex != 0
            onPressed: dupView.currentIndex--
            Image {
                anchors.centerIn: parent
                width: parent.width / 2
                height: width / 2
                source: "qrc:/icons/up"
                opacity: parent.enabled ? 1 : 0.3
            }
        }

        ListView {
            id: dupView
            anchors { top: buttonUp.bottom; bottom: buttonDown.top; right: parent.right }
            width: __dupsFullWidth
            clip: true
            model: PhotoModel {
                photoIds: root.duplicateList
            }
            delegate: Item {
                property bool isCurrentItem: ListView.isCurrentItem
                width: dupView.width
                height: thumbnail.height
                z: isCurrentItem ? 1 : -1
                ImportDelegate {
                    id: thumbnail
                    anchors { right: parent.right; top: parent.top }
                    width: isCurrentItem ? __dupsFullWidth : __dupsWidth
                    height: width
                    source: model.url
                    itemMaxSize: parent.width

                    onClicked: dupView.currentIndex = index

                    Behavior on width { NumberAnimation {} }
                }
            }

            function getDuplicatePhotoId() {
                return model.get(currentIndex, "photoId")
            }
        }

        MouseArea {
            id: buttonDown
            anchors { bottom: parent.bottom; left: parent.left; right: parent.right }
            height: width / 2
            enabled: dupView.currentIndex != dupView.count - 1
            onPressed: dupView.currentIndex++
            Image {
                anchors.centerIn: parent
                width: parent.width / 2
                height: width / 2
                source: "qrc:/icons/down"
                opacity: parent.enabled ? 1 : 0.3
            }
        }
    }

    MouseArea {
        anchors.fill: parent
        onPressed: if (mouse.modifiers == Qt.NoModifier) { mouse.accepted = false }
        onClicked: root.clicked(mouse)
    }

    Image {
        id: statusIcon
        anchors { left: parent.left; top: parent.top; margins: 4 }
        width: parent.width / 4
        height: width
        smooth: true
    }

    states: [
        State {
            name: "ignored"
            when: itemStatus == Importer.Ignored
            PropertyChanges { target: statusIcon; source: "qrc:/icons/import_ignore" }
        },
        State {
            name: "importAsNew"
            when: duplicateAction == Importer.ImportAsNew
            PropertyChanges { target: statusIcon; source: "qrc:/icons/import_as_new" }
        },
        State {
            name: "importReplacing"
            when: duplicateAction == Importer.ImportReplacing
            PropertyChanges { target: statusIcon; source: "qrc:/icons/import_replacing" }
        },
        State {
            name: "importAsVersion"
            when: duplicateAction == Importer.ImportAsVersion
            PropertyChanges { target: statusIcon; source: "qrc:/icons/import_as_version" }
        },
        State {
            name: "undecided"
        }
    ]
}
