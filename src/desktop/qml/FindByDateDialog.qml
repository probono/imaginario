import Imaginario 1.0
import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.0
import QtQuick.Window 2.2
import "."

Dialog {
    id: root

    property var photoModel: null

    title: qsTr("Find by date")

    contentItem: ColumnLayout {
        spacing: 20

        RowLayout {
            Label {
                font.bold: true
                text: qsTr("Select period:")
            }

            ComboBox {
                id: periodControl
                Layout.fillWidth: true
                model: periodModel
                onCurrentIndexChanged: computeDays()
            }
        }

        GridLayout {
            columns: 2
            enabled: periodControl.currentIndex == 0
            Label {
                font.bold: true
                text: qsTr("Start date:")
            }

            Label {
                font.bold: true
                text: qsTr("End date:")
            }

            Calendar {
                id: cal0
                Layout.fillWidth: true
                Layout.fillHeight: true
            }

            Calendar {
                id: cal1
                Layout.fillWidth: true
                Layout.fillHeight: true
                minimumDate: cal0.selectedDate
            }
        }

        RowLayout {
            anchors { right: parent.right }

            Button {
                anchors.verticalCenter: parent.verticalCenter
                text: qsTr("Cancel")
                iconName: "cancel"
                onClicked: root.close()
            }

            Button {
                anchors.verticalCenter: parent.verticalCenter
                text: qsTr("Find")
                iconName: "dialog-ok"
                onClicked: {
                    root.save()
                    root.close()
                }
            }
        }
    }

    Component.onCompleted: setupTimes()

    ListModel {
        id: periodModel
        ListElement { text: qsTr("Custom range") }
        ListElement { text: qsTr("Today") }
        ListElement { text: qsTr("Yesterday") }
        ListElement { text: qsTr("Last 7 days") }
        ListElement { text: qsTr("Last 30 days") }
        ListElement { text: qsTr("Last 90 days") }
        ListElement { text: qsTr("Last 360 days") }
    }

    function setupTimes() {
        var now = new Date()
        periodModel.append({ text: Qt.formatDate(now, "MMMM") })
        var t = new Date(now.getTime())
        t.setMonth(now.getMonth() - 1)
        periodModel.append({ text: Qt.formatDate(t, "MMMM") })
        periodModel.append({ text: Qt.formatDate(now, "yyyy") })
        var t = new Date(now.getTime())
        t.setFullYear(now.getFullYear() - 1)
        periodModel.append({ text: Qt.formatDate(t, "yyyy") })

        if (!isNaN(photoModel.time0)) cal0.selectedDate = photoModel.time0
        if (!isNaN(photoModel.time1)) cal1.selectedDate = photoModel.time1
    }

    function computeDays() {
        var time0 = new Date()
        var time1 = new Date()
        var period = periodControl.currentIndex
        if (period == 0) {
            return
        } else if (period == 1) {
            // defaults are fine
        } else if (period == 2) {
            time0.setDate(time0.getDate() - 1)
            time1 = new Date(time0.getTime())
        } else if (period >= 3 && period <= 6) {
            var days = [ 7, 30, 90, 360 ]
            time0.setDate(time0.getDate() - days[period - 3])
            time1 = new Date()
        } else if (period == 7) {
            time0.setDate(1)
            time1.setDate(1)
            time1.setMonth(time1.getMonth() + 1)
            time1.setDate(time1.getDate() - 1)
        } else if (period == 8) {
            time0.setDate(1)
            time0.setMonth(time0.getMonth() - 1)
            time1.setDate(0)
        } else if (period == 9) {
            time0.setMonth(0, 1)
            time1.setMonth(11, 31)
        } else if (period == 10) {
            time0.setFullYear(time0.getFullYear() - 1)
            time0.setMonth(0, 1)
            time1.setFullYear(time1.getFullYear() - 1)
            time1.setMonth(11, 31)
        }
        cal0.selectedDate = time0
        cal1.selectedDate = time1
    }

    function save() {
        var time0 = cal0.selectedDate
        var time1 = cal1.selectedDate

        time0.setHours(0, 0, 0, 0)
        time1.setHours(23, 59, 59, 999)
        photoModel.time0 = time0
        photoModel.time1 = time1
    }
}
