import Imaginario 1.0
import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.1
import QtQuick.Window 2.2

GridLayout {
    id: root

    columns: 2
    columnSpacing: 4
    rowSpacing: 4

    Label {
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignRight
        text: qsTr("When importing photos, copy them to:")
    }

    ComboBox {
        id: combo
        Layout.minimumWidth: 200 * Screen.devicePixelRatio
        Layout.fillWidth: true
        model: [
            Settings.photoDir,
            qsTr("Choose folder...")
        ]

        onActivated: if (index == 1) {
            // The timer is a workaround for https://bugreports.qt.io/browse/QTBUG-51113
            timer.start()
            fileDialog.open()
        }
        Timer {
            id: timer
            interval: 10
            onTriggered: combo.currentIndex = 0
        }
    }

    Label {
        Layout.alignment: Qt.AlignRight
        Layout.rowSpan: 3
        text: qsTr("Store tags and descriptions")
        anchors.baseline: radioEmbed.baseline
    }

    RadioButton {
        id: radioEmbed
        text: qsTr("Inside the image files when possible")
        checked: false
        exclusiveGroup: exifGroup
    }
    RadioButton {
        id: radioXmp
        text: qsTr("In XMP files next to the images")
        checked: false
        exclusiveGroup: exifGroup
    }
    RadioButton {
        id: radioDatabase
        text: qsTr("Only in the Imaginario database")
        checked: false
        exclusiveGroup: exifGroup
    }
    ExclusiveGroup {
        id: exifGroup
        onCurrentChanged: {
            Settings.embed = (current == radioEmbed)
            Settings.writeXmp = (current != radioDatabase)
        }
    }

    // Just to prevent vertical stretching
    Item {
        implicitHeight: 0
        Layout.fillHeight: true
    }

    FileDialog {
        id: fileDialog
        title: qsTr("Select image directory")
        folder: shortcuts.pictures
        selectMultiple: false
        selectFolder: true
        onAccepted: Settings.photoDir = urlToLocalFile(fileUrl)

        function urlToLocalFile(url) {
            var s = url.toString()
            return s.indexOf("file://") == 0 ? s.substr(7) : s
        }
    }

    Component.onCompleted: {
        var activeRadio = Settings.writeXmp ? (Settings.embed ? radioEmbed : radioXmp) : radioDatabase
        activeRadio.checked = true
    }
}
