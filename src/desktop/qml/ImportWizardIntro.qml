import Imaginario 1.0
import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.0
import QtQuick.Window 2.2

ColumnLayout {
    id: root

    property bool done: importer.count > 0
    property var importer: null
    property var tagModel: null

    GroupBox {
        id: toolBar
        anchors.horizontalCenter: parent.horizontalCenter
        Layout.minimumWidth: 400 * Screen.devicePixelRatio
        title: qsTr("Import from:")

        Column {
            anchors.fill: parent
            spacing: 10

            Button {
                anchors { left: parent.left; right: parent.right }
                text: qsTr("Choose folder...")
                onClicked: fileDialog.open()
            }

            Repeater {
                model: FolderModel {}
                Button {
                    anchors { left: parent.left; right: parent.right }
                    text: model.displayName
                    onClicked: importer.importFolder = "file://" + model.path
                }
            }
        }
    }

    GridLayout {
        anchors { left: parent.left; right: parent.right }
        Layout.fillHeight: true
        columns: 2

        CheckBox {
            id: copyFilesCtrl
            Layout.fillWidth: true
            Layout.minimumWidth: 256 * Screen.devicePixelRatio
            text: qsTr("Copy files to the Photos folder")
            checked: Settings.copyFiles
        }

        CheckBox {
            id: recursiveCtrl
            Layout.fillWidth: true
            Layout.minimumWidth: 256 * Screen.devicePixelRatio
            text: qsTr("Include subfolders")
            checked: importer.recursive
            onCheckedChanged: importer.recursive = checked
        }
    }

    FileDialog {
        id: fileDialog
        title: qsTr("Select import source")
        folder: shortcuts.pictures
        selectFolder: true
        onAccepted: importer.importFolder = fileUrl
    }

    function saveSettings() {
        Settings.copyFiles = copyFilesCtrl.checked
    }
}
