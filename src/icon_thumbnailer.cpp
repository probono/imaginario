/*
 * Copyright (C) 2016 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "icon_thumbnailer.h"

#include <QCryptographicHash>
#include <QDir>
#include <QFile>
#include <QFileInfo>
#include <QImageReader>
#include <QStandardPaths>

using namespace Imaginario;

namespace Imaginario {

class IconThumbnailerPrivate
{
    IconThumbnailerPrivate(int iconSize);

    void setupCacheDir();
    QString thumbnailFileName(const QUrl &uri) const {
        QByteArray ba = QCryptographicHash::hash(uri.toEncoded(),
                                                 QCryptographicHash::Md5);
        return QString::fromLatin1(ba.toHex()) + ".png";
    }

    QString thumbnailPath(const QUrl &image) const;

    QImage create(const QUrl &uri) const;

private:
    friend class IconThumbnailer;
    int m_iconSize;
    QDir m_dir;
};

} // namespace

IconThumbnailerPrivate::IconThumbnailerPrivate(int iconSize):
    m_iconSize(iconSize)
{
    setupCacheDir();
}

void IconThumbnailerPrivate::setupCacheDir()
{
    QString base =
        QStandardPaths::writableLocation(QStandardPaths::CacheLocation);
    m_dir = base + QString("/tag_icons_%1").arg(m_iconSize);
    if (!m_dir.exists()) {
        m_dir.mkpath(".");
    }
}

QString IconThumbnailerPrivate::thumbnailPath(const QUrl &image) const
{
    return m_dir.filePath(thumbnailFileName(image));
}

QImage IconThumbnailerPrivate::create(const QUrl &uri) const
{
    QFileInfo fileInfo(uri.toLocalFile());
    if (Q_UNLIKELY(!fileInfo.isReadable())) return QImage();

    QImageReader reader(fileInfo.filePath());

    QSize size(m_iconSize, m_iconSize);

    /* scale when loading, if possible */
    QSize imageSize = reader.size();
    if (imageSize.isValid()) {
        int minLength = qMin(imageSize.width(), imageSize.height());
        QRect clipRect((imageSize.width() - minLength) / 2,
                       (imageSize.height() - minLength) / 2,
                       minLength, minLength);
        reader.setClipRect(clipRect);
        reader.setScaledSize(size);
    }

    QImage thumbnail = reader.read();
    if (!thumbnail.isNull()) {
        QString filePath(thumbnailPath(uri));
        thumbnail.save(filePath, "PNG", 90);
    }

    return thumbnail;
}

IconThumbnailer::IconThumbnailer(int iconSize):
    d_ptr(new IconThumbnailerPrivate(iconSize))
{
}

IconThumbnailer::~IconThumbnailer()
{
    delete d_ptr;
}

QImage IconThumbnailer::load(const QUrl &uri) const
{
    Q_D(const IconThumbnailer);

    QImageReader reader(d->thumbnailPath(uri));
    QImage image = reader.read();
    if (image.isNull()) {
        image = d->create(uri);
    }

    return image;
}
