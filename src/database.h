/*
 * Copyright (C) 2014 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef IMAGINARIO_DATABASE_H
#define IMAGINARIO_DATABASE_H

#include "abstract_database.h"
#include "roll.h"
#include "types.h"

#include <QObject>
#include <QSharedDataPointer>
#include <QUrl>
#include <QVariant>
#include <QVector>

namespace Imaginario {

class DatabasePrivate;
class Database: public AbstractDatabase
{
    Q_OBJECT

public:
    class Photo;

private:
    class PhotoData: public QSharedData
    {
        enum VersionFlags {
            NoVersions = 0,
            IsDefaultVersion = 1,
            IsNonDefaultVersion = 2,
        };
        PhotoData(): id(-1), rollId(0), rating(-1) {}
        friend class Photo;
        friend class DatabasePrivate;
        PhotoId id;
        QString baseUri;
        QString fileName;
        QString description;
        QDateTime time;
        RollId rollId;
        VersionFlags versionFlags;
        int rating;
        GeoPoint location;
    };

public:
    class Photo {
    public:
        Photo(): d(new PhotoData) {}

        PhotoId id() const { return d->id; }
        bool isValid() const { return id() >= 0; }

        void setBaseUri(const QString &s) { d->baseUri = s; }
        QString baseUri() const { return d->baseUri; }

        void setFileName(const QString &s) { d->fileName = s; }
        QString fileName() const { return d->fileName; }
        QString filePath() const { return url().toLocalFile(); }
        QUrl url() const { return QUrl(baseUri() + "/" + fileName()); }

        void setDescription(const QString &s) { d->description = s; }
        QString description() const { return d->description; }

        void setTime(const QDateTime &t) { d->time = t; }
        QDateTime time() const { return d->time; }

        RollId rollId() const { return d->rollId; }

        void setRating(int rating) { d->rating = rating; }
        int rating() const { return d->rating; }

        void setLocation(const GeoPoint &p) { d->location = p; }
        GeoPoint location() const { return d->location; }

        bool isDefaultVersion() const {
            return d->versionFlags == PhotoData::IsDefaultVersion;
        }
        bool isNonDefaultVersion() const {
            return d->versionFlags == PhotoData::IsNonDefaultVersion;
        }

    private:
        friend class DatabasePrivate;
        Photo(PhotoData *d): d(d) {};
        QSharedDataPointer<PhotoData> d;
    };

    static Database *instance();
    ~Database();

    void clearCache();

    RollId createRoll(const QDateTime &time = QDateTime());
    QList<Roll> rolls(int limit = -1);

    PhotoId addPhoto(const Photo &photo, RollId roll);
    Photo photo(PhotoId photoId);
    bool deletePhoto(PhotoId photoId);
    bool makeVersion(PhotoId photoId, PhotoId master);
    bool addTags(PhotoId photo, const QList<Tag> &tags);
    bool addTagWithArea(PhotoId photo, Tag tag, const QRectF &area);
    bool setTags(PhotoId photo, const QList<Tag> &tags);
    bool setTags(PhotoId photo, const QStringList &tagNames) {
        return setTags(photo, tags(tagNames));
    }
    bool removeTags(PhotoId photo, const QList<Tag> &tags);
    QList<Tag> tags(PhotoId photoId);
    QVector<TagArea> tagAreas(PhotoId photoId);

    int computeUsageCount(Tag tag, const QList<PhotoId> &photos);

    bool setDescription(PhotoId photo, const QString &description);
    bool setRating(PhotoId photo, int rating);
    bool setLocation(PhotoId photo, const GeoPoint &p);

    QList<Photo> photos(const QList<PhotoId> &photoIds);
    QList<Photo> findPhotos(SearchFilters filters,
                            Qt::SortOrder sort = Qt::DescendingOrder);
    QList<Photo> photoVersions(PhotoId photoId);
    PhotoId masterVersion(PhotoId photoId);

    Tag createTag(Tag parent, const QString &name, const QString &icon);
    struct TagUpdate {
        QString name;
        QString icon;
        QVariant parent;
    };
    bool updateTag(Tag tag, const TagUpdate &data);
    bool deleteTag(Tag tag);
    Tag tag(const QString &name);
    QList<Tag> allTags();
    QList<Tag> tags(Tag parent);
    QList<Tag> tags(const QStringList &tagNames);

    /* Default tags */
    Tag uncategorizedTag() const;
    Tag peopleTag() const;
    Tag placesTag() const;
    Tag eventsTag() const;
    Tag importedTagsTag() const;

Q_SIGNALS:
    void photoAdded(PhotoId id);
    void photoDeleted(PhotoId id);
    void photoChanged(PhotoId id);
    void photoTagsChanged(PhotoId id);
    void photoTagAreasChanged(PhotoId id);

    void rollAdded(Roll roll);

    void tagCreated(Tag tag);
    void tagChanged(Tag tag);
    void tagDeleted(Tag tag);

protected:
    Database(QObject *parent = 0);

private:
    friend class Roll;
    friend class Tag;
    Q_DECLARE_PRIVATE(Database)
};

} // namespace

#endif // IMAGINARIO_DATABASE_H
