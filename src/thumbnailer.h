/*
 * Copyright (C) 2015 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef IMAGINARIO_THUMBNAILER_H
#define IMAGINARIO_THUMBNAILER_H

#include <QImage>
#include <QUrl>

namespace Imaginario {

class ThumbnailerPrivate;
class Thumbnailer
{
public:
    Thumbnailer();
    ~Thumbnailer();

    /* If the caller has a QFileInfo for the image, we should pass it instead
     * of the QUrl here. */
    QImage load(const QUrl &uri, const QSize &requestedSize) const;

    bool create(const QUrl &uri) const;

    static bool &size256Used();
    static bool &size512Used();

private:
    ThumbnailerPrivate *d_ptr;
    Q_DECLARE_PRIVATE(Thumbnailer)
};

} // namespace

#endif // IMAGINARIO_THUMBNAILER_H
