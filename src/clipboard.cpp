/*
 * Copyright (C) 2016 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "clipboard.h"

#include <QDebug>
#include <QClipboard>
#include <QGuiApplication>
#include <QMimeData>

using namespace Imaginario;

namespace Imaginario {

class ClipboardPrivate
{
    Q_DECLARE_PUBLIC(Clipboard)

public:
    ClipboardPrivate(Clipboard *q);
    ~ClipboardPrivate();


private:
    QClipboard *m_clipboard;
    Clipboard *q_ptr;
};

} // namespace

ClipboardPrivate::ClipboardPrivate(Clipboard *q):
    m_clipboard(QGuiApplication::clipboard()),
    q_ptr(q)
{
}

ClipboardPrivate::~ClipboardPrivate()
{
}

Clipboard::Clipboard(QObject *parent):
    QObject(parent),
    d_ptr(new ClipboardPrivate(this))
{
}

Clipboard::~Clipboard()
{
    delete d_ptr;
}

void Clipboard::copyUrls(const QList<QUrl> &urls)
{
    Q_D(Clipboard);

    QMimeData *mimeData = new QMimeData;
    mimeData->setUrls(urls);
    d->m_clipboard->setMimeData(mimeData);
}
