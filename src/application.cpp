/*
 * Copyright (C) 2015 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "application.h"
#include "i18n.h"
#include "job_executor.h"

#include <QDebug>
#include <QIcon>
#include <QThreadPool>

using namespace Imaginario;

namespace Imaginario {

class ApplicationPrivate: public QObject
{
    Q_OBJECT
    Q_DECLARE_PUBLIC(Application)

public:
    ApplicationPrivate(Application *q);

private Q_SLOTS:
    void onStateChanged(Qt::ApplicationState state);

private:
    Application *q_ptr;
};

} // namespace

ApplicationPrivate::ApplicationPrivate(Application *q):
    QObject(),
    q_ptr(q)
{

    QObject::connect(q_ptr,
                     SIGNAL(applicationStateChanged(Qt::ApplicationState)),
                     this, SLOT(onStateChanged(Qt::ApplicationState)));

}

void ApplicationPrivate::onStateChanged(Qt::ApplicationState state)
{
    JobExecutor *executor = JobExecutor::instance();
    /* Workaround for https://bugs.launchpad.net/bugs/1456706
     * TODO remove once above bug is fixed */
    if (QGuiApplication::platformName().startsWith("ubuntu") &&
        state == Qt::ApplicationInactive) {
        state = Qt::ApplicationSuspended;
    }

    if (state == Qt::ApplicationSuspended) {
        executor->stop();
    } else if (state == Qt::ApplicationActive) {
        executor->start();
    }
}

Application::Application(int &argc, char **argv):
#ifdef DESKTOP_BUILD
    QApplication(argc, argv),
#else
    QGuiApplication(argc, argv),
#endif
    d_ptr(new ApplicationPrivate(this))
{
    setApplicationDisplayName(APPLICATION_DISPLAY_NAME);
    setApplicationName(APPLICATION_NAME);
    setApplicationVersion(APPLICATION_VERSION);
    setOrganizationName(QString());
    setOrganizationDomain(APPLICATION_NAME);
    setWindowIcon(QIcon(":/icons/imaginario"));

    installTranslator(new GettextTranslator(this));
}

Application::~Application()
{
    QThreadPool::globalInstance()->waitForDone(5000);
    delete d_ptr;
}

#include "application.moc"
