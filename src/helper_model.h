/*
 * Copyright (C) 2017 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef IMAGINARIO_HELPER_MODEL_H
#define IMAGINARIO_HELPER_MODEL_H

#include <QAbstractListModel>
#include <QJsonArray>
#include <QList>
#include <QMimeType>
#include <QQmlParserStatus>
#include <QScopedPointer>
#include <QUrl>

class QJSValue;

namespace Imaginario {

class HelperModelPrivate;
class HelperModel: public QAbstractListModel, public QQmlParserStatus
{
    Q_OBJECT
    Q_INTERFACES(QQmlParserStatus)
    Q_ENUMS(Roles)
    Q_PROPERTY(int count READ rowCount NOTIFY countChanged)
    Q_PROPERTY(QJsonArray helpersData READ helpersData \
               WRITE setHelpersData NOTIFY helpersDataChanged)
    Q_PROPERTY(QJsonArray defaultHelpersData READ defaultHelpersData CONSTANT)
    Q_PROPERTY(QString photoDir READ photoDir WRITE setPhotoDir \
               NOTIFY photoDirChanged)
    Q_PROPERTY(QMimeType mimeType READ mimeType WRITE setMimeType \
               NOTIFY mimeTypeChanged)

public:
    enum Roles {
        ActionNameRole = Qt::UserRole + 1,
        CommandRole,
        CanBatchRole,
        ReadOnlyRole,
        MakeVersionRole,
    };

    HelperModel(QObject *parent = 0);
    ~HelperModel();

    void setHelpersData(const QJsonArray &data);
    QJsonArray helpersData() const;

    QJsonArray defaultHelpersData() const;

    void setPhotoDir(const QString &photoDir);
    QString photoDir() const;

    void setMimeType(const QMimeType &mimeType);
    QMimeType mimeType() const;

    Q_INVOKABLE bool runHelper(int row, const QUrl &file, const QJSValue &callback);
    Q_INVOKABLE bool runHelper(int row, const QList<QUrl> &urls,
                               const QJSValue &callback);

    Q_INVOKABLE QVariant get(int row, const QString &role) const;

    int rowCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
    QVariant data(const QModelIndex &index,
                  int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;
    QHash<int, QByteArray> roleNames() const Q_DECL_OVERRIDE;

    void classBegin() Q_DECL_OVERRIDE;
    void componentComplete() Q_DECL_OVERRIDE;

Q_SIGNALS:
    void countChanged();
    void helpersDataChanged();
    void photoDirChanged();
    void mimeTypeChanged();

private:
    QScopedPointer<HelperModelPrivate> d_ptr;
    Q_DECLARE_PRIVATE(HelperModel)
};

} // namespace

#endif // IMAGINARIO_HELPER_MODEL_H
