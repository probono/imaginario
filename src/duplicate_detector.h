/*
 * Copyright (C) 2015 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef IMAGINARIO_DUPLICATE_DETECTOR_H
#define IMAGINARIO_DUPLICATE_DETECTOR_H

#include "types.h"

#include <QList>
#include <QObject>
#include <QUrl>

namespace Imaginario {

struct MaybeDuplicate {
    MaybeDuplicate(PhotoId id, double score): id(id), score(score) {}
    PhotoId id;
    double score;
};

typedef QList<MaybeDuplicate> MaybeDuplicates;

class DuplicateDetectorPrivate;
class DuplicateDetector: public QObject
{
    Q_OBJECT
    Q_PROPERTY(double minimumScore READ minimumScore WRITE setMinimumScore \
               NOTIFY minimumScoreChanged)

public:
    struct RawPair {
        QUrl raw;
        QUrl edited;
        bool operator==(const RawPair &other) const {
            return raw == other.raw && edited == other.edited;
        }
    };
    typedef QVector<RawPair> RawPairs;

    DuplicateDetector(QObject *parent = 0);
    ~DuplicateDetector();

    void setMinimumScore(double score);
    double minimumScore() const;

    Q_INVOKABLE bool checkFile(const QUrl &url);

    MaybeDuplicates duplicatesFor(const QUrl &url) const;
    RawPairs makeRawPairs(const QList<QUrl> &urls) const;

Q_SIGNALS:
    void minimumScoreChanged();

private:
    DuplicateDetectorPrivate *d_ptr;
    Q_DECLARE_PRIVATE(DuplicateDetector)
};

} // namespace

#endif // IMAGINARIO_DUPLICATE_DETECTOR_H
