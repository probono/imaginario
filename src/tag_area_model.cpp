/*
 * Copyright (C) 2015 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "tag_area_model.h"

#include "database.h"
#include "face_detector.h"

#include <QDebug>
#include <QHash>
#include <QRectF>
#include <QVariant>
#include <QVector>
#include <QtConcurrent>

using namespace Imaginario;

namespace Imaginario {

typedef QList<QRectF> Areas;

class NameArea
{
public:
    enum Source {
        Local = 0,
        Database,
        Detected,
    };
    NameArea(): source(Local) {}
    NameArea(const TagArea &a):
        rect(a.rect()),
        tagName(a.tag().name()),
        source(Database)
    {}
    NameArea(const QRectF &rect, const QString &tagName = QString()):
        rect(rect),
        tagName(tagName),
        source(Detected)
    {}

    QRectF rect;
    QString tagName;
    Source source;
};

class TagAreaModelPrivate: public QObject
{
    Q_OBJECT
    Q_DECLARE_PUBLIC(TagAreaModel)

public:
    TagAreaModelPrivate(TagAreaModel *q);
    ~TagAreaModelPrivate();

    Areas detectFaces(QUrl fileUrl);

    bool areaIsSame(const QRectF &a, const QRectF &b);
    bool areaIsNew(const QRectF &area);

private Q_SLOTS:
    void onDetectionFinished();
    void onDatabaseChanged(PhotoId id);

private:
    QHash<int, QByteArray> m_roles;
    Database *m_db;
    PhotoId m_photoId;
    QVector<NameArea> m_areas;
    FaceDetector m_detector;
    QFutureWatcher<Areas> m_detectOperation;
    mutable TagAreaModel *q_ptr;
};

} // namespace

TagAreaModelPrivate::TagAreaModelPrivate(TagAreaModel *q):
    m_db(Database::instance()),
    m_photoId(-1),
    q_ptr(q)
{
    m_roles[TagAreaModel::TagRole] = "tag";
    m_roles[TagAreaModel::RectRole] = "rect";

    QObject::connect(&m_detectOperation, SIGNAL(finished()),
                     this, SLOT(onDetectionFinished()));
    QObject::connect(m_db, SIGNAL(photoTagAreasChanged(PhotoId)),
                     this, SLOT(onDatabaseChanged(PhotoId)));
}

TagAreaModelPrivate::~TagAreaModelPrivate()
{
}

Areas TagAreaModelPrivate::detectFaces(QUrl fileUrl)
{
    return m_detector.checkFile(fileUrl);
}

bool TagAreaModelPrivate::areaIsSame(const QRectF &a, const QRectF &b)
{
    QRectF intersection = a & b;
    if (intersection.isEmpty()) return false;

    qreal areaInt = intersection.width() * intersection.height();
    qreal areaA = a.width() * a.height();
    return areaInt > areaA * 0.7;
}

bool TagAreaModelPrivate::areaIsNew(const QRectF &area)
{
    Q_FOREACH(const NameArea &na, m_areas) {
        if (areaIsSame(na.rect, area)) return false;
    }
    return true;
}

void TagAreaModelPrivate::onDetectionFinished()
{
    Q_Q(TagAreaModel);

    if (!m_detectOperation.isCanceled()) {
        Areas areas = m_detectOperation.result();

        int index = m_areas.count();
        Areas filteredAreas;
        Q_FOREACH(const QRectF &area, areas) {
            if (areaIsNew(area)) {
                filteredAreas.append(area);
            }
        }
        q->beginInsertRows(QModelIndex(),
                           index, index + filteredAreas.count() - 1);
        Q_FOREACH(const QRectF &area, filteredAreas) {
            m_areas.append(NameArea(area));
        }
        q->endInsertRows();
    }

    Q_EMIT q->detectingChanged();
}

void TagAreaModelPrivate::onDatabaseChanged(PhotoId photoId)
{
    Q_Q(TagAreaModel);

    if (photoId != m_photoId) return;

    /* replace all areas which we got from the DB */
    QVector<NameArea> newAreas;
    QVector<TagArea> storedAreas = m_db->tagAreas(m_photoId);
    Q_FOREACH(const TagArea &a, storedAreas) {
        newAreas.append(NameArea(a));
    }
    Q_FOREACH(const NameArea &a, m_areas) {
        if (a.source != NameArea::Database) {
            newAreas.append(a);
        }
    }
    q->beginResetModel();
    m_areas = newAreas;
    q->endResetModel();
}

TagAreaModel::TagAreaModel(QObject *parent):
    QAbstractListModel(parent),
    d_ptr(new TagAreaModelPrivate(this))
{
    QObject::connect(this, SIGNAL(modelReset()),
                     this, SIGNAL(countChanged()));
    QObject::connect(this, SIGNAL(rowsRemoved(const QModelIndex&,int,int)),
                     this, SIGNAL(countChanged()));
    QObject::connect(this, SIGNAL(rowsInserted(const QModelIndex&,int,int)),
                     this, SIGNAL(countChanged()));
}

TagAreaModel::~TagAreaModel()
{
    delete d_ptr;
}

void TagAreaModel::setPhotoId(PhotoId photoId)
{
    Q_D(TagAreaModel);
    if (d->m_photoId == photoId) return;
    d->m_photoId = photoId;

    beginResetModel();
    d->m_areas.clear();
    QVector<TagArea> storedAreas = d->m_db->tagAreas(photoId);
    d->m_areas.reserve(storedAreas.count());
    Q_FOREACH(const TagArea &a, storedAreas) {
        d->m_areas.append(NameArea(a));
    }
    endResetModel();

    Q_EMIT photoIdChanged();
}

PhotoId TagAreaModel::photoId() const
{
    Q_D(const TagAreaModel);
    return d->m_photoId;
}

bool TagAreaModel::canDetect() const
{
    Q_D(const TagAreaModel);
    return d->m_detector.isValid();
}

bool TagAreaModel::isDetecting() const
{
    Q_D(const TagAreaModel);
    return d->m_detectOperation.isRunning();
}

bool TagAreaModel::detectFaces()
{
    Q_D(TagAreaModel);
    if (!d->m_detector.isValid() || isDetecting()) {
        return false;
    }
    QUrl fileUrl = d->m_db->photo(d->m_photoId).url();
    QFuture<Areas> future =
        QtConcurrent::run(d, &TagAreaModelPrivate::detectFaces,
                          fileUrl);
    d->m_detectOperation.setFuture(future);

    Q_EMIT detectingChanged();

    return true;
}

void TagAreaModel::cancelDetection()
{
    Q_D(TagAreaModel);
    /* This will cause the finished() signal to be emitted and the
     * future to be canceled, if it's running. */
    d->m_detectOperation.setFuture(QFuture<Areas>());
}

QVariant TagAreaModel::get(int row, const QString &roleName) const
{
    int role = roleNames().key(roleName.toLatin1(), -1);
    return data(index(row, 0), role);
}

int TagAreaModel::rowCount(const QModelIndex &parent) const
{
    Q_D(const TagAreaModel);
    Q_UNUSED(parent);
    return d->m_areas.count();
}

QVariant TagAreaModel::data(const QModelIndex &index, int role) const
{
    Q_D(const TagAreaModel);

    int row = index.row();
    if (row < 0 || row >= d->m_areas.count()) return QVariant();

    const NameArea &a = d->m_areas[row];
    switch (role) {
    case TagRole:
        return a.tagName;
    case RectRole:
        return a.rect;
    default:
        qWarning() << "Unknown role ID:" << role;
        return QVariant();
    }
}

QHash<int, QByteArray> TagAreaModel::roleNames() const
{
    Q_D(const TagAreaModel);
    return d->m_roles;
}

#include "tag_area_model.moc"
