# Imaginario

## A cross-platform photo manager application

[![build status](https://gitlab.com/mardy/imaginario/badges/master/build.svg)](https://gitlab.com/mardy/imaginario/commits/master)
[![coverage report](https://gitlab.com/mardy/imaginario/badges/master/coverage.svg)](https://gitlab.com/mardy/imaginario/commits/master)

Imaginario is a photo manager application written in Qt+QML. Initially written
as a mobile application for Ubuntu phones and tablets, it has grown into a
cross-platform application aiming to be used across different operating systems
and form factors.

The current development focus is on preparing Imaginario to be used as a desktop
application; mobile ports to Android and iOS are a bit further down the roadmap.

## Features

- Migrate photos from Shotwell, F-Spot (and others, if you [request it](https://gitlab.com/mardy/imaginario/issues/new))
- Adding metadata to images: tags, geolocation, title, description and rating
- Metadata can be embedded in the images or stored as a sidecar file
- Filter pictures by tags, date, geolocation, import roll
- Hide pictures containing specific user-defined tags
- Face detection and face/object tagging

## Screenshots

### Imaginario on Ubuntu phone:

<a href="https://www.mardy.it/archivos/imagines/imaginario-main.png"><img src="https://www.mardy.it/archivos/imagines/imaginario-main.png" border="0" width="270px"></a>
<a href="https://www.mardy.it/archivos/imagines/imaginario-map-filter.png"><img src="https://www.mardy.it/archivos/imagines/imaginario-map-filter.png" border="0" width="270px"></a>
<a href="https://www.mardy.it/archivos/imagines/imaginario-settings.png"><img src="https://www.mardy.it/archivos/imagines/imaginario-settings.png" border="0" width="270px"></a>

### Imaginario on the desktop:

<a href="https://1.bp.blogspot.com/-6dCJ2PSV1fs/V0qh9rqt7sI/AAAAAAAABXs/DZdbIOJ_tbURvEAvkPfjIBOfjS2xCqT2gCLcB/s1600/ImaginarioDesktop.png" imageanchor="1" style="margin-left: auto; margin-right: auto;"><img border="0" height="468" src="https://1.bp.blogspot.com/-6dCJ2PSV1fs/V0qh9rqt7sI/AAAAAAAABXs/DZdbIOJ_tbURvEAvkPfjIBOfjS2xCqT2gCLcB/s640/ImaginarioDesktop.png" width="640" /></a>

## How to install

Imaginario is available in a [PPA](https://launchpad.net/~mardy/+archive/ubuntu/imaginario) for Ubuntu-based distributions:
```
sudo add-apt-repository ppa:mardy/imaginario
sudo apt-get install imaginario
```


## How to build

In order to test out the desktop version of Imaginario (and hopefully to
contribute to its development ;-) ) please checkout the source code and follow these steps:

```
mkdir build && cd build
qmake CONFIG+=desktop CONFIG+=debug PREFIX=/tmp/opt
make install
```
This will install imaginario in `/tmp/opt`, which will get cleared the next time
you reboot your computer; this is convenient if you don't want to risk messing
up your system just to try out the application.

In order to launch Imaginario:
```
cd /tmp/opt/share
../bin/imaginario
```

Have fun! :-)